/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.gui;

import com.davidlm.base.*;
import com.davidlm.informes.ReportGenerator;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

import javax.persistence.PersistenceException;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;


/**
 * Clase Controlador del Modelo, Vista, Controlador de usuario tipo Vendedor.
 */
public class ControladorVendedor extends Component implements ActionListener, ListSelectionListener, ChangeListener, FocusListener, MouseListener, ItemListener {


    private VistaVendedor2 vista;
    private ModeloVendedor modelo;


    /**
     *Constructor de la clase Controlador de tipo Vendedor. Tiene varios métodos.
     * @param vista Objeto tipo VistaVendedor2
     * @param modelo Objeto tipo ModeloVendedor
     */
    public ControladorVendedor(VistaVendedor2 vista, ModeloVendedor modelo) {
        this.vista = vista;
        this.modelo = modelo;

        modelo.conectar();
        addActionListeners(this);
        addListSelectionListener(this);
        addFocusListeners(this);
        addItemListeners(this);

        listarCaracteristicasPedido(modelo.getCaracteristicasPedido());
        listarPedidos(modelo.getPedidos());
        listarClientes(modelo.getClientes());

        calcularPrecioTotalesDeTodos();
        listarPedidos(modelo.getPedidos());

    }


    /**
     * Método que nos configura los listeners de los ComboBox
     * @param controlador Objeto tipo ControladorVendedor
     */
    private void addItemListeners(ControladorVendedor controlador) {
        vista.comboClientePedido.addItemListener(controlador);
        vista.comboPedidoCaractPedido.addItemListener(controlador);
        vista.comboProductoCaractPedido.addItemListener(controlador);

    }

    /**
     * Método que nos configura los listeners de los JList
     * @param listener Objeto tipo ListSelectionListener
     */
    private void addListSelectionListener(ListSelectionListener listener){
        vista.listCaracteristicasPedido.addListSelectionListener(listener);
        vista.listCliente.addListSelectionListener(listener);
        vista.listPedido.addListSelectionListener(listener);
        vista.listCaracteristicasPedidoPedidoVendedor.addListSelectionListener(listener);
        vista.listProductosCaracteristicasPedidoPedido.addListSelectionListener(listener);

    }


    /**
     * Método que nos configura los listeners de los ComboBox para cuando los seleccionemos
     * @param controlador Objeto tipo ControladorVendedor
     */
    private void addFocusListeners(ControladorVendedor controlador) {
        vista.comboPedidoCaractPedido.addFocusListener(controlador);
        vista.comboProductoCaractPedido.addFocusListener(controlador);
        vista.comboClientePedido.addFocusListener(controlador);

    }


    /**
     * Método que nos configura los listeners de los botones.
     * @param listener
     */
    private void addActionListeners(ActionListener listener) {

        vista.btnAnadirProductoPedido.addActionListener(listener);
        vista.btnModificarProductoPedido.addActionListener(listener);
        vista.btnListarProductoPedido.addActionListener(listener);
        vista.btnEliminarProductoPedido.addActionListener(listener);



        vista.btnAnadirPedidoVendedor.addActionListener(listener);
        vista.btnModificarPedidoVendedor.addActionListener(listener);
        vista.btnListarPedidoVendedor.addActionListener(listener);
        vista.btnEliminarPedidoVendedor.addActionListener(listener);
        vista.btnGenerarFactura.addActionListener(listener);



        vista.btnAnadirCliente.addActionListener(listener);
        vista.btnModificarCliente.addActionListener(listener);
        vista.btnListarCliente.addActionListener(listener);
        vista.btnEliminarCliente.addActionListener(listener);

        vista.cerrarSesion.addActionListener(listener);


    }


    /**
     * Método que recibirá la señal de cuando usemos un botón. Según el action command que tenga, a través del switch, ejecutará
     * una serie de órdenes. También después de realizar las accciones configuradas, ejecutará los métodos que nos visualizará
     * los datos que tengamos en la base de datos en cada JList correspondiente.
     * @param e ActionEvent Recibe el dato emitido, normalmente al pulsar un botón.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();


        switch (comando) {

            /**
             *Cuando se active esta orden se comprobará si hay algún campo vacío. En caso contrario, nos emite una
             * ventana.
             * Se crea un CaracteristicasPedido. Se recojen los datos de cada caja de texto de la ficha y se modifica cada atributo.
             * Se da de alta.
             * Y se vuelve a utilizar el JList.
             *Hay distintos try-catch para cada situación. Si no hemos introducido el tipo de correcto, etc.
             */
            case "AnadirProductoPedido":


                if(vista.dcbmProductoCaracteristicasPedido.getSelectedItem()==null||
                        vista.dcbmPedidoCaracteristicasPedido.getSelectedItem()==null||
                        vista.txtCantidadProductoPedido.getText().equalsIgnoreCase("")){
                    JOptionPane.showMessageDialog(null, "Error, rellene todos los campos, por favor. ");
                }else {
                    CaracteristicasPedido nuevoCaracteristicasPedido = new CaracteristicasPedido();
                    nuevoCaracteristicasPedido.setProducto((Producto) vista.dcbmProductoCaracteristicasPedido.getSelectedItem());
                    nuevoCaracteristicasPedido.setPedido((Pedido) vista.dcbmPedidoCaracteristicasPedido.getSelectedItem());
                    try {
                        nuevoCaracteristicasPedido.setCantidad(Integer.parseInt(vista.txtCantidadProductoPedido.getText()));
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca un valor numérico entero en \"Cantidad\", por favor. ");
                        break;
                    }

                    modelo.altaCaracteristicasPedido(nuevoCaracteristicasPedido);
                    modificarTotalesPedidos(modelo.getPedidos());
                    listarPedidos(modelo.getPedidos());
                    listarCaracteristicasPedido(modelo.getCaracteristicasPedido());
                }
                break;
            /**
             * Cuando se ejecuta esta orden se modifica un CaracteristicasPedido seleccionado en el JList de CaracteristicasPedido.
             * Primero nos avisará de si no hemos seleccionado un CaracteristicasPedido que lo hagamos.
             * Después, comprueba si hemos rellenado todos los campos, que no hay ninguno vacío.
             * Cuando lo ha hecho, modifica los atributos que hemos puesto en la ficha del CaracteristicasPedido seleccionado.
             * Cuando termina, vuelve a listar el JList de CaracteristicasPedidos.
             */
            case "ModificarProductoPedido":
                CaracteristicasPedido caracteristicasPedidoSeleccion=null;
                try {
                    caracteristicasPedidoSeleccion = (CaracteristicasPedido) vista.listCaracteristicasPedido.getSelectedValue();
                    caracteristicasPedidoSeleccion.setProducto((Producto) vista.dcbmProductoCaracteristicasPedido.getSelectedItem());
                }catch (Exception ex){
                    JOptionPane.showMessageDialog(null, "Error, seleccione un CaracteristicasPedido" +
                            " para poder modificarlo, por favor. ");
                    break;
                }

                if(vista.dcbmProductoCaracteristicasPedido.getSelectedItem()==null||
                        vista.dcbmPedidoCaracteristicasPedido.getSelectedItem()==null||
                        vista.txtCantidadProductoPedido.getText().equalsIgnoreCase("")){
                    JOptionPane.showMessageDialog(null, "Error, seleccione un detalle de pedido, por favor. ");

                }else {
                    caracteristicasPedidoSeleccion=null;
                    try {
                        caracteristicasPedidoSeleccion = (CaracteristicasPedido) vista.listCaracteristicasPedido.getSelectedValue();
                        caracteristicasPedidoSeleccion.setProducto((Producto) vista.dcbmProductoCaracteristicasPedido.getSelectedItem());
                    }catch (Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, seleccione un CaracteristicasPedido" +
                                " para poder modificarlo, por favor. ");
                        break;
                    }
                    caracteristicasPedidoSeleccion.setPedido((Pedido) vista.dcbmPedidoCaracteristicasPedido.getSelectedItem());
                    try{
                        caracteristicasPedidoSeleccion.setCantidad(Integer.parseInt(vista.txtCantidadProductoPedido.getText()));
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca un valor numérico entero en \"Cantidad\", por favor. ");
                        break;
                    }
                    modelo.modificarCaracteristicasPedido(caracteristicasPedidoSeleccion);
                    modificarTotalesPedidos(modelo.getPedidos());
                    listarPedidos(modelo.getPedidos());
                    listarCaracteristicasPedido(modelo.getCaracteristicasPedido());
                }



                break;
            /**
             * Cuando se ejecuta esta orden, se nos ordena el JList de CaracteristicasPedido por nombre en orden alfabético.
             *
             */
            case "ListarProductoPedido":
                listarCaracteristicasPedidoPorCantidadMayorAMenor(modelo.getCaracteristicasPedidoPorCantidad());


                break;
            /**
             * Cuando se ejecuta este orden, se nos borrará un almacén que hayamos seleccionado.
             * También nos avisará con try-catch, si hay errores de persistencia o no hemos seleccionado
             * un almacén.
             * Se nos borrarán los campos de la ficha.
             */
            case "EliminarProductoPedido":
                try {
                    CaracteristicasPedido caracteristicaPedidoBorrado = (CaracteristicasPedido) vista.listCaracteristicasPedido.getSelectedValue();
                    modelo.borrarCaracteristicasPedido(caracteristicaPedidoBorrado);
                    modificarTotalesPedidos(modelo.getPedidos());
                    listarPedidos(modelo.getPedidos());
                    listarCaracteristicasPedido(modelo.getCaracteristicasPedido());
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null,"Error, seleccione un CaracteristicasPedido para " +
                            "poder borrar, por favor.");
                }
                limpiarCamposVistaCaracteristicasPedido();
                break;

            /**
             *Cuando se active esta orden se comprobará si hay algún campo vacío. En caso contrario, nos emite una
             * ventana.
             * Se crea un almacén. Se recojen los datos de cada caja de texto de la ficha y se modifica cada atributo.
             * Se da de alta.
             * Y se vuelve a utilizar el JList.
             *Hay distintos try-catch para cada situación. Si no hemos introducido el tipo de correcto, etc.
             */
            case "AnadirPedido":


                if(vista.dcbmClientePedido.getSelectedItem()==null||
                        vista.dateFechaCompraPedido.getDate()==null/*||
                        vista.txtPrecioTotalPedido.getText().equalsIgnoreCase("")*/){
                    JOptionPane.showMessageDialog(null, "Error, rellene todos los campos, por favor. ");

                }else{
                    Pedido nuevoPedido = new Pedido();
                    nuevoPedido.setCliente((Cliente)vista.dcbmClientePedido.getSelectedItem());
                    try {
                        nuevoPedido.setFechaCompra(Date.valueOf(vista.dateFechaCompraPedido.getDate()));
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca una fecha correctamente clicando en el botón de fecha " +
                                "y selecionando una, por favor. ");
                        break;
                    }
                    /*
                    try {
                        nuevoPedido.setPrecioTotal(Float.parseFloat(vista.txtPrecioTotalPedido.getText()));
                    }catch (Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca un valor númerico en \"Precio total\", por favor. ");
                        break;
                    }
                    */
                    nuevoPedido.setPrecioTotal(0);
                    nuevoPedido.setRealizado("no");
                    modelo.altaPedido(nuevoPedido);
                    modificarTotalesPedidos(modelo.getPedidos());
                    listarPedidos(modelo.getPedidos());
                }
                break;

            /**
             * Cuando se ejecuta esta orden se modifica un Pedido seleccionado en el JList de Pedidos.
             * Primero nos avisará de si no hemos seleccionado un Pedido que lo hagamos.
             * Después, comprueba si hemos rellenado todos los campos, que no hay ninguno vacío.
             * Cuando lo ha hecho, modifica los atributos que hemos puesto en la ficha del Pedido seleccionado.
             * Cuando termina, vuelve a listar el JList de Pedidos.
             */
            case "ModificarPedido":
                Pedido pedidoSeleccion =null;
                try{
                    pedidoSeleccion = (Pedido) vista.listPedido.getSelectedValue();
                    pedidoSeleccion.setCliente((Cliente) vista.dcbmClientePedido.getSelectedItem());
                }catch (Exception ex){
                    JOptionPane.showMessageDialog(null, "Error, seleccione un pedido" +
                            " para poder modificarlo, por favor. ");
                    break;
                }
                pedidoSeleccion =null;
                if(vista.dcbmClientePedido.getSelectedItem()==null||
                        vista.dateFechaCompraPedido.getDate()==null/*||
                        vista.txtPrecioTotalPedido.getText().equalsIgnoreCase("")*/){
                    JOptionPane.showMessageDialog(null, "Error, rellene todos los campos, por favor. ");

                }else {
                    try{
                        pedidoSeleccion = (Pedido) vista.listPedido.getSelectedValue();
                        pedidoSeleccion.setCliente((Cliente) vista.dcbmClientePedido.getSelectedItem());
                    }catch (Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, seleccione un pedido" +
                                " para poder modificarlo, por favor. ");
                        break;
                    }
                    pedidoSeleccion.setFechaCompra(Date.valueOf(vista.dateFechaCompraPedido.getDate()));
                    /*
                    try{
                        pedidoSeleccion.setPrecioTotal(Float.parseFloat(vista.txtPrecioTotalPedido.getText()));
                    }catch (Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca un valor númerico en \"Precio total\", por favor. ");
                        break;
                    }
                    */
                    modelo.modificarPedido(pedidoSeleccion);

                    listarPedidos(modelo.getPedidos());
                }
                break;

            /**
             * Cuando se ejecuta esta orden, se nos ordena el JList de CaracteristicasPedido por nombre en orden alfabético.
             *
             */
            case "ListarPedido":
                listarPedidoPorPrecioDeMayorAMenor(modelo.getPedidosOrdenadosPorPrecio());
                break;

            /**
             * Cuando se ejecuta este orden, se nos borrará un pedido que hayamos seleccionado.
             * También nos avisará con try-catch, si hay errores de persistencia o no hemos seleccionado
             * un almacén.
             * Se nos borrarán los campos de la ficha.
             */
            case "EliminarPedido":
                try{
                    Pedido pedidoBorrado = (Pedido) vista.listPedido.getSelectedValue();
                    modelo.borrarPedido(pedidoBorrado);
                    listarPedidos(modelo.getPedidos());
                }catch (PersistenceException ex){
                    JOptionPane.showMessageDialog(null,"Error de persistencia, tiene que borrar antes " +
                            "los \"Detalles de pedido (CaracterísticasPedido)\" asociados a este pedido, por favor.");
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null,"Error, seleccione un pedido para " +
                            "poder borrar, por favor.");
                }
                limpiarCamposVistaPedido();
                break;

            /**
             *Cuando se active esta orden se comprobará si hay algún campo vacío. En caso contrario, nos emite una
             * ventana.
             * Se crea un cliente. Se recojen los datos de cada caja de texto de la ficha y se modifica cada atributo.
             * Se da de alta.
             * Y se vuelve a utilizar el JList.
             *Hay distintos try-catch para cada situación. Si no hemos introducido el tipo de correcto, etc.
             */
            case "AnadirCliente":
                if(vista.txtNombreCliente.getText().equalsIgnoreCase("")||
                        vista.txtApellidosCliente.getText().equalsIgnoreCase("")||
                        vista.txtTelefonoCliente.getText().equalsIgnoreCase("")||
                        vista.txtCorreoElectronicoCliente.getText().equalsIgnoreCase("")||
                        vista.txtNumeroTarjetaCliente.getText().equalsIgnoreCase("")||
                        vista.txtPaisCliente.getText().equalsIgnoreCase("")||
                        vista.txtCiudadCliente.getText().equalsIgnoreCase("")||
                        vista.txtCalleCliente.getText().equalsIgnoreCase("")||
                        vista.txtCodigoPostalCliente.getText().equalsIgnoreCase("")||
                        vista.txtDNICliente.getText().equalsIgnoreCase("")){
                    JOptionPane.showMessageDialog(null, "Error, rellene todos los campos, por favor. ");
                }else{
                    Cliente nuevoCliente = new Cliente();
                    nuevoCliente.setNombre(vista.txtNombreCliente.getText());
                    nuevoCliente.setApellidos(vista.txtApellidosCliente.getText());
                    try{
                        nuevoCliente.setTelefono(Integer.parseInt(vista.txtTelefonoCliente.getText()));
                    }catch (Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca un valor númerico entero en \"Teléfono\", por favor. ");
                        break;
                    }

                    nuevoCliente.setCorreoElectronico(vista.txtCorreoElectronicoCliente.getText());
                    nuevoCliente.setNumeroTarjeta(vista.txtNumeroTarjetaCliente.getText());
                    nuevoCliente.setPais(vista.txtPaisCliente.getText());
                    nuevoCliente.setCiudad(vista.txtCiudadCliente.getText());
                    nuevoCliente.setCalle(vista.txtCalleCliente.getText());
                    nuevoCliente.setCodigoPostal(vista.txtCodigoPostalCliente.getText());
                    nuevoCliente.setDni(vista.txtDNICliente.getText());
                    try{
                        modelo.altaCliente(nuevoCliente);
                    }catch (PersistenceException ex){
                        JOptionPane.showMessageDialog(null, "Error, DNI repetido. Sólo puede haber un único DNI, gracias.");
                    }
                    listarClientes(modelo.getClientes());
                }
                break;

            /**
             * Cuando se ejecuta esta orden se modifica un cliente seleccionado en el JList de clientes.
             * Primero nos avisará de si no hemos seleccionado un cliente que lo hagamos.
             * Después, comprueba si hemos rellenado todos los campos, que no hay ninguno vacío.
             * Cuando lo ha hecho, modifica los atributos que hemos puesto en la ficha del cliente seleccionado.
             * Cuando termina, vuelve a listar el JList de clientes.
             */
            case "ModificarCliente":
                Cliente clienteSeleccion =null;
                try{
                    clienteSeleccion = (Cliente) vista.listCliente.getSelectedValue();
                    clienteSeleccion.setNombre(vista.txtNombreCliente.getText());
                }catch (Exception ex){
                    JOptionPane.showMessageDialog(null, "Error, seleccione un cliente" +
                            " para poder modificarlo, por favor. ");
                    break;
                }
                if(vista.txtNombreCliente.getText().equalsIgnoreCase("")||
                        vista.txtApellidosCliente.getText().equalsIgnoreCase("")||
                        vista.txtTelefonoCliente.getText().equalsIgnoreCase("")||
                        vista.txtCorreoElectronicoCliente.getText().equalsIgnoreCase("")||
                        vista.txtNumeroTarjetaCliente.getText().equalsIgnoreCase("")||
                        vista.txtPaisCliente.getText().equalsIgnoreCase("")||
                        vista.txtCiudadCliente.getText().equalsIgnoreCase("")||
                        vista.txtCalleCliente.getText().equalsIgnoreCase("")||
                        vista.txtCodigoPostalCliente.getText().equalsIgnoreCase("")||
                        vista.txtDNICliente.getText().equalsIgnoreCase("")){
                    JOptionPane.showMessageDialog(null, "Error, rellene todos los campos, por favor. ");
                }else {
                    clienteSeleccion =null;
                    try{
                        clienteSeleccion = (Cliente) vista.listCliente.getSelectedValue();
                        clienteSeleccion.setNombre(vista.txtNombreCliente.getText());
                    }catch (Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, seleccione un cliente" +
                                " para poder modificarlo, por favor. ");
                        break;
                    }
                    clienteSeleccion.setApellidos(vista.txtApellidosCliente.getText());

                    try{
                        clienteSeleccion.setTelefono(Integer.parseInt(vista.txtTelefonoCliente.getText()));
                    }catch (Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca un valor númerico entero en \"Teléfono\", por favor. ");
                        break;
                    }
                    clienteSeleccion.setCorreoElectronico(vista.txtCorreoElectronicoCliente.getText());
                    clienteSeleccion.setNumeroTarjeta(vista.txtNumeroTarjetaCliente.getText());
                    clienteSeleccion.setPais(vista.txtPaisCliente.getText());
                    clienteSeleccion.setCiudad(vista.txtCiudadCliente.getText());
                    clienteSeleccion.setCalle(vista.txtCalleCliente.getText());
                    clienteSeleccion.setCodigoPostal(vista.txtCodigoPostalCliente.getText());
                    clienteSeleccion.setDni(vista.txtDNICliente.getText());
                    try{
                        modelo.modificarCliente(clienteSeleccion);
                    }catch (PersistenceException ex){
                        JOptionPane.showMessageDialog(null, "Error, DNI repetido. Sólo puede haber un único DNI, gracias.");
                    }
                    listarClientes(modelo.getClientes());
                }
                break;

            /**
             * Cuando se ejecuta esta orden, se nos ordena el JList de Clientes por nombre en orden alfabético.
             *
             */
            case "ListarCliente":
                listarClientesPorOrdenAlfabetico(modelo.getClientesPorOrdenAlfabetico());
                System.out.println("LISTADO-------------------------->");
                break;

            /**
             * Cuando se ejecuta este orden, se nos borrará un cliente que hayamos seleccionado.
             * También nos avisará con try-catch, si hay errores de persistencia o no hemos seleccionado
             * un cliente.
             * Se nos borrarán los campos de la ficha.
             */
            case "EliminarCliente":
                try{
                    Cliente clienteBorrado = (Cliente) vista.listCliente.getSelectedValue();
                    modelo.borrarCliente(clienteBorrado);
                    listarClientes(modelo.getClientes());
                }catch (PersistenceException ex){
                    JOptionPane.showMessageDialog(null,"Error de persistencia, tiene que borrar antes " +
                            "los pedidos asociados a este cliente, por favor.");
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null,"Error, seleccione un cliente para " +
                            "poder borrar, por favor.");
                }
                limpiarCamposVistaCliente();
                break;

            /**
             * Cuando se ejecute esta orden, si hemos seleccionado una factura se nos creará un informe a través de JasperReport
             * Si nos hemos seleccionado un pedido, nos avisará con un JOptionPane
             */
            case "generarFactura":
                JasperPrint informeLleno2=null;
                try {
                    pedidoSeleccion = (Pedido) vista.listPedido.getSelectedValue();
                    informeLleno2 = ReportGenerator.generarInformePorPedido(pedidoSeleccion.getId());
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Error, seleccione un pedido para " +
                            "poder realizar una factura, por favor. ");
                    break;
                }
                // System.out.println(pedidoSeleccion.getId());


                // JasperPrint informeLleno = ReportGenerator.generarInformePeliculas();
                /*
                 JasperViewer viewer = new JasperViewer(informeLleno2);
                 viewer.setVisible(true);
                   */
                try {
                    JasperExportManager.exportReportToPdfFile(informeLleno2, "InformePorPedido.pdf");
                } catch (JRException ex) {
                    // TODO Auto-generated catch block
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Error, no pudimos crear el archivo en PDF, " +
                            "disculpe las molestias. ");
                }
                break;

            /**
             * Cuando se ejecute esta orden, se nos enviará al login que se inicia cuando abrimos el programa.
             */
            case "CerrarSesion":
                vista.dispose();
                Login2 login = new Login2();
                break;
        }
    }



    /**
     * Método que nos lista los productos que hay en la ficha CaracteristicasPedido en el combobox.
     *
     */
    private void listarProductosCaractPedido(){
        java.util.List<Producto> listaProductos = modelo.getProductos();
        vista.dcbmProductoCaracteristicasPedido.removeAllElements();
        for(Producto producto : listaProductos){
            vista.dcbmProductoCaracteristicasPedido.addElement(producto);
        }
    }

    /**
     * Método que nos lista los Pedidos en la ficha CaracterísticasPedido en el combobox.
     */
    private void listarPedidosCaractPedido(){
        List<Pedido> listaPedidos = modelo.getPedidos();
        vista.dcbmPedidoCaracteristicasPedido.removeAllElements();
        for(Pedido pedido : listaPedidos){
            vista.dcbmPedidoCaracteristicasPedido.addElement(pedido);
        }
    }


    /**
     * Método que nos calcula el PrecioTotal de un pedido. Recibe un arraylist de CaracteristicasPedido.
     * @param list Objeto de arraylist.
     * @return Objeto tipo double
     */
    private double calcularPrecioTotalPedido(ArrayList<CaracteristicasPedido> list){
        double precioTotal=0;
        double precioSubTotal=0;
        for (int i=0;i<list.size();i++){
            precioSubTotal=0;
            precioSubTotal=list.get(i).getCantidad()*list.get(i).getProducto().getPrecio();
            precioTotal+=precioSubTotal;
        }
        return precioTotal;
    }


    /**
     * Método que nos modifica los totales de cada pedido. Recibe un ArrayList de Pedido.
     * @param list Objeto tipo ArrayList<Pedido>
     */
    private void modificarTotalesPedidos(ArrayList<Pedido> list){
        for(int i=0;i<list.size();i++){
            double precioTotal=0;
            precioTotal=calcularPrecioTotalPedido(modelo.getCaracteristicasPedidoDeUnPedido(list.get(i)));
            list.get(i).setPrecioTotal(precioTotal);
            modelo.modificarPedido(list.get(i));
        }
    }


    /**
     * Método que nos lista las CaracterísticasPedido de la ficha CaracterísticasPedido. Recibe un arraylist de
     * CaracterísticasPedido
     *
     * @param lista Objeto tipo ArrayList<CaracteristicasPedido>
     */
    public void listarCaracteristicasPedido (ArrayList<CaracteristicasPedido> lista){
        vista.dlmCaracteristicasPedidos.clear();
        for (CaracteristicasPedido unCaracteristicasPedido : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmCaracteristicasPedidos.addElement(unCaracteristicasPedido);
        }

    }

    /**
     * Método que nos lista las CaracteristicasPedido de CaracteristicasPedido según la cantidad de mayor a menor.
     * @param lista Objeto tipo ArrayList<CaracteristicasPedido>
     */
    public void listarCaracteristicasPedidoPorCantidadMayorAMenor (ArrayList<CaracteristicasPedido> lista){
        vista.dlmCaracteristicasPedidos.clear();
        for (CaracteristicasPedido unCaracteristicasPedido : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmCaracteristicasPedidos.addElement(unCaracteristicasPedido);
        }
    }


    public void listarProductosCaracteristicasPedido(ArrayList<Producto> lista){
        vista.dlmProductoCaracteristicasPedido.clear();
        for (Producto unProducto : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmProductoCaracteristicasPedido.addElement(unProducto);
        }
    }

    public void listarPedidosCaracteristicasPedido(ArrayList<Pedido> lista){
        vista.dlmPedidoCaracteristicasPedido.clear();
        for (Pedido unPedido : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmPedidoCaracteristicasPedido.addElement(unPedido);
        }
    }



    /**
     * Método que nos lista los pedidos en la ficha Pedidos
     * @param lista Objeto tipo ArrayList<Pedido>
     */
    public void listarPedidos (ArrayList<Pedido> lista){
        vista.dlmPedido.clear();
        for (Pedido unPedido : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmPedido.addElement(unPedido);
        }
    }

    /**
     * Método que nos lista los pedidos en la ficha Pedidos según el precio de mayor a menor
     * @param lista Objeto de ArrayList<Pedido>
     */
    public void listarPedidoPorPrecioDeMayorAMenor (ArrayList<Pedido> lista){
        vista.dlmPedido.clear();
        for (Pedido unPedido : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmPedido.addElement(unPedido);
        }
    }

    /**
     * Método que nos las CaracterísticasPedido en la ficha Pedidos. Recibe un arraylist de CaracteristicasPedido
     * @param lista Objeto de ArrayList
     */
    public void listarCaracteristicasPedidoPedido(ArrayList<CaracteristicasPedido> lista){
        vista.dlmCaracteristicasPedidoPedido.clear();
        for(CaracteristicasPedido unCaracteristicasPedido: lista){
            vista.dlmCaracteristicasPedidoPedido.addElement(unCaracteristicasPedido);
        }
    }

    public void listarProductosCaractPedidoPedido(ArrayList<Producto> lista){
        vista.dlmProductoCaracteristicasPedidoPedido.clear();
        for(Producto unProducto:lista){
            vista.dlmProductoCaracteristicasPedidoPedido.addElement(unProducto);
        }
    }



    /**
     * Método que nos lista los clientes de la ficha Clientes. Recibe un ArrayList de tipo Cliente
     * @param lista Objeto de ArrayList
     */
    public void listarClientes (ArrayList<Cliente> lista){
        vista.dlmCliente.clear();
        for (Cliente unCliente : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmCliente.addElement(unCliente);
        }
    }

    /**
     * Método que nos lista los clientes de la ficha Cliente por orden alfabético. Recibe un ArrayList de tipo Cliente.
     * @param lista Objeto de ArrayList
     */
    public void listarClientesPorOrdenAlfabetico (ArrayList<Cliente> lista){
        vista.dlmCliente.clear();
        for (Cliente unCliente : lista) {
            //System.out.println(unProveedor.getCorreo());
            System.out.println(unCliente);
            vista.dlmCliente.addElement(unCliente);
        }
    }

    /**
     * Método que nos lista los pedidos que hizo un cliente. Recibe un arraylist de tipo Pedido.
     * @param lista Objeto tipo ArrayList<Pedido>
     */
    public void listarPedidosCliente(ArrayList<Pedido> lista){
        vista.dlmPedidosCliente.clear();
        for(Pedido unPedido : lista) {

            vista.dlmPedidosCliente.addElement(unPedido);
        }
    }



    /**
     * Método que nos rellenará el combobox de Cliente que hay en la ficha Pedido.
     */
    private void listarClientesPedido(){
        List<Cliente> listaClientes = modelo.getClientes();
        vista.dcbmClientePedido.removeAllElements();
        for(Cliente cliente : listaClientes){
            vista.dcbmClientePedido.addElement(cliente);
        }
    }


    /**
     * Método que nos permite limpiar los campos de la ficha CaracteristicasPedido
     */
    public void limpiarCamposVistaCaracteristicasPedido(){
        vista.dcbmProductoCaracteristicasPedido.setSelectedItem(null);
        vista.dcbmPedidoCaracteristicasPedido.setSelectedItem(null);
        vista.txtCantidadProductoPedido.setText("");
    }

    /**
     * Método que nos permite limpiar los campos de la ficha Pedido
     */
    public void limpiarCamposVistaPedido(){
        vista.dcbmClientePedido.setSelectedItem(null);
        vista.dateFechaCompraPedido.setDate(null);
        vista.txtPrecioTotalPedido.setText(null);
    }


    /**
     * Método que nos permite limpiar los campos de la ficha Cliente
     */
    public void limpiarCamposVistaCliente(){
        vista.txtNombreCliente.setText("");
        vista.txtApellidosCliente.setText("");
        vista.txtTelefonoCliente.setText("");
        vista.txtCorreoElectronicoCliente.setText("");
        vista.txtNumeroTarjetaCliente.setText("");
        vista.txtPaisCliente.setText("");
        vista.txtCiudadCliente.setText("");
        vista.txtCalleCliente.setText("");
        vista.txtCodigoPostalCliente.setText("");
        vista.txtDNICliente.setText("");
    }

    /**
     *Método que nos calcula los precios totales de Pedidos
     */
    public void calcularPrecioTotalesDeTodos(){
        ArrayList<Pedido> lista= modelo.getPedidos();
        for(Pedido pedido: lista){
            Float precioTotal=(float)calcularPrecioTotalPedido(modelo.getCaracteristicasPedidoDeUnPedido(pedido));
            //System.out.println(precioTotal+"--------------------->PRECIO TOTAL");
            pedido.setPrecioTotal(precioTotal);
            modelo.modificarPedido(pedido);
        }
    }

    /**
     * Método que nos cargar los datos en cada comobobox del programa al ciclar sobre él.
     */
    @Override
    public void focusGained(FocusEvent e) {
        if(e.getSource() == vista.comboProductoCaractPedido) {
            vista.comboProductoCaractPedido.hidePopup();
            listarProductosCaractPedido();
            vista.comboProductoCaractPedido.showPopup();
        }else if(e.getSource() == vista.comboPedidoCaractPedido) {
            vista.comboPedidoCaractPedido.hidePopup();
            listarPedidosCaractPedido();
            vista.comboPedidoCaractPedido.showPopup();
        }else if(e.getSource() == vista.comboClientePedido) {
            vista.comboClientePedido.hidePopup();
            listarClientesPedido();
            vista.comboClientePedido.showPopup();
        }
    }

    @Override
    public void focusLost(FocusEvent e) {

    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void stateChanged(ChangeEvent e) {

    }


    /**
     * Método que nos detectará cuando cliquemos sobre un registro de cada JList del programa.
     * @param e Objeto tipo ListSelectionEvent
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){
            /**
             * Nos rellena los campos de la ficha CaracteristicasPedido.
             *
             */
            if(e.getSource() == vista.listCaracteristicasPedido) {
                CaracteristicasPedido caracteristicasPedidoSeleccion = (CaracteristicasPedido) vista.listCaracteristicasPedido.getSelectedValue();
                vista.dcbmProductoCaracteristicasPedido.setSelectedItem(caracteristicasPedidoSeleccion.getProducto());
                vista.dcbmPedidoCaracteristicasPedido.setSelectedItem(caracteristicasPedidoSeleccion.getPedido());
                vista.txtCantidadProductoPedido.setText(String.valueOf(caracteristicasPedidoSeleccion.getCantidad()));
            }
            /**
             * Nos rellena los campos de la ficha Pedido. Nos lista sus caracteristicaspedido en su JList
             */
            if(e.getSource() == vista.listPedido) {
                Pedido pedidoSeleccion = (Pedido) vista.listPedido.getSelectedValue();
                Float precioTotal=(float)calcularPrecioTotalPedido(modelo.getCaracteristicasPedidoDeUnPedido(pedidoSeleccion));
                //System.out.println(precioTotal+"--------------------->PRECIO TOTAL");
                pedidoSeleccion.setPrecioTotal(precioTotal);
                vista.dcbmClientePedido.setSelectedItem(pedidoSeleccion.getCliente());
                vista.dateFechaCompraPedido.setDate(pedidoSeleccion.getFechaCompra().toLocalDate());
                vista.txtPrecioTotalPedido.setText(String.valueOf(pedidoSeleccion.getPrecioTotal()));
                modelo.modificarPedido(pedidoSeleccion);
                listarCaracteristicasPedidoPedido(modelo.getCaracteristicasPedidoPedido(pedidoSeleccion));
                vista.dlmProductoCaracteristicasPedidoPedido.clear();
            }
            /**
             * Nos rellena el JList de ProducotoCaracteristicasPedido en la ficha Pedido y nos saca el producto.
             */
            if(e.getSource() == vista.listCaracteristicasPedidoPedidoVendedor) {
                CaracteristicasPedido caracteristicasPedidoSeleccion = (CaracteristicasPedido) vista.listCaracteristicasPedidoPedidoVendedor.getSelectedValue();
                vista.dlmProductoCaracteristicasPedidoPedido.clear();
                vista.dlmProductoCaracteristicasPedidoPedido.addElement(caracteristicasPedidoSeleccion.getProducto().toString());
            }

            /**
             * Nos rellena los campos del JList de la ficha Cliente. Y también nos muestra en otro JList los pedidos que ha realizado.
             */
            if(e.getSource() == vista.listCliente) {
                Cliente clienteSeleccion = (Cliente) vista.listCliente.getSelectedValue();
                vista.txtNombreCliente.setText(clienteSeleccion.getNombre());
                vista.txtApellidosCliente.setText(clienteSeleccion.getApellidos());
                vista.txtTelefonoCliente.setText(String.valueOf(clienteSeleccion.getTelefono()));
                vista.txtCorreoElectronicoCliente.setText(clienteSeleccion.getCorreoElectronico());
                vista.txtNumeroTarjetaCliente.setText(clienteSeleccion.getNumeroTarjeta());
                vista.txtPaisCliente.setText(clienteSeleccion.getPais());
                vista.txtCiudadCliente.setText(clienteSeleccion.getCiudad());
                vista.txtCalleCliente.setText(clienteSeleccion.getCalle());
                vista.txtCodigoPostalCliente.setText(clienteSeleccion.getCodigoPostal());
                vista.txtDNICliente.setText(clienteSeleccion.getDni());
                listarPedidosCliente(modelo.getPedidosCliente(clienteSeleccion));
            }

        }
    }
}

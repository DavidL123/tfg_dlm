/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;


/**
 * Clase ventana que nos permitirá crear usuarios.
 */
public class Login5 extends JFrame{
    private JTextField txtNuevoUsuarioLogin5;
    private JTextField txtContrasenaLogin5;
    private JTextField txtRepiteContrasenaLogin5;
    private JButton btnResetLogin5;
    private JButton btnCrearUsuario5;
    private JButton btnCancelar5;
    private JPanel panel2;
    private JRadioButton radioButtonAdministrador;
    private JRadioButton radioButtonVendedor;
    private JRadioButton radioButtonEmpaquetador;
    private ButtonGroup buttonGroup1;


    /**
     * Constructor de la ventana que nos permite crear usuarios. Tiene sus configuraciones.
     *Tiene 3 botones:
     * -Reset: Nos limpia los campos.
     * -Crear usuario: Nos crea un usuario.
     * -Cancelar: Nos vuelve al menú de atrás.
     */
    public Login5(){
        this.setBounds(100,100,500,300);
        this.setResizable(false);
        this.setContentPane(panel2);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        btnCancelar5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                Login3 login = new Login3();
            }
        });

        btnCrearUsuario5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String contrasena=txtContrasenaLogin5.getText();
                String contrasenaRe=txtRepiteContrasenaLogin5.getText();
                if(contrasena.equalsIgnoreCase(contrasenaRe)){
                    if(!txtNuevoUsuarioLogin5.getText().equals("")){
                        if(!radioButtonAdministrador.isSelected()&&!radioButtonVendedor.isSelected()
                                &&!radioButtonEmpaquetador.isSelected()){
                            Login13RadioButtonNoSelected login = new Login13RadioButtonNoSelected();
                        }else if(comprobarUsuario()){
                            Login7UsuarioYaCreado login = new Login7UsuarioYaCreado();
                            txtNuevoUsuarioLogin5.setText(null);
                            txtContrasenaLogin5.setText(null);
                            txtRepiteContrasenaLogin5.setText(null);

                        }else{
                            crearUsuario();
                            Login6UsuarioCreado login = new Login6UsuarioCreado();

                        }
                    }else{
                      Login8NombreUsuarioVacio login = new Login8NombreUsuarioVacio();
                    }
                }else{

                    Login11ContrasenasNoCoinciden login = new Login11ContrasenasNoCoinciden();
                    txtNuevoUsuarioLogin5.setText(null);
                    txtContrasenaLogin5.setText(null);
                    txtRepiteContrasenaLogin5.setText(null);
                }
            }
        });

        btnResetLogin5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                txtNuevoUsuarioLogin5.setText(null);
                txtContrasenaLogin5.setText(null);
                txtRepiteContrasenaLogin5.setText(null);
                

            }
        });



    }

    /**
     * Método que nos comprueba si existe el usuario que hay en la base de datos.
     * @return
     */
    public boolean comprobarUsuario() {
        String usuario=txtNuevoUsuarioLogin5.getText();
        boolean existeUser=false;
        try {
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/baseecommerce?serverTimezone=UTC",
                    "root","");

            Statement st =con.createStatement();

            String sentencia = "SELECT nombre_usuario, contrasena FROM usuarios";
            PreparedStatement PSt = con.prepareStatement(sentencia);

            ResultSet rs=PSt.executeQuery();
            while(rs.next()){
                if(rs.getString(1).equals(usuario)){
                    existeUser=true;
                    System.out.println("Existe");
                }
            }
            if(rs!=null){
                rs.close();
            }

        } catch (SQLException ex) {
            // TODO Auto-generated catch block
            ex.printStackTrace();
        }
        return existeUser;
    }

    /**
     * Método que nos ejecutará una sentencia que nos permitirá crear un nuevo usuario y su contraseña
     * en nuestra base de datos.
     */
    public void crearUsuario(){
        String nombreUsuario=txtNuevoUsuarioLogin5.getText();
        String contrasena=txtContrasenaLogin5.getText();
        String tipoUsuario="";
        if(radioButtonAdministrador.isSelected()){
            tipoUsuario="administrador";
        }else if(radioButtonVendedor.isSelected()){
            tipoUsuario="vendedor";
        }else if(radioButtonEmpaquetador.isSelected()){
            tipoUsuario="empaquetador";
        }
        try {
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/baseecommerce?serverTimezone=UTC",
                    "root","");
            Statement st =con.createStatement();
            String sentencia = "INSERT INTO usuarios (nombre_usuario, contrasena,tipo_usuario) VALUES (?, ?, ?);";
            PreparedStatement PSt = con.prepareStatement(sentencia);
            PSt.setString(1, nombreUsuario);
            PSt.setString(2, contrasena);
            PSt.setString(3, tipoUsuario);
            boolean existeUserAndPass=false;
            PSt.executeUpdate();
            if(PSt!=null){
                PSt.close();
            }
        } catch (SQLException ex) {
            // TODO Auto-generated catch block
            ex.printStackTrace();
        }
    }
}

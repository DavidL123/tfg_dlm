/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.gui;

import javax.swing.*;


/**
 * Clase vista del programa del usuario Empaquetador
 */
public class VistaEmpaquetador extends JFrame{
    private JPanel panel1;
     JList listPedido;
     JList listCaracteristicasPedido;
     JButton btnCompletadoVistaEmpaq;
    JList listProductosCaracteristicasPedido;



    DefaultListModel dlmPedido;
    DefaultListModel dlmCaracteristicasPedidos;
    DefaultListModel dlmProductoCaracteristicasPedido;

    JMenuItem cerrarSesion;

    /**
     * Constructor de la vista del empaquetador. Tienen sus configuraciones y se ejecutan varios métodos.
     *  - Tiene las configuraciones de la Ventana
     *  - Configura los DefaultListModel con los JList
     *  - Configura los DefaultComboBoxModel con los JComboBox
     */
    public VistaEmpaquetador(){
        this.setContentPane(panel1);


        this.setExtendedState(this.MAXIMIZED_BOTH);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);

        crearModelos();
        crearMenus();
    }

    /**
     * Método para inicialiar los DefaultListModel y configurarlos.
     */
    private void crearModelos() {


        dlmCaracteristicasPedidos=new DefaultListModel();
        listCaracteristicasPedido.setModel(dlmCaracteristicasPedidos);

        dlmPedido=new DefaultListModel();
        listPedido.setModel(dlmPedido);

        dlmProductoCaracteristicasPedido=new DefaultListModel();
        listProductosCaracteristicasPedido.setModel(dlmProductoCaracteristicasPedido);



    }

    /**
     * Método para crear los botones en la barra de menú con JMenuItem, JMenu y JMenuBar.
     */
    private void crearMenus(){
        /*
        itemConectar= new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");

         */
        cerrarSesion= new JMenuItem("Cerrar Sesión");
        cerrarSesion.setActionCommand("CerrarSesion");
        JMenu menuArchivo = new JMenu ("Archivo");

        // menuArchivo.add(itemConectar);
        menuArchivo.add(cerrarSesion);
        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);
        setJMenuBar(barraMenu);
    }
}

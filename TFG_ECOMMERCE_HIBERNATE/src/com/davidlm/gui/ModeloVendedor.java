/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.gui;

import com.davidlm.base.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.ArrayList;

/**
 * Clase Modelo del Modelo, Vista, Controlador de usuario tipo Vendedor.
 */
public class ModeloVendedor {


    SessionFactory sessionFactory;

    /**
     * Método que nos permite desconectar del SessionFactory.
     */
    public void desconectar() {
        //Cierro la factoria de sessiones
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    /**
     * Método que nos permitirá conectar con la base de datos.
     */
    public void conectar() {
        Configuration configuracion = new Configuration();

        configuracion.configure("hibernate.cfg.xml");

        configuracion.addAnnotatedClass(Almacen.class);
        configuracion.addAnnotatedClass(Cliente.class);
        configuracion.addAnnotatedClass(CaracteristicasPedido.class);
        configuracion.addAnnotatedClass(Pedido.class);
        configuracion.addAnnotatedClass(Producto.class);
        configuracion.addAnnotatedClass(Proveedor.class);
        String user="root";
        configuracion.setProperty("hibernate.connection.username",user);
        configuracion.setProperty("hibernate.connection.password","");
        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();


        sessionFactory = configuracion.buildSessionFactory(ssr);

    }



    /**
     * Método que nos permite dar de alta un CaracteristicasPedido
     */
    public void altaCaracteristicasPedido(CaracteristicasPedido nuevoCaracteristicaPedido) {

        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoCaracteristicaPedido);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Método que nos permite modificar un CaracteristicasPedido
     * @param caracteristicasPedidoSeleccion Objeto de la clase CaracteristicasPedido
     */
    public void modificarCaracteristicasPedido(CaracteristicasPedido caracteristicasPedidoSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(caracteristicasPedidoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }


    /**
     * Método que nos borra un CaracteristicasPedido
     * @param caracteristicasPedidoSeleccion Objeto de la clase CaracteristicasPedido
     */
    public void borrarCaracteristicasPedido(CaracteristicasPedido caracteristicasPedidoSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(caracteristicasPedidoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos devuelve un arraylist de las CaracteristicasPedido
     * @return Objeto ArrayList<CaracteristicasPedido>
     */
    public ArrayList<CaracteristicasPedido> getCaracteristicasPedido() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM CaracteristicasPedido ");
        ArrayList<CaracteristicasPedido> listaCaracteristicaPedido = (ArrayList<CaracteristicasPedido>)query.getResultList();
        sesion.close();
        return listaCaracteristicaPedido;
    }

    /**
     * Método que nos devuelve un arraylist de caracteristicaspedido ordenador por cantidad de mayor a menor.
     * @return Objeto ArrayList<CaracteristicasPedido>
     */
    public ArrayList<CaracteristicasPedido> getCaracteristicasPedidoPorCantidad() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM CaracteristicasPedido ORDER BY cantidad ASC ");
        ArrayList<CaracteristicasPedido> listaCaracteristicasPedido = (ArrayList<CaracteristicasPedido>)query.getResultList();
        sesion.close();
        return listaCaracteristicasPedido;
    }






    /**
     * Método que nos da de alta un Pedido
     * @param nuevoPedido Objeto de la clase Pedido
     */
    public void altaPedido(Pedido nuevoPedido) {

        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoPedido);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Método que nos modifica un pedido
     * @param pedidoSeleccion Objeto de la clase Pedido
     */
    public void modificarPedido(Pedido pedidoSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(pedidoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos borra un pedido
     * @param pedidoSeleccion Objeto de la clase Pedido
     */
    public void borrarPedido(Pedido pedidoSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(pedidoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos devuelve un arraylist con los pedidos de la base de datos.
     * @return Objeto ArrayList<Pedido>
     */
    public ArrayList<Pedido> getPedidos() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Pedido ");
        ArrayList<Pedido> listaPedidos = (ArrayList<Pedido>)query.getResultList();
        sesion.close();
        return listaPedidos;
    }

    /**
     * Método que nos devuelve un arraylist con los pedidos ordenados de mayor a menor.
     * @return Objeto ArrayList<Pedido>
     */
    public ArrayList<Pedido> getPedidosOrdenadosPorPrecio() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Pedido ORDER BY precioTotal ASC ");
        ArrayList<Pedido> listaPedidoOrdenados = (ArrayList<Pedido>)query.getResultList();
        sesion.close();
        return listaPedidoOrdenados;
    }


    /**
     * Método que nos devuelve un arraylist de las caracteristicas de un pedido que recibe.
     * @param unPedido Objeto de la clase Pedido
     * @return Objeto ArrayList<Pedido>
     */
    public ArrayList<CaracteristicasPedido> getCaracteristicasPedidoPedido(Pedido unPedido) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM CaracteristicasPedido WHERE pedido.id="+unPedido.getId());
        ArrayList<CaracteristicasPedido> listaCaracteristicasPedido= (ArrayList<CaracteristicasPedido>)query.getResultList();
        sesion.close();
        return listaCaracteristicasPedido;
    }


    /**
     * Método que nos devuelve un arraylist de productos.
     * @return
     */
    public ArrayList<Producto> getProductos() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Producto ");
        ArrayList<Producto> listaProductos = (ArrayList<Producto>)query.getResultList();
        sesion.close();
        return listaProductos;
    }



    /**
     * Método que nos genera un nuevo cliente.
     * @param nuevoCliente Objeto de clase Cliente
     */
    public void altaCliente(Cliente nuevoCliente) {

        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoCliente);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Método que modifica un cliente.
     * @param clienteSeleccion Objeto de clase Cliente
     */
    public void modificarCliente(Cliente clienteSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(clienteSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos borra un cliente.
     * @param clienteSeleccion Objeto de clase Cliente
     */
    public void borrarCliente(Cliente clienteSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(clienteSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos devuelve un arraylist de clientes.
     * @return Objeto ArrayList<Cliente>
     */
    public ArrayList<Cliente> getClientes() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Cliente ");
        ArrayList<Cliente> listaClientes = (ArrayList<Cliente>)query.getResultList();
        sesion.close();
        return listaClientes;
    }

    /**
     * Método que nos devuelve un arraylist de los clientes ordenados por nombre alfabéticamente.
     * @return Objeto ArrayList<Cliente>
     */
    public ArrayList<Cliente> getClientesPorOrdenAlfabetico() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Cliente ORDER BY nombre ASC ");
        ArrayList<Cliente> listaClientesOrdenados = (ArrayList<Cliente>)query.getResultList();
        sesion.close();
        return listaClientesOrdenados;
    }

    /**
     * Método que nos devuelve un arraylist con los pedidos de un cliente que recibe.
     * @param unCliente Objeto de la clase Cliente.
     * @return Objeto ArrayList<Cliente>
     */
    public ArrayList<Pedido> getPedidosCliente(Cliente unCliente) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Pedido WHERE cliente.id="+unCliente.getId());
        ArrayList<Pedido> listaPedidosCliente= (ArrayList<Pedido>)query.getResultList();
        sesion.close();
        return listaPedidosCliente;
    }

    /**
     * Método que nos devuelve un arraylist de las caracteristicaspedido de un pedido
     * @param unPedido Objeto de la clase Pedido.
     * @return Objeto ArrayList<Cliente>
     */
    public ArrayList<CaracteristicasPedido> getCaracteristicasPedidoDeUnPedido(Pedido unPedido) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM CaracteristicasPedido WHERE pedido.id = "+unPedido.getId());
        ArrayList<CaracteristicasPedido> listaCaracteristicaPedido = (ArrayList<CaracteristicasPedido>)query.getResultList();
        sesion.close();
        return listaCaracteristicaPedido;
    }


}

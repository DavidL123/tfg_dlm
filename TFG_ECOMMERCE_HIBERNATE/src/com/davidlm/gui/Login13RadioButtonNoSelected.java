/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Clase ventana que nos avisa de que no has seleccionado un radiobutton al crear un usuario.
 */
public class Login13RadioButtonNoSelected extends JFrame{
    private JButton btnAceptarLogin13;
    private JPanel panel1;

    /**
     * Constructor de la ventana que nos avisa. Tiene las configuraciones.
     * Si clicamos en Aceptar desaparece la ventana.
     */
    public Login13RadioButtonNoSelected(){
        this.setBounds(100,100,500,300);
        this.setResizable(false);
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);

        btnAceptarLogin13.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
}

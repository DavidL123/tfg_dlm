/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.gui;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Clase ventana que nos permite borrar usuarios.
 */
public class Login14BorrarUsuario extends JFrame{
     JPanel panel1;
     JTable tableModelUsuario;
     JButton btnBorrarUsuario;
    private JButton btnCancerlarBorrarUsuario;
    DefaultTableModel tableUsu;
    Modelo modelo;


    /**
     * Constructor de la ventana que nos permite borrar Usuarios.
     * Tiene las configuraciones.
     * @throws SQLException
     */
    public Login14BorrarUsuario() throws SQLException {

        this.setBounds(100,100,500,300);
        this.setResizable(false);
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);

        modelo = new Modelo();
        tableUsu= new DefaultTableModel();
        tableModelUsuario.setModel(tableUsu);
        iniciarTabla();
        cargarFilas(modelo.obtenerDatos());

        btnBorrarUsuario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try{
                    int filaBorrar=tableModelUsuario.getSelectedRow();
                    int idBorrar =0;
                    try {
                        idBorrar = (Integer) tableModelUsuario.getValueAt(filaBorrar, 0);
                    }catch (Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, seleccione un usuario para " +
                                "poder borrarlo, por favor. ");
                    }
                    modelo.eliminarCliente(idBorrar);
                    tableUsu.removeRow(filaBorrar);
                }catch (SQLException e1){
                    e1.printStackTrace();
                }
            }
        });
        btnCancerlarBorrarUsuario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                Login3 login = new Login3();
            }
        });

    }


    //MÉTODOS LOGIN14BORRARUSUARIO

    /**
     * Método que nos pone los nombres de las columnas.
     */
    public void iniciarTabla(){
        String[] headers={"Id","Nombre de usuario","Tipo de usuario"};
        tableUsu.setColumnIdentifiers(headers);
    }

    /**
     * Método que nos carga las filas.
     * @param resultSet Objeto ResultSet
     * @throws SQLException En caso de error.
     */
    private void cargarFilas(ResultSet resultSet) throws SQLException {

        if(resultSet!=null) {
            Object[] fila = new Object[9];
            tableUsu.setRowCount(0);

            while (resultSet.next()) {
                fila[0] = resultSet.getObject(1);
                fila[1] = resultSet.getObject(2);
                fila[2] = resultSet.getObject(4);
                tableUsu.addRow(fila);
            }


        }

    }



}

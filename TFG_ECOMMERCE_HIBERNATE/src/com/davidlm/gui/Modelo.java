/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.gui;

import com.davidlm.base.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.management.relation.Role;
import javax.persistence.Query;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.*;
import java.util.ArrayList;
import java.util.Vector;


/**
 * Clase Modelo del Modelo, Vista, Controlador de usuario tipo Administrador.
 */
public class Modelo {


    SessionFactory sessionFactory;

    /**
     * Método que nos permitirá desconectar de la base de datos.
     */
    public void desconectar() {
        //Cierro la factoria de sessiones
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    /**
     * Método que nos permitirá conectar con la base de datos.
     */
    public void conectar() {
        Configuration configuracion = new Configuration();

        configuracion.configure("hibernate.cfg.xml");

        configuracion.addAnnotatedClass(Almacen.class);
        configuracion.addAnnotatedClass(Cliente.class);
        configuracion.addAnnotatedClass(CaracteristicasPedido.class);
        configuracion.addAnnotatedClass(Pedido.class);
        configuracion.addAnnotatedClass(Producto.class);
        configuracion.addAnnotatedClass(Proveedor.class);

        String user="root";
        configuracion.setProperty("hibernate.connection.username",user);
        configuracion.setProperty("hibernate.connection.password","");
        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();



        sessionFactory = configuracion.buildSessionFactory(ssr);

    }





    /**
     * Método que nos permitirá modificar un proveedor determinado.
     * @param nuevoProveedor Objeto de la clase Proveedor.
     */
    public void altaProveedor(Proveedor nuevoProveedor) {

        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoProveedor);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Método que nos permitirá modificar un proveedor determinado.
     * @param proveedorSeleccion Objeto de la clase Proveedor.
     */
    public void modificarProveedor(Proveedor proveedorSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(proveedorSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos permitirá borrar un proveedor.
     * @param proveedorSeleccion Objeto de la clase Proveedor.
     */
    public void borrarProveedor(Proveedor proveedorSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(proveedorSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos permitirá obtener los Provedores y lo devolverá mediante un ArrayList
     * @return Objeto de tipo ArrayList
     */
    public ArrayList<Proveedor> getProveedores() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Proveedor ");
        ArrayList<Proveedor> listaProveedores = (ArrayList<Proveedor>)query.getResultList();
        sesion.close();
        return listaProveedores;
    }

    /**
     * Método que nos permitirá obtener los Provedores y lo devolverá mediante un ArrayList
     * @return Objeto de tipo ArrayList
     */
    public ArrayList<Proveedor> getProveedoresPorOrdenAlfabetico() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Proveedor ORDER BY nombre ASC ");
        ArrayList<Proveedor> listaProveedores = (ArrayList<Proveedor>)query.getResultList();
        sesion.close();
        return listaProveedores;
    }

    /**
     * Método que
     * @param unProveedor Objeto de la clase Proveedor.
     * @return Objeto de tipo ArrayList
     */
    public ArrayList<Producto> getProductosProveedor(Proveedor unProveedor) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Producto WHERE proveedor.id="+unProveedor.getId());
        ArrayList<Producto> listaProductosProveedor= (ArrayList<Producto>)query.getResultList();
        sesion.close();
        return listaProductosProveedor;
    }





    /**
     * Método que sirve para dar de alta a un Producto
     * @param nuevoProducto Objeto de la clase Producto
     */
    public void altaProducto(Producto nuevoProducto) {

        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoProducto);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Método que sirve para modificar a un Producto
     * @param productoSeleccion Objeto de la clase Producto
     */
    public void modificarProducto(Producto productoSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(productoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que sirve para borrar a un Producto
     * @param productoSeleccion Objeto de la clase Producto
     */
    public void borrarProducto(Producto productoSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(productoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que sirve para obtener los productos de la base de datos. Nos devuelve un ArrayList.
     * @return ArrayList<Producto>
     */
    public ArrayList<Producto> getProductos() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Producto ");
        ArrayList<Producto> listaProductos = (ArrayList<Producto>)query.getResultList();
        sesion.close();
        return listaProductos;
    }

    /**
     * Método para obtener los productos de la base de datos ordenador por nombre alfabéticamente.
     *  Nos devuelve un ArrayList.
     * @return ArrayList<Producto>
     */
    public ArrayList<Producto> getProductosPorOrdenAlfabetico() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Producto ORDER BY nombre ASC ");
        ArrayList<Producto> listaProductos = (ArrayList<Producto>)query.getResultList();
        sesion.close();
        return listaProductos;
    }






    /**
     * Método que nos da de alta un almacén.
     * @param nuevoAlmacen Objeto de la clase Almacén.
     */
    public void altaAlmacen(Almacen nuevoAlmacen) {

        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoAlmacen);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Método que nos modifica un almacén.
     * @param almacenSeleccion Objeto de la clase Almacén.
     */
    public void modificarAlmacen(Almacen almacenSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(almacenSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos borra un almacén.
     * @param almacenSeleccion Objeto de la clase Almacén.
     */
    public void borrarAlmacen(Almacen almacenSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(almacenSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos devuelve un ArrayList<Almacen> con los almacenes de la base de datos.
     * @return Objeto ArrayList<Almacen>
     */
    public ArrayList<Almacen> getAlmacenes() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Almacen ");
        ArrayList<Almacen> listaAlmacenes = (ArrayList<Almacen>)query.getResultList();
        sesion.close();
        return listaAlmacenes;
    }


    /**
     * Método que nos devuelve los almacenes ordenados por nombre alfabéticamente.
     * @return Objeto ArrayList<Almacen>
     */
    public ArrayList<Almacen> getAlmacenesPorOrdenAlfabetico() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Almacen ORDER BY nombre ASC ");
        ArrayList<Almacen> listaAlmacenes = (ArrayList<Almacen>)query.getResultList();
        sesion.close();
        return listaAlmacenes;
    }

    /**
     * Método que nos devuelve los productos que pertenecen a un determinado almacén.
     * Nos devuelve un arraylist.
     *
     * @param unAlmacen Objeto de la clase Almacén
     * @return Objeto ArrayList<Producto>
     */
    public ArrayList<Producto> getProductosAlmacen(Almacen unAlmacen) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Producto WHERE almacen.id="+unAlmacen.getId());
        ArrayList<Producto> listaProductosAlmacen = (ArrayList<Producto>)query.getResultList();
        sesion.close();
        return listaProductosAlmacen;
    }



    /**
     * Método que nos da de alta un CaracteristicasPedido.
     *
     * @param nuevoCaracteristicaPedido Objeto de la clase CaracteristicasPedido
     */
    public void altaCaracteristicasPedido(CaracteristicasPedido nuevoCaracteristicaPedido) {

        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoCaracteristicaPedido);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Método que nos modifica un registro de la clase CaracteristicasPedido
     * @param caracteristicasPedidoSeleccion Objeto de la clase CaracteristicasPedido
     */

    public void modificarCaracteristicasPedido(CaracteristicasPedido caracteristicasPedidoSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(caracteristicasPedidoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos borra un registro de la clase CaracteristicasPedido
     * @param caracteristicasPedidoSeleccion
     */
    public void borrarCaracteristicasPedido(CaracteristicasPedido caracteristicasPedidoSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(caracteristicasPedidoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos devuelve un listado de CaracteristicasPedido de la base de datos.
     * Nos devuevle un ArrayList<CaracteristicasPedido>
     * @return Objeto de ArrayList<CaracteristicasPedido>
     */
    public ArrayList<CaracteristicasPedido> getCaracteristicasPedido() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM CaracteristicasPedido ");
        ArrayList<CaracteristicasPedido> listaCaracteristicaPedido = (ArrayList<CaracteristicasPedido>)query.getResultList();
        sesion.close();
        return listaCaracteristicaPedido;
    }

    /**
     * Método que nos devuelve un listado de los CaracteristicasPedidos ordenador por cantidad de mayor a menor.
     * @return Objeto de ArrayList<CaracteristicasPedido>
     */
    public ArrayList<CaracteristicasPedido> getCaracteristicasPedidoPorCantidad() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM CaracteristicasPedido ORDER BY cantidad ASC ");
        ArrayList<CaracteristicasPedido> listaCaracteristicasPedido = (ArrayList<CaracteristicasPedido>)query.getResultList();
        sesion.close();
        return listaCaracteristicasPedido;
    }

    /**
     * Método que nos devuelve un listado de CaracteristicasPedido de un pedido que recibe.
     * Nos devuelve un ArrayList
     * @param unPedido Objeto de la clase Pedido
     * @return ArrayList<CaracteristicasPedido>
     */
    public ArrayList<CaracteristicasPedido> getCaracteristicasPedidoDeUnPedido(Pedido unPedido) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM CaracteristicasPedido WHERE pedido.id = "+unPedido.getId());
        ArrayList<CaracteristicasPedido> listaCaracteristicaPedido = (ArrayList<CaracteristicasPedido>)query.getResultList();
        sesion.close();
        return listaCaracteristicaPedido;
    }

    /*
    public ArrayList<Pedido> getPedidosCaracteristicasPedido(CaracteristicasPedido unCaracteristicasPedido) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Pedido WHERE ="+unAlmacen.getId());
        ArrayList<Producto> listaProductosAlmacen = (ArrayList<Producto>)query.getResultList();
        sesion.close();
        return listaProductosAlmacen;
    }

    */




    /**
     * Método que nos da de alta un pedido.
     *
     * @param nuevoPedido Objeto de la clase Pedido
     */
    public void altaPedido(Pedido nuevoPedido) {

        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoPedido);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Método que nos modifica un pedido.
     * @param pedidoSeleccion Objeto de la clase Pedido
     */
    public void modificarPedido(Pedido pedidoSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(pedidoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos borra un pedido.
     * @param pedidoSeleccion Objeto de la clase Pedido
     */
    public void borrarPedido(Pedido pedidoSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(pedidoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos devuelve un ArrayList con la lista de Pedidos.
     * @return Objeto ArrayList<Pedido>
     */
    public ArrayList<Pedido> getPedidos() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Pedido ");
        ArrayList<Pedido> listaPedidos = (ArrayList<Pedido>)query.getResultList();
        sesion.close();
        return listaPedidos;
    }

    /**
     * Método que nos devuelve un ArrayList con la lista de Pedidos ordenador por precio.
     * @return Objeto ArrayList<Pedido>
     */
    public ArrayList<Pedido> getPedidosOrdenadosPorPrecio() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Pedido ORDER BY precioTotal ASC ");
        ArrayList<Pedido> listaPedidoOrdenados = (ArrayList<Pedido>)query.getResultList();
        sesion.close();
        return listaPedidoOrdenados;
    }

    /**
     * Método que nos devuelve un ArrayList con la lista de caracteristicaspedido de un determinado Pedido.
     * @return Objeto ArrayList<Pedido>
     */
    public ArrayList<CaracteristicasPedido> getCaracteristicasPedidoPedido(Pedido unPedido) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM CaracteristicasPedido WHERE pedido.id="+unPedido.getId());
        ArrayList<CaracteristicasPedido> listaCaracteristicasPedido= (ArrayList<CaracteristicasPedido>)query.getResultList();
        sesion.close();
        return listaCaracteristicasPedido;
    }
    /*
    public ArrayList<CaracteristicasPedido> getProductoCaracteristicasPedidoPedido(CaracteristicasPedido unCaracteristicasPedido) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Producto WHERE caracteristicaspedido"+unCaracteristicasPedido.getId());
        ArrayList<Producto> listaProductoCaracteristicasPedido= (ArrayList<Producto>)query.getResultList();
        sesion.close();
        return listaProductoCaracteristicasPedido;
    }
    */





    /**
     * Método que nos da de alta un cliente.
     * @param nuevoCliente Objeto tipo Cliente
     */
    public void altaCliente(Cliente nuevoCliente) {

        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoCliente);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     *
     * Método que nos modifica un nuevo cliente.
     * @param clienteSeleccion Objeto tipo Cliente
     */
    public void modificarCliente(Cliente clienteSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(clienteSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }


    /**
     * Método que nos borra un cliente.
     * @param clienteSeleccion Objeto tipo Cliente
     */
    public void borrarCliente(Cliente clienteSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(clienteSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos devuelve un ArrayList de Clientes de la base de datos.
     * @return Objeto tipo ArrayList<Cliente>
     */
    public ArrayList<Cliente> getClientes() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Cliente ");
        ArrayList<Cliente> listaClientes = (ArrayList<Cliente>)query.getResultList();
        sesion.close();
        return listaClientes;
    }

    /**
     * Método que nos devuevle un ArrayList de Clientes ordenador por nombre alfabéticamente.
     * @return  Objeto tipo ArrayList<Cliente>
     */
    public ArrayList<Cliente> getClientesPorOrdenAlfabetico() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Cliente ORDER BY nombre ASC ");
        ArrayList<Cliente> listaClientesOrdenados = (ArrayList<Cliente>)query.getResultList();
        sesion.close();
        return listaClientesOrdenados;
    }

    /**
     * Método que nos devuelve un ArrayList con los pedidos realizados por un cliente.
     * @param unCliente
     * @return  Objeto tipo ArrayList<Pedido>
     */
    public ArrayList<Pedido> getPedidosCliente(Cliente unCliente) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Pedido WHERE cliente.id="+unCliente.getId());
        ArrayList<Pedido> listaPedidosCliente= (ArrayList<Pedido>)query.getResultList();
        sesion.close();
        return listaPedidosCliente;
    }





    /**
     * Método que nos permite obtener los datos de la tabla usuarios, mediante tecnología jdbc.
     * @return Objeto ResultSet
     * @throws SQLException En caso de no conectar con la base de datos. Nos avisa mediante un JOptionPane
     */
    public ResultSet obtenerDatos() throws SQLException{
        Connection conexion= DriverManager.getConnection("jdbc:mysql://localhost:3306/baseecommerce?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
                "root","");
        ResultSet resultado=null;
        try {
            String consulta = "SELECT * FROM usuarios";
            PreparedStatement sentencia = null;
            sentencia = conexion.prepareStatement(consulta);
            resultado = sentencia.executeQuery();
            return resultado;
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Probablemente no existe la tabla, ímportela, por favor",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
        return resultado;
    }


    /**
     * Método que nos elimina un cliente pasándole un determinado valor entero.
     * @param id Objeto tipo int
     * @return Objeto tipon int
     * @throws SQLException
     */
    public int eliminarCliente(int id) throws SQLException {


        Connection conexion= DriverManager.getConnection("jdbc:mysql://localhost:3306/baseecommerce?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
                "root","");
            String consulta="DELETE FROM usuarios WHERE id=?";

            PreparedStatement sentencia=null;

            sentencia=conexion.prepareStatement(consulta);
            sentencia.setInt(1,id);

            int resultado=sentencia.executeUpdate();
            if(sentencia!=null){
                sentencia.close();
            }

            return resultado;

    }
}

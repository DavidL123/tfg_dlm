/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Clase ventana que nos avisa de que el usuario o la contraseña es incorrecta.
 */
public class Login9NombreContrasenaIncorrecta extends JFrame{
    private JButton btnAceptarLogin10;
    private JPanel panel1;

    /**
     * Constructor de la ventana que nos avisa si hemos ingresado la contraseña incorrecta.
     * Cuando cliquemos en el botón Aceptar desaparece la ventana.
     */
    public Login9NombreContrasenaIncorrecta(){
        this.setBounds(100,100,500,300);
        this.setResizable(false);
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);

        btnAceptarLogin10.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
}

/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Clase ventana que nos avisa de que hemos creado el usuario correctamente.
 */
public class Login6UsuarioCreado extends JFrame{
    private JButton btnAceptarLogin6;
    private JPanel panel1;

    /**
     * Constructor de la ventana que nos avisa de que hemos creado el usuario. Tiene su configuración.
     * El botón Aceptar hace desaparecer la ventana.
     */
    public Login6UsuarioCreado(){


            this.setBounds(100,100,500,300);
            this.setResizable(false);
            this.setContentPane(panel1);
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.setLocationRelativeTo(null);
            this.setVisible(true);


            btnAceptarLogin6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    dispose();
                }
            });


    }
}

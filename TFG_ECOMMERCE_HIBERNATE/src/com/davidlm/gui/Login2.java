/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

/**
 * Clase Login de nuestro programa.
 */
public class Login2 extends JFrame{
    private JPanel panel1;
    private JTextField txtUsuario2;
    private JButton btnLogin2;
    private JButton btnSalir2;
    private JButton btnResetLogin2;
    private JPasswordField passField2;


    /**
     * Constructor de la ventana login. Tiene las configuraciones.
     * Tiene 3 botones:
     * -Login: Te conecta con la base de datos y comprueba que tipo de usario es y si es correcto el usuario y contraseña.
     * -Reset: Resetea en todos los campos
     * -Salir: Nos lleva a la ventana de Salir.
     */
    public Login2(){



        //frame.setBounds(100, 100, 450, 300);
        this.add(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 200);
        this.setLocationRelativeTo(null);
        //this.pack();
        //frame.dispose();
        this.setResizable(false);
        this.setVisible(true);


        btnLogin2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String password=passField2.getText();
                String usuario=txtUsuario2.getText();
                System.out.println(txtUsuario2);

                try {
                    Connection con =null;
                    try {
                        con =DriverManager.getConnection("jdbc:mysql://localhost:3306/baseecommerce?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
                                "root", "");
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, fallo de conexión. " +
                                "Asegúrese de tener conectado el programa XAMPP, por favor.");
                    }
                    Statement st =con.createStatement();
                    String sentencia = "SELECT nombre_usuario, contrasena, tipo_usuario FROM usuarios";
                    PreparedStatement PSt = con.prepareStatement(sentencia);
                    boolean existeUserAndPass=false;
                    String tipoUsuario="";
                    ResultSet rs=PSt.executeQuery();
                    while(rs.next()){
                        if(rs.getString(1).equals(usuario)&& rs.getString(2).equals(password)){
                            existeUserAndPass=true;
                            tipoUsuario=rs.getString(3);
                        }
                    }
                    if(existeUserAndPass){
                        txtUsuario2.setText(null);
                        passField2.setText(null);
                        switch (tipoUsuario){
                            case "administrador":
                                Login3 principal = new Login3();
                                dispose();
                                break;
                            case "vendedor":
                                VistaVendedor2 vistaV = new VistaVendedor2();
                                ModeloVendedor modeloV = new ModeloVendedor();
                                ControladorVendedor controlaV = new ControladorVendedor(vistaV,modeloV);
                                dispose();
                                break;
                            case "empaquetador":
                                VistaEmpaquetador vistaE = new VistaEmpaquetador();
                                ModeloEmpaquetador modeloE = new ModeloEmpaquetador();
                                ControladorEmpaquetador controlaE = new ControladorEmpaquetador(vistaE,modeloE);
                                dispose();
                                break;
                        }


                    }else{
                        //System.out.println("Usuario y contraseña incorrecto");
                        //NombreContrasenaIncorrecto nombre= new NombreContrasenaIncorrecto();
                        //nombre.main(null);
                        System.out.println("Incorrecto");
                        Login9NombreContrasenaIncorrecta login = new Login9NombreContrasenaIncorrecta();
                        txtUsuario2.setText(null);
                        passField2.setText(null);
                    }

                } catch (SQLException ex) {
                    // TODO Auto-generated catch block
                    ex.printStackTrace();
                }
            }
        });

        btnResetLogin2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                txtUsuario2.setText(null);
                passField2.setText(null);
            }
        });


        btnSalir2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Login12EstasSeguroSalir login = new Login12EstasSeguroSalir();
            }
        });
    }
}


/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Clase ventana que nos avisa de que las contraseñas no coinciden al crear un usuario.
 */
public class Login11ContrasenasNoCoinciden extends JFrame{
    private JButton btnAceptarLogin11;
    private JPanel panel1;

    /**
     * Constructor de la ventana que nos avisa cuando las contraseñas no coinciden al crear un usuario.
     * Tienen las configuraciones.
     * Si clicamos en el botón Aceptar desaparece.
     */
    public Login11ContrasenasNoCoinciden(){
        this.setBounds(100,100,500,300);
        this.setResizable(false);
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);

        btnAceptarLogin11.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
}

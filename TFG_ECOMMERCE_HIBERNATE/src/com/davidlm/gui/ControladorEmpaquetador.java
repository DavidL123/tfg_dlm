/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.gui;

import com.davidlm.base.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Date;
import java.util.ArrayList;

/**
 * Clase Controlador del Modelo, Vista, Controlador de usuario tipo Empaquetador.
 */
public class ControladorEmpaquetador extends Component implements ActionListener, ListSelectionListener, ChangeListener, FocusListener, MouseListener, ItemListener {


    private VistaEmpaquetador vista;
    private ModeloEmpaquetador modelo;


    private String rutaFoto;

    public ControladorEmpaquetador(VistaEmpaquetador vista, ModeloEmpaquetador modelo) {
        this.vista = vista;
        this.modelo = modelo;
// <property name="connection.driver_class">com.mysql.cj.jdbc.Driver</property>
        modelo.conectar();
        addActionListeners(this);
        addListSelectionListener(this);
        rutaFoto=null;



        listarPedidos(modelo.getPedidos());

        calcularPrecioTotalesDeTodos();
        listarPedidos(modelo.getPedidos());
    }

    /**
     * Método que contiene los listener de los JList
     * @param listener Objeto tipo ListSelectionListener
     */
    private void addListSelectionListener(ListSelectionListener listener){
        vista.listCaracteristicasPedido.addListSelectionListener(listener);
        vista.listProductosCaracteristicasPedido.addListSelectionListener(listener);
        vista.listPedido.addListSelectionListener(listener);

    }




    /**
     * Método que contiene los listener de los botones
     * @param listener Objeto tipo ActionListener
     */
    private void addActionListeners(ActionListener listener) {
        vista.btnCompletadoVistaEmpaq.addActionListener(listener);
        vista.cerrarSesion.addActionListener(listener);
    }

    /**
     * Método que nos lista en el JList de pedido de la ficha Pedidos
     * @param lista Objeto tipo ArrayList<Pedido>
     */
    public void listarPedidos (ArrayList<Pedido> lista){
        vista.dlmPedido.clear();
        for (Pedido unPedido : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmPedido.addElement(unPedido);
        }
    }


    /**
     * Método que nos lista en el JList de caracteristicaspedido de la ficha Pedidos
     * @param lista Objeto tipo ArrayList<CaracteristicasPedido>
     */
    public void listarCaracteristicasPedidoPedido(ArrayList<CaracteristicasPedido> lista){
        vista.dlmCaracteristicasPedidos.clear();
        for(CaracteristicasPedido unCaracteristicasPedido: lista){
            vista.dlmCaracteristicasPedidos.addElement(unCaracteristicasPedido);
        }
    }


    /**
     *Método que nos calcula los precios totales de Pedidos
     */
    public void calcularPrecioTotalesDeTodos(){
        ArrayList<Pedido> lista= modelo.getPedidos();
        for(Pedido pedido: lista){
            Float precioTotal=(float)calcularPrecioTotalPedido(modelo.getCaracteristicasPedidoDeUnPedido(pedido));
            //System.out.println(precioTotal+"--------------------->PRECIO TOTAL");
            pedido.setPrecioTotal(precioTotal);
            modelo.modificarPedido(pedido);
        }
    }

    /**
     * Método que nos calcula el PrecioTotal de un pedido. Recibe un arraylist de CaracteristicasPedido.
     * @param list Objeto de arraylist.
     * @return Objeto tipo double
     */
    private double calcularPrecioTotalPedido(ArrayList<CaracteristicasPedido> list){
        double precioTotal=0;
        double precioSubTotal=0;
        for (int i=0;i<list.size();i++){
            precioSubTotal=0;
            precioSubTotal=list.get(i).getCantidad()*list.get(i).getProducto().getPrecio();
            precioTotal+=precioSubTotal;
        }
        return precioTotal;
    }
    /**
     * Método que recibirá la señal de cuando usemos un botón. Según el action command que tenga, a través del switch, ejecutará
     * una serie de órdenes. También después de realizar las accciones configuradas, ejecutará los métodos que nos visualizará
     * los datos que tengamos en la base de datos en cada JList correspondiente.
     * @param e ActionEvent Recibe el dato emitido, normalmente al pulsar un botón.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch (comando) {

            /**
             * Orden que modifica un el campo Realizado de pedido para indicar que ya está realizado.
             */
            case "CompletarPedido":
                Pedido pedidoCompletado = (Pedido) vista.listPedido.getSelectedValue();
                if(pedidoCompletado!=null){
                    if(pedidoCompletado.getRealizado().equalsIgnoreCase("No")){
                        pedidoCompletado.setRealizado("Sí");
                        modelo.modificarPedido(pedidoCompletado);
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Error, seleccione un pedido, por favor. ");
                    break;
                }
                listarPedidos(modelo.getPedidos());
                break;
            /**
             * Orden que cierra sesión y vuelve al original.
             */
            case "CerrarSesion":
                vista.dispose();
                Login2 login = new Login2();
                break;

        }
    }

    @Override
    public void focusGained(FocusEvent e) {


    }

    @Override
    public void focusLost(FocusEvent e) {

    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void stateChanged(ChangeEvent e) {

    }

    /**
     * Método que nos detectará cuando cliquemos sobre un registro de cada JList del programa.
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {

        if(e.getValueIsAdjusting()){

            /**
             * Nos rellena el JList Producto al seleccionar un CaracteristicasPedido.
             *
             */
            if(e.getSource() == vista.listCaracteristicasPedido) {
                CaracteristicasPedido caracteristicasPedidoSeleccion = (CaracteristicasPedido) vista.listCaracteristicasPedido.getSelectedValue();
                vista.dlmProductoCaracteristicasPedido.clear();
                vista.dlmProductoCaracteristicasPedido.addElement(caracteristicasPedidoSeleccion.getProducto().toString());
            }

            /**
             * Nos rellena el JList CaracteristicasPedido al seleccionar un Pedido.
             */
            if(e.getSource() == vista.listPedido) {
                Pedido pedidoSeleccion = (Pedido) vista.listPedido.getSelectedValue();
                listarCaracteristicasPedidoPedido(modelo.getCaracteristicasPedidoPedido(pedidoSeleccion));
            }



        }
    }
}

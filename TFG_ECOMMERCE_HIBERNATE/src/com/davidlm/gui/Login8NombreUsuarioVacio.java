/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Clase ventana que nos avisa de que el usuario está vacio al clicar sobre crear usuario.
 */
public class Login8NombreUsuarioVacio extends JFrame{
    private JButton btnAceptarLogin8;
    private JPanel panel1;

    /**
     * Constructor de una ventana que nos avisa si tenemos el campo vacío cuando nos logueamos.
     * Tiene sus configuraciones.
     * Al clicar sobre el aceptar desaparece la ventana.
     */
    public Login8NombreUsuarioVacio(){
        this.setBounds(100,100,500,300);
        this.setResizable(false);
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);

        btnAceptarLogin8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

    }
}

/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.gui;

import com.davidlm.base.*;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;


/**
 * Clase vista del programa del usuario Vendedor
 */
public class VistaVendedor extends JFrame{
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
     JTextField txtTelefonoCliente;
    JButton btnAnadirCliente;
     JButton btnModificarCliente;
     JButton btnEliminarCliente;
     JList listCliente;
     JButton btnListarCliente;
     JComboBox comboClientePedido;
     JList listPedidosCliente;
     JTextField txtCantidadProductoPedido;
     JButton btnAnadirProductoPedido;
     JButton btnModificarProductoPedido;
     JButton btnEliminarProductoPedido;
     JButton btnListarProductoPedido;
     JComboBox comboProductoCaractPedido;
     JComboBox comboPedidoCaractPedido;
     JList listCaracteristicasPedido;
    DatePicker dateFechaCompraPedido;
     JTextField txtPrecioTotalPedido;
     JList listPedido;
     JList listCaracteristicasPedidoPedidoVendedor;
     JList listProductosCaracteristicasPedidoPedido;
     JButton btnAnadirPedidoVendedor;
     JButton btnModificarPedidoVendedor;
     JButton btnListarPedidoVendedor;
     JButton btnEliminarPedidoVendedor;
     JTextField txtCorreoElectronicoCliente;
     JTextField txtNumeroTarjetaCliente;
     JTextField txtPaisCliente;
     JTextField txtCiudadCliente;
     JTextField txtCalleCliente;
     JTextField txtCodigoPostalCliente;
     JTextField txtDNICliente;
     JTextField txtApellidosCliente;
     JTextField txtNombreCliente;




    DefaultListModel dlmCaracteristicasPedidos;
    DefaultListModel dlmProductoCaracteristicasPedido;
    DefaultListModel dlmPedidoCaracteristicasPedido;

    DefaultListModel dlmPedido;
    DefaultListModel dlmCaracteristicasPedidoPedido;
    DefaultListModel dlmProductoCaracteristicasPedidoPedido;



    DefaultListModel dlmCliente;
    DefaultListModel dlmPedidosCliente;

    /*
    DefaultTableModel dtmProveedorProducto;
    DefaultTableModel dtmCaracteristicasPedido;
    */


    DefaultComboBoxModel<Producto> dcbmProductoCaracteristicasPedido;
    DefaultComboBoxModel<Pedido> dcbmPedidoCaracteristicasPedido;
    DefaultComboBoxModel<Cliente> dcbmClientePedido;


    /**
     * Constructuro de la vista de vendedor.
     *  - Tiene las configuraciones de la Ventana
     *  - Configura los DefaultListModel con los JList
     *  - Configura los DefaultComboBoxModel con los JComboBox
     */
    public VistaVendedor(){
        this.setContentPane(panel1);


        this.setExtendedState(this.MAXIMIZED_BOTH);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //this.setSize(new Dimension(this.getWidth()+50, this.getHeight()+50));
        //this.setLocationRelativeTo(null);
        this.pack();
        this.setVisible(true);

        crearModelos();
        inicializarCombos();
    }

    /**
     * Método para inicializar los DefaultListModel y configurarlos.
     */
    private void crearModelos() {


        /*
        dlmProveedorProducto = new DefaultListModel();
        listProveedorProducto.setModel(dlmProveedorProducto);

         */

        dlmCaracteristicasPedidos=new DefaultListModel();
        listCaracteristicasPedido.setModel(dlmCaracteristicasPedidos);

        dlmPedido=new DefaultListModel();
        listPedido.setModel(dlmPedido);

        dlmCaracteristicasPedidoPedido=new DefaultListModel();
        listPedido.setModel(dlmCaracteristicasPedidoPedido);

        dlmProductoCaracteristicasPedidoPedido=new DefaultListModel();
        listProductosCaracteristicasPedidoPedido.setModel(dlmProductoCaracteristicasPedidoPedido);

        dlmCliente= new DefaultListModel();
        listCliente.setModel(dlmCliente);

        dlmPedidosCliente= new DefaultListModel();
        listPedidosCliente.setModel(dlmPedidosCliente);

/*
        dtmCaracteristicasPedido=new DefaultTableModel();
        tableCaracteristicasPedido.setModel(dtmCaracteristicasPedido);

        String headers[] = { "Id","Cantidad", "Id_producto","Id_pedido" };

        dtmCaracteristicasPedido.setColumnIdentifiers(headers);
*/
        /*
        dtmProveedorProducto= new DefaultTableModel();
        listProveedorProducto.setModel(dtmProveedorProducto);
        */
    }


    /**
     * Método para inicializar los DefaultComboBoxModel y configurarlos.
     */
    private void inicializarCombos(){



        //Caracteristicas pedido
        dcbmProductoCaracteristicasPedido =  new DefaultComboBoxModel<>();
        comboProductoCaractPedido.setModel(dcbmProductoCaracteristicasPedido);

        dcbmPedidoCaracteristicasPedido =  new DefaultComboBoxModel<>();
        comboPedidoCaractPedido.setModel(dcbmPedidoCaracteristicasPedido);



        //Pedido
        dcbmClientePedido=new DefaultComboBoxModel<>();
        comboClientePedido.setModel(dcbmClientePedido);
    }
}

/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.gui;

import com.davidlm.base.*;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

/**
 * Clase vista del programa del usuario Administrador.
 */
public class Vista extends JFrame {
    private JScrollPane ScrollPane;
    private JTabbedPane tabbedPane1;
     JButton btnAnadirProveedor;
     JButton btnModificarProveedor;
     JButton btnEliminarProveedor;

     DatePicker dateFechaCompraPedido;
     JTextField txtNombreProveedor;
     JTextField txtTelefonoProveedor;
     JTextField txtCorreoElectronicoProveedor;
     JTextField txtPaisProveedor;
     JTextField txtCiudadProveedor;
     JTextField txtCalleProveedor;
     JTextField txtNumeroProveedor;
     JTextField txtCodigoPostalProveedor;
     JTextField txtNIFProveedor;
     JTextField txtPersonaContactoProveedor;
     JButton btnListarProveedor;
     JTextField txtNombreProducto;
     JTextField txtMarcaProducto;
     JTextField txtPesoProducto;
     JTextField txtDimensionesProducto;
     JTextField txtPrecioProducto;
     JTextField txtRutaFotoProducto;
     JButton btnAnadirProducto;
     JButton btnModificarProducto;
     JButton btnEliminarProducto;
    JList listProveedor;
 JRadioButton rbEnvioTrueProducto;
     JRadioButton rbEnvioFalseProducto;
     JButton btnImagenProducto;
     JButton btnListarProducto;
     JTextField txtNombreAlmacen;
     JTextField txtTelefonoAlmacen;
     JTextField txtCorreoElectronicoAlmacen;
     JTextField txtMetrosCuadradosAlmacen;
     JTextField txtCantidadTrabajadoresAlmacen;
     JTextField txtCiudadAlmacen;
     JTextField txtCalleAlmacen;
     JTextField txtCodigoPostalAlmacen;
     JButton btnAnadirAlmacen;
     JButton btnModificarAlmacen;
     JButton btnEliminarAlmacen;
    JButton btnListarAlmacen;
    JTextField txtCantidadProductoPedido;
     JButton btnAnadirProductoPedido;
     JButton btnModificarProductoPedido;
     JButton btnEliminarProductoPedido;
    JButton btnListarProductoPedido;
     JTextField txtPrecioTotalPedido;
     JButton btnAnadirPedido;
     JButton btnModificarPedido;
     JButton btnEliminarPedido;
    JButton btnListarPedido;
     JTextField txtNombreCliente;
     JTextField txtApellidosCliente;
     JTextField txtTelefonoCliente;
     JTextField txtCorreoElectronicoCliente;
     JTextField txtNumeroTarjetaCliente;
     JTextField txtPaisCliente;
     JTextField txtCiudadCliente;
     JTextField txtCalleCliente;
     JTextField txtCodigoPostalCliente;
     JButton btnAnadirCliente;
     JButton btnModificarCliente;
     JButton btnEliminarCliente;
    JButton btnListarCliente;
     JPanel panel1;
     DatePicker dateFechaProducto;
     JTextField txtCategoriaProducto;


    JList listProveedorProducto;
     JComboBox comboProveedorProducto;
     JComboBox comboAlmacenProducto;
     JList listProducto;
     JLabel labelFoto;
    JList listProductoAlmacen;
     JList listAlmacen;
    JList listPedido;
    JList listCliente;
    JList listPedidosCliente;
     JComboBox comboProductoCaractPedido;
     JComboBox comboPedidoCaractPedido;
     JList listCaracteristicasPedido;
    JList listClientePedido;
    JComboBox comboClientePedido;
     JList listCaracteristicasPedidoPedido;


    JList listProductosProveedor;
     JTextField txtDNICliente;
     JList listProductoCaracteristicasPedidoPedido;
     JButton btnDineroMes;
     JButton btnCantProductosCategoria;
     JButton btnCantPedidCliente;
     JButton btnClientGastaronMas500Mes;
    JButton btnGenerarFactura;
    JList listPrecioTotal;
    JTable tableCaracteristicasPedido;


    DefaultListModel dlmProveedor;
    DefaultListModel dlmProductosProveedor;

    DefaultListModel dlmProducto;
    DefaultListModel dlmAlmacen;
    DefaultListModel dlmProductoAlmacen;
    DefaultListModel dlmProveedorProducto;
    DefaultListModel dlmCaracteristicasPedidos;
    DefaultListModel dlmProductoCaracteristicasPedido;
    DefaultListModel dlmPedidoCaracteristicasPedido;

    DefaultListModel dlmPedido;
    DefaultListModel dlmCaracteristicasPedidoPedido;
    DefaultListModel dlmProductoCaracteristicasPedidoPedido;




    DefaultListModel dlmCliente;
    DefaultListModel dlmPedidosCliente;

    /*
    DefaultTableModel dtmProveedorProducto;
    DefaultTableModel dtmCaracteristicasPedido;
    */

    DefaultComboBoxModel<Almacen> dcbmAlmacenProducto;
    DefaultComboBoxModel<Proveedor> dcbmProveedorProducto;
    DefaultComboBoxModel<Producto> dcbmProductoCaracteristicasPedido;
    DefaultComboBoxModel<Pedido> dcbmPedidoCaracteristicasPedido;
    DefaultComboBoxModel<Cliente> dcbmClientePedido;


    /*JMenuItem itemConectar;*/
    JMenuItem cerrarSesion;

    /**
     * Constructor de la clase Vista
     *
     *
     * - Tiene las configuraciones de la Ventana
     * - Configura los DefaultListModel con los JList
     * - Configura los DefaultComboBoxModel con los JComboBox
     */
    public Vista(){


        this.setContentPane(ScrollPane);


        this.setExtendedState(this.MAXIMIZED_BOTH);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //this.setSize(new Dimension(this.getWidth()+50, this.getHeight()+50));
        //this.setLocationRelativeTo(null);
        this.pack();
        this.setVisible(true);


        crearModelos();
        inicializarCombos();
        txtPrecioTotalPedido.setEditable(false);
        crearMenus();
    }

    /**
     * Método que nos crea los DefaultListModel y los agrega a los JList correspondientes.
     */
    private void crearModelos() {

        dlmProveedor = new DefaultListModel();
        listProveedor.setModel(dlmProveedor);

        dlmProductosProveedor= new DefaultListModel();
        listProductosProveedor.setModel(dlmProductosProveedor);

        dlmProducto = new DefaultListModel();
        listProducto.setModel(dlmProducto);

        dlmAlmacen = new DefaultListModel();
        listAlmacen.setModel(dlmAlmacen);

        dlmProductoAlmacen = new DefaultListModel();
        listProductoAlmacen.setModel(dlmProductoAlmacen);


        dlmCaracteristicasPedidos=new DefaultListModel();
        listCaracteristicasPedido.setModel(dlmCaracteristicasPedidos);

        dlmPedido=new DefaultListModel();
        listPedido.setModel(dlmPedido);

        dlmCaracteristicasPedidoPedido=new DefaultListModel();
        listCaracteristicasPedidoPedido.setModel(dlmCaracteristicasPedidoPedido);


        dlmProductoCaracteristicasPedidoPedido =new DefaultListModel();
        listProductoCaracteristicasPedidoPedido.setModel(dlmProductoCaracteristicasPedidoPedido);

        dlmCliente= new DefaultListModel();
        listCliente.setModel(dlmCliente);

        dlmPedidosCliente= new DefaultListModel();
        listPedidosCliente.setModel(dlmPedidosCliente);
           /*
        dlmProveedorProducto = new DefaultListModel();
        listProveedorProducto.setModel(dlmProveedorProducto);

         */

/*
        dtmCaracteristicasPedido=new DefaultTableModel();
        tableCaracteristicasPedido.setModel(dtmCaracteristicasPedido);

        String headers[] = { "Id","Cantidad", "Id_producto","Id_pedido" };

        dtmCaracteristicasPedido.setColumnIdentifiers(headers);
*/
        /*
        dtmProveedorProducto= new DefaultTableModel();
        listProveedorProducto.setModel(dtmProveedorProducto);
        */
    }


    /**
     * Método que nos inicializa los DefaultComboBoxes y modifica los ComboBoxes
     */
    private void inicializarCombos(){

       //Producto
       dcbmAlmacenProducto = new DefaultComboBoxModel<>();
       comboAlmacenProducto.setModel(dcbmAlmacenProducto);

       dcbmProveedorProducto =  new DefaultComboBoxModel<>();
       comboProveedorProducto.setModel(dcbmProveedorProducto);


       //Caracteristicas pedido
          dcbmProductoCaracteristicasPedido =  new DefaultComboBoxModel<>();
          comboProductoCaractPedido.setModel(dcbmProductoCaracteristicasPedido);

          dcbmPedidoCaracteristicasPedido =  new DefaultComboBoxModel<>();
          comboPedidoCaractPedido.setModel(dcbmPedidoCaracteristicasPedido);



          //Pedido
          dcbmClientePedido=new DefaultComboBoxModel<>();
          comboClientePedido.setModel(dcbmClientePedido);
      }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }


    /**
     * Método que nos crea un JMenuItem y JMenu. Agrega el JMenuItem al JMenu y nos agrega el JMenu al JMenuBar.
     */
    private void crearMenus(){
        cerrarSesion= new JMenuItem("Cerrar Sesión");
        cerrarSesion.setActionCommand("CerrarSesion");
        JMenu menuArchivo = new JMenu ("Archivo");


        menuArchivo.add(cerrarSesion);
        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);
        setJMenuBar(barraMenu);
    }


}

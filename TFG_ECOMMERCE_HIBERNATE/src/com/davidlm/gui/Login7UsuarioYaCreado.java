/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Clase ventana que nos avisa de que el usuario ya está creado.
 */
public class Login7UsuarioYaCreado extends JFrame{
    private JButton btnAceptarLogin7;
    private JPanel panel1;

    /**
     * Constructor de la ventana que nos avisa de ya está creado el usuario. Tiene su configuración.
     * Si clicamos en el botón Aceptar descaparece la ventana.
     */
    public Login7UsuarioYaCreado(){


        this.setBounds(100,100,500,300);
        this.setResizable(false);
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);

        btnAceptarLogin7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
}

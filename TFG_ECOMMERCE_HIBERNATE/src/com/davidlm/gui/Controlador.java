/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.gui;

import com.davidlm.base.*;
import com.davidlm.informes.ReportGenerator;
import jdk.nashorn.internal.scripts.JO;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;
import org.hibernate.exception.ConstraintViolationException;

import javax.imageio.ImageIO;
import javax.persistence.PersistenceException;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase Controlador del Modelo, Vista, Controlador de usuario tipo Administrador.
 */
public class Controlador extends Component implements ActionListener, ListSelectionListener, ChangeListener, FocusListener, MouseListener, ItemListener {


    private Vista vista;
    private Modelo modelo;


    private String rutaFoto;


    /**
     * Constructor de clase Controlador, recibe los objetos Vista y Modelo. También añade y configura
     * los métodos ActionListener, ListSelectionListener, ChangeListener, FocusListener, MouseListener, ItemListener;
     * @param vista Objeto de la clase Vista
     * @param modelo Objeto de la clase Modelo
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;


        addActionListeners(this);
        addListSelectionListener(this);
        addFocusListeners(this);
        addItemListeners(this);

        rutaFoto=null;


        modelo.conectar();
        listarProveedores(modelo.getProveedores());
        listarProductos(modelo.getProductos());
        listarAlmacenes(modelo.getAlmacenes());
        listarCaracteristicasPedido(modelo.getCaracteristicasPedido());
        listarPedidos(modelo.getPedidos());
        listarClientes(modelo.getClientes());

        calcularPrecioTotalesDeTodos();
        listarPedidos(modelo.getPedidos());

    }

    /**
     * Método con los listeners de los ComboBox
     * @param controlador
     */
    private void addItemListeners(Controlador controlador) {
        vista.comboAlmacenProducto.addItemListener(controlador);
        vista.comboProveedorProducto.addItemListener(controlador);
        vista.comboProductoCaractPedido.addItemListener(controlador);
        vista.comboPedidoCaractPedido.addItemListener(controlador);
        vista.comboClientePedido.addItemListener(controlador);

    }

    /**
     * Método con los listeners de los DefaultListModel
     * @param listener
     */
    private void addListSelectionListener(ListSelectionListener listener){
        vista.listProveedor.addListSelectionListener(listener);
        vista.listProducto.addListSelectionListener(listener);
        vista.listAlmacen.addListSelectionListener(listener);
        vista.listCaracteristicasPedido.addListSelectionListener(listener);
        vista.listPedido.addListSelectionListener(listener);
        vista.listCaracteristicasPedidoPedido.addListSelectionListener(listener);
        vista.listCliente.addListSelectionListener(listener);
    }


    /**
     * Método con los listeners de los JComboBox
     * @param controlador
     */
    private void addFocusListeners(Controlador controlador) {
        vista.comboAlmacenProducto.addFocusListener(controlador);
        vista.comboProveedorProducto.addFocusListener(controlador);
        vista.comboPedidoCaractPedido.addFocusListener(controlador);
        vista.comboProductoCaractPedido.addFocusListener(controlador);
        vista.comboClientePedido.addFocusListener(controlador);

    }


    /**
     * Método de los listeners de los botones.
     * @param listener
     */
    private void addActionListeners(ActionListener listener) {

        vista.btnAnadirProveedor.addActionListener(listener);
        vista.btnModificarProveedor.addActionListener(listener);
        vista.btnListarProveedor.addActionListener(listener);
        vista.btnEliminarProveedor.addActionListener(listener);



        vista.btnAnadirProducto.addActionListener(listener);
        vista.btnModificarProducto.addActionListener(listener);
        vista.btnListarProducto.addActionListener(listener);
        vista.btnEliminarProducto.addActionListener(listener);
        vista.rbEnvioTrueProducto.addActionListener(listener);
        vista.rbEnvioFalseProducto.addActionListener(listener);
        vista.btnImagenProducto.addActionListener(listener);


        vista.btnAnadirAlmacen.addActionListener(listener);
        vista.btnModificarAlmacen.addActionListener(listener);
        vista.btnEliminarAlmacen.addActionListener(listener);
        vista.btnListarAlmacen.addActionListener(listener);



        vista.btnAnadirProductoPedido.addActionListener(listener);
        vista.btnModificarProductoPedido.addActionListener(listener);
        vista.btnEliminarProductoPedido.addActionListener(listener);
        vista.btnListarProductoPedido.addActionListener(listener);


        vista.btnAnadirPedido.addActionListener(listener);
        vista.btnModificarPedido.addActionListener(listener);
        vista.btnEliminarPedido.addActionListener(listener);
        vista.btnListarPedido.addActionListener(listener);


        vista.btnAnadirCliente.addActionListener(listener);
        vista.btnModificarCliente.addActionListener(listener);
        vista.btnListarCliente.addActionListener(listener);
        vista.btnEliminarCliente.addActionListener(listener);


        vista.btnGenerarFactura.addActionListener(listener);
        vista.btnCantProductosCategoria.addActionListener(listener);
        vista.btnCantPedidCliente.addActionListener(listener);
        vista.btnClientGastaronMas500Mes.addActionListener(listener);

        /*vista.itemConectar.addActionListener(listener);*/
        vista.cerrarSesion.addActionListener(listener);

    }

    /**
     * Método que recibirá la señal de cuando usemos un botón. Según el action command que tenga, a través del switch, ejecutará
     * una serie de órdenes. También después de realizar las accciones configuradas, ejecutará los métodos que nos visualizará
     * los datos que tengamos en la base de datos en cada JList correspondiente.
     * @param e ActionEvent Recibe el dato emitido, normalmente al pulsar un botón.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch (comando) {
            /**
             *Cuando se active esta orden se comprobará si hay algún campo vacío. En caso contrario, nos emite una
             * ventana.
             * Se crea un proveedor. Se recojen los datos de cada caja de texto y se modifica cada atributo.
             * Se da de alta.
             * Y se vuelve a utilizar el JList.
             *
             */
            case "AnadirProveedor":


                if (vista.txtNombreProveedor.getText().equalsIgnoreCase("") ||
                        vista.txtTelefonoProveedor.getText().equalsIgnoreCase("") ||
                        vista.txtCorreoElectronicoProveedor.getText().equalsIgnoreCase("") ||
                        vista.txtPaisProveedor.getText().equalsIgnoreCase("") ||
                        vista.txtCiudadProveedor.getText().equalsIgnoreCase("") ||
                        vista.txtCalleProveedor.getText().equalsIgnoreCase("") ||
                        vista.txtCodigoPostalProveedor.getText().equalsIgnoreCase("") ||
                        vista.txtNIFProveedor.getText().equalsIgnoreCase("") ||
                        vista.txtPersonaContactoProveedor.getText().equalsIgnoreCase("")) {
                    JOptionPane.showMessageDialog(null, "Rellene todos los campos, por favor");
                } else {

                        Proveedor nuevoProveedor = new Proveedor();
                        nuevoProveedor.setNombre(vista.txtNombreProveedor.getText());
                        try{
                            nuevoProveedor.setTelefono(Integer.parseInt(vista.txtTelefonoProveedor.getText()));
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(null, "Introduzca un valor númerico entero en " +
                                    "el campo \"Teléfono\", por favor.");
                            break;
                        }
                        nuevoProveedor.setCorreoElectronico(vista.txtCorreoElectronicoProveedor.getText());
                        nuevoProveedor.setPais(vista.txtPaisProveedor.getText());
                        nuevoProveedor.setCiudad(vista.txtCiudadProveedor.getText());
                        nuevoProveedor.setCalle(vista.txtCalleProveedor.getText());
                        try{
                            nuevoProveedor.setNumero(Integer.parseInt(vista.txtNumeroProveedor.getText()));
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(null, "Introduzca un valor númerico entero en " +
                                    "el campo \"Número\", por favor.");
                            break;
                        }
                        try{
                        nuevoProveedor.setCodigoPostal(Integer.parseInt(vista.txtCodigoPostalProveedor.getText()));
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(null, "Introduzca un valor númerico entero en " +
                                    "el campo \"Código postal\", por favor.");
                            break;
                        }
                        nuevoProveedor.setNif(vista.txtNIFProveedor.getText());
                        nuevoProveedor.setPersonaContacto(vista.txtPersonaContactoProveedor.getText());
                        try {
                            modelo.altaProveedor(nuevoProveedor);
                        }catch (PersistenceException ex){
                            JOptionPane.showMessageDialog(null, "Error, NIF repetido. Sólo puede haber un único NIF, gracias");
                        }
                        listarProveedores(modelo.getProveedores());

                }

                break;
            /**
             * Cuando se ejecuta esta orden se modifica un proveedor seleccionado en el JList de proveedores.
             * Primero nos avisará de si no hemos seleccionado un proveedor que lo hagamos.
             * Después, comprueba si hemos rellenado todos los campos, que no hay ninguno vacío.
             * Cuando lo ha hecho, modifica los atributos que hemos puesto en la ficha del proveedor seleccionado.
             * Cuando termina, vuelve a listar el JList de proveedores.
             */
            case "ModificarProveedor":


                Proveedor proveedorSeleccion =null;
                try{
                    proveedorSeleccion = (Proveedor) vista.listProveedor.getSelectedValue();
                    proveedorSeleccion.setNombre(vista.txtNombreProveedor.getText());
                }catch (Exception ex){
                    JOptionPane.showMessageDialog(null, "Error, seleccione un proveedor" +
                            " para poder modificarlo, por favor. ");
                    break;
                }

                if (vista.txtNombreProveedor.getText().equalsIgnoreCase("") ||
                        vista.txtTelefonoProveedor.getText().equalsIgnoreCase("") ||
                        vista.txtCorreoElectronicoProveedor.getText().equalsIgnoreCase("") ||
                        vista.txtPaisProveedor.getText().equalsIgnoreCase("") ||
                        vista.txtCiudadProveedor.getText().equalsIgnoreCase("") ||
                        vista.txtCalleProveedor.getText().equalsIgnoreCase("") ||
                        vista.txtNumeroProveedor.getText().equalsIgnoreCase("") ||
                        vista.txtCodigoPostalProveedor.getText().equalsIgnoreCase("") ||
                        vista.txtNIFProveedor.getText().equalsIgnoreCase("") ||
                        vista.txtPersonaContactoProveedor.getText().equalsIgnoreCase("")) {
                    JOptionPane.showMessageDialog(null, "Rellene todos los campos, por favor.");
                } else {
                        proveedorSeleccion =null;
                        try{
                            proveedorSeleccion = (Proveedor) vista.listProveedor.getSelectedValue();
                            proveedorSeleccion.setNombre(vista.txtNombreProveedor.getText());
                        }catch (Exception ex){
                            JOptionPane.showMessageDialog(null, "Error, seleccione un proveedor" +
                                    " para poder modificarlo, por favor. ");
                        }
                        proveedorSeleccion.setNombre(vista.txtNombreProveedor.getText());
                        try{
                            proveedorSeleccion.setTelefono(Integer.parseInt(vista.txtTelefonoProveedor.getText()));
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(null, "Introduzca un valor númerico entero en " +
                                    "el campo \"Teléfono\", por favor.");
                        }
                        proveedorSeleccion.setCorreoElectronico(vista.txtCorreoElectronicoProveedor.getText());
                        proveedorSeleccion.setPais(vista.txtPaisProveedor.getText());
                        proveedorSeleccion.setCiudad(vista.txtCiudadProveedor.getText());
                        proveedorSeleccion.setCalle(vista.txtCalleProveedor.getText());
                        try{
                            proveedorSeleccion.setNumero(Integer.parseInt(vista.txtNumeroProveedor.getText()));
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(null, "Introduzca un valor númerico entero en " +
                                    "el campo \"Número\", por favor.");
                            break;
                        }
                        try{
                            proveedorSeleccion.setCodigoPostal(Integer.parseInt(vista.txtCodigoPostalProveedor.getText()));
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(null, "Introduzca un valor númerico entero en " +
                                    "el campo \"Código postal\", por favor.");
                            break;
                        }
                        proveedorSeleccion.setNif(vista.txtNIFProveedor.getText());
                        proveedorSeleccion.setPersonaContacto(vista.txtPersonaContactoProveedor.getText());
                        try{
                            modelo.modificarProveedor(proveedorSeleccion);
                        }catch (PersistenceException ex){
                            JOptionPane.showMessageDialog(null, "Error, NIF repetido. Sólo puede haber un único NIF, gracias");
                        }
                        listarProveedores(modelo.getProveedores());

                }
                break;
            /**
             * Cuando se ejecuta esta orden, se nos ordena el JList de proveedores por nombre en orden alfabético.
             *
             */
            case "ListarProveedor":
                listarProveedoresPorNombreAlfabetico(modelo.getProveedoresPorOrdenAlfabetico());


                break;
            /**
             * Cuando se ejecuta este orden, se nos borrará un proveedor que hayamos seleccionado.
             * También nos avisará con try-catch, si hay errores de persistencia o no hemos seleccionado
             * un proveedor.
             * Se nos borrarán los campos de la ficha.
             */
            case "EliminarProveedor":
                try {
                    Proveedor proveedorBorrado = (Proveedor) vista.listProveedor.getSelectedValue();
                    modelo.borrarProveedor(proveedorBorrado);
                    listarProveedores(modelo.getProveedores());

                }catch (PersistenceException ex){
                    JOptionPane.showMessageDialog(null,"Error de persistencia, tiene que borrar antes " +
                            "los productos asociados a este proveedor, por favor.");
                 }catch(Exception ex){
                    JOptionPane.showMessageDialog(null,"Error, seleccione un proveedor para " +
                            "poder borrar, por favor.");
                }
                limpiarCamposVistaProveedor();
                break;




            /**
             *Cuando se active esta orden se comprobará si hay algún campo vacío. En caso contrario, nos emite una
             * ventana.
             * Se crea un producto. Se recojen los datos de cada caja de texto de la ficha y se modifica cada atributo.
             * Se da de alta.
             * Y se vuelve a utilizar el JList.
             *Hay distintos try-catch para cada situación. Si no hemos introducido el tipo de correcto, si no hemos seleccionado
             * la foto,
             */
            case "AnadirProducto":


                File ruta=null;
                byte[] bytes=null;
                if(vista.txtNombreProducto.getText().equalsIgnoreCase("") || vista.txtMarcaProducto.getText().equalsIgnoreCase("") ||
                        vista.txtPesoProducto.getText().equalsIgnoreCase("")  || vista.txtCategoriaProducto.getText().equalsIgnoreCase("")  ||
                        vista.txtDimensionesProducto.getText().equalsIgnoreCase("")  || vista.txtPrecioProducto.getText().equalsIgnoreCase("")  ||
                        vista.dateFechaProducto.getDate()==null || (!vista.rbEnvioFalseProducto.isSelected() && !vista.rbEnvioTrueProducto.isSelected()) ||
                        vista.dcbmAlmacenProducto.getSelectedItem()==null || vista.dcbmProveedorProducto.getSelectedItem()==null){
                    JOptionPane.showMessageDialog(null, "Error, rellene todos los campos, por favor. ");

                }else{
                    Producto nuevoProducto = new Producto();
                    nuevoProducto.setNombre(vista.txtNombreProducto.getText());
                    nuevoProducto.setMarca(vista.txtMarcaProducto.getText());
                    try {
                        nuevoProducto.setPesoEnGramos(Integer.parseInt(vista.txtPesoProducto.getText()));
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca un valor numérico entero en \"Peso\", por favor. ");
                        break;
                    }
                    nuevoProducto.setCategoria(vista.txtCategoriaProducto.getText());
                    nuevoProducto.setDimensiones(vista.txtDimensionesProducto.getText());

                    try{
                        nuevoProducto.setPrecio(Float.parseFloat(vista.txtPrecioProducto.getText()));
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca un valor numérico en \"Precio\", por favor. ");
                        break;
                    }
                    try {
                        nuevoProducto.setFechaCreacion(Date.valueOf(vista.dateFechaProducto.getDate()));
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca una fecha correctamente clicando en el botón de fecha " +
                                "y selecionando una, por favor. ");
                        break;
                    }
                    if(vista.rbEnvioTrueProducto.isSelected()){
                        nuevoProducto.setEnvioInternacional((byte)(true?1:0));
                    }
                    if(vista.rbEnvioFalseProducto.isSelected()){
                        nuevoProducto.setEnvioInternacional((byte)(false?1:0));
                    }
                    System.out.println(rutaFoto+"-------------------------------->");
                    //File ruta = new File(vista.txtRutaFotoProducto.getText());
                    try {
                        ruta = new File(rutaFoto);
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, vuelva a seleccionar una foto clicando en el botón \"Imagen\", por favor.");
                        break;
                    }


                    bytes=null;
                    try {
                        bytes = Files.readAllBytes(ruta.toPath());
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                    nuevoProducto.setImagen(bytes);
                    nuevoProducto.setAlmacen((Almacen) vista.dcbmAlmacenProducto.getSelectedItem());
                    nuevoProducto.setProveedor((Proveedor) vista.dcbmProveedorProducto.getSelectedItem());


                    //Para que después no se quede guardada esa ruta y no dé problemas.
                    rutaFoto=null;
                    try{
                        modelo.altaProducto(nuevoProducto);
                    }catch (PersistenceException ex){
                        JOptionPane.showMessageDialog(null, "Error, Nombre repetido. Sólo puede haber un único Nombre, gracias.");
                    }

                    listarProductos(modelo.getProductos());
                }

                break;
            /**
             * Cuando se ejecuta esta orden se modifica un producto seleccionado en el JList de productos.
             * Primero nos avisará de si no hemos seleccionado un producto que lo hagamos.
             * Después, comprueba si hemos rellenado todos los campos, que no hay ninguno vacío.
             * Cuando lo ha hecho, modifica los atributos que hemos puesto en la ficha del producto seleccionado.
             * Cuando termina, vuelve a listar el JList de productos.
             */
            case "ModificarProducto":


                Producto productoSeleccion =null;
                try{
                    productoSeleccion = (Producto) vista.listProducto.getSelectedValue();
                    productoSeleccion.setNombre(vista.txtNombreProducto.getText());
                }catch (Exception ex){
                    JOptionPane.showMessageDialog(null, "Error, seleccione un producto" +
                            " para poder modificarlo, por favor. ");
                    break;
                }
                if(vista.txtNombreProducto.getText().equalsIgnoreCase("") || vista.txtMarcaProducto.getText().equalsIgnoreCase("") ||
                        vista.txtPesoProducto.getText().equalsIgnoreCase("")  || vista.txtCategoriaProducto.getText().equalsIgnoreCase("")  ||
                        vista.txtDimensionesProducto.getText().equalsIgnoreCase("")  || vista.txtPrecioProducto.getText().equalsIgnoreCase("")  ||
                        vista.dateFechaProducto.getDate()==null || (!vista.rbEnvioFalseProducto.isSelected() && !vista.rbEnvioTrueProducto.isSelected()) ||
                        vista.dcbmAlmacenProducto.getSelectedItem()==null || vista.dcbmProveedorProducto.getSelectedItem()==null){
                    JOptionPane.showMessageDialog(null, "Error, rellene todos los campos, por favor. ");

                }else {
                    productoSeleccion =null;
                    try{
                        productoSeleccion = (Producto) vista.listProducto.getSelectedValue();
                        productoSeleccion.setNombre(vista.txtNombreProducto.getText());
                    }catch (Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, seleccione un producto" +
                                " para poder modificarlo, por favor. ");
                        break;
                    }
                    productoSeleccion.setMarca(vista.txtMarcaProducto.getText());
                    try{
                        productoSeleccion.setPesoEnGramos(Integer.parseInt(vista.txtPesoProducto.getText()));
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca un valor numérico entero en \"Peso\", por favor. ");
                        break;
                    }
                    productoSeleccion.setCategoria(vista.txtCategoriaProducto.getText());
                    productoSeleccion.setDimensiones(vista.txtDimensionesProducto.getText());
                    try{
                        productoSeleccion.setPrecio(Float.parseFloat(vista.txtPrecioProducto.getText()));
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca un valor numérico en \"Precio\", por favor. ");
                        break;
                    }
                    try{
                        productoSeleccion.setFechaCreacion(Date.valueOf(vista.dateFechaProducto.getDate()));
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca una fecha correctamente clicando en el botón de fecha " +
                                "y selecionando una, por favor. ");
                        break;
                    }
                    if (vista.rbEnvioTrueProducto.isSelected()) {
                        productoSeleccion.setEnvioInternacional((byte) (true ? 1 : 0));
                    }

                    if (vista.rbEnvioFalseProducto.isSelected()) {
                        productoSeleccion.setEnvioInternacional((byte) (false ? 1 : 0));
                    }

                    if (rutaFoto != null) {
                        ruta = new File(rutaFoto);


                        bytes = null;

                        try {
                            bytes = Files.readAllBytes(ruta.toPath());
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        productoSeleccion.setImagen(bytes);
                    }
                    productoSeleccion.setAlmacen((Almacen) vista.dcbmAlmacenProducto.getSelectedItem());
                    productoSeleccion.setProveedor((Proveedor) vista.dcbmProveedorProducto.getSelectedItem());
                    try{
                        modelo.modificarProducto(productoSeleccion);
                    }catch (PersistenceException ex){
                        JOptionPane.showMessageDialog(null, "Error, Nombre repetido. Sólo puede haber un único Nombre, gracias.");
                    }
                    //MODIFICACIÓN ÚLTIMA 25-4-21
                    modificarTotalesPedidos(modelo.getPedidos());
                    listarPedidos(modelo.getPedidos());
                    listarCaracteristicasPedido(modelo.getCaracteristicasPedido());

                    listarProductos(modelo.getProductos());
                }
                break;
            /**
             * Cuando se ejecuta esta orden, se nos ordena el JList de proveedores por nombre en orden alfabético.
             *
             */
            case "ListarProducto":
                listarProductosPorNombreAlfabetico(modelo.getProductosPorOrdenAlfabetico());


                break;
            /**
             * Cuando se ejecuta este orden, se nos borrará un producto que hayamos seleccionado.
             * También nos avisará con try-catch, si hay errores de persistencia o no hemos seleccionado
             * un producto.
             * Se nos borrarán los campos de la ficha.
             */
            case "EliminarProducto":
                try {
                    Producto productoBorrado = (Producto) vista.listProducto.getSelectedValue();
                    modelo.borrarProducto(productoBorrado);

                    listarProductos(modelo.getProductos());
                }catch (PersistenceException ex){
                    JOptionPane.showMessageDialog(null,"Error de persistencia, tiene que borrar antes " +
                            "las \"CaracterísticasPedido\" asociadas este producto, por favor.");
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null,"Error, seleccione un producto para " +
                            "poder borrar, por favor.");
                }
                limpiarCamposVistaProducto();
                break;
            /**
             * Cuando se ejecuta esta orden, se nos abre una ventan que nos permitirá elegir una foto para el campo Imagen de la
             * clase producto.
             */
            case "ImagenProducto":
                try {
                    JFileChooser j = new JFileChooser();
                    FileNameExtensionFilter fil = new FileNameExtensionFilter("JPG, PNG & GIF", "jpg", "png", "gif");
                    j.setFileFilter(fil);

                    int s = j.showOpenDialog(this);
                    if (s == JFileChooser.APPROVE_OPTION) {
                        rutaFoto = j.getSelectedFile().getAbsolutePath();

                        vista.labelFoto.setIcon(ResizeImage(rutaFoto));


                    }
                }catch (Exception ex){
                    JOptionPane.showMessageDialog(null,"Error, seleccione una imagen de tipo \"JPG\", \"PNG\" o \"GIF\", por favor.");
                }
                break;
            /**
             *Cuando se active esta orden se comprobará si hay algún campo vacío. En caso contrario, nos emite una
             * ventana.
             * Se crea un almacén. Se recojen los datos de cada caja de texto de la ficha y se modifica cada atributo.
             * Se da de alta.
             * Y se vuelve a utilizar el JList.
             *Hay distintos try-catch para cada situación. Si no hemos introducido el tipo de correcto, etc.
             */
            case "AnadirAlmacen":

                if(vista.txtNombreAlmacen.getText().equalsIgnoreCase("")||
                        vista.txtTelefonoAlmacen.getText().equalsIgnoreCase("")||
                        vista.txtCorreoElectronicoAlmacen.getText().equalsIgnoreCase("")||
                        vista.txtMetrosCuadradosAlmacen.getText().equalsIgnoreCase("")||
                        vista.txtCantidadTrabajadoresAlmacen.getText().equalsIgnoreCase("")||
                        vista.txtCiudadAlmacen.getText().equalsIgnoreCase("")||
                        vista.txtCalleAlmacen.getText().equalsIgnoreCase("")||
                        vista.txtCodigoPostalAlmacen.getText().equalsIgnoreCase("")) {
                    JOptionPane.showMessageDialog(null, "Error, rellene todos los campos, por favor. ");
                }else{
                    Almacen nuevoAlmacen = new Almacen();
                    nuevoAlmacen.setNombre(vista.txtNombreAlmacen.getText());
                    try{
                        nuevoAlmacen.setTelefono(Integer.parseInt(vista.txtTelefonoAlmacen.getText()));
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca un valor numérico entero en \"Teléfono\", por favor. ");
                        break;
                    }
                    nuevoAlmacen.setCorreoElectronico(vista.txtCorreoElectronicoAlmacen.getText());
                    try{
                    nuevoAlmacen.setMetrosCuadrados(Integer.parseInt(vista.txtMetrosCuadradosAlmacen.getText()));
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca un valor numérico entero en \"Metros cuadrados\", por favor. ");
                        break;
                    }
                    try{
                        nuevoAlmacen.setCantidadTrabajadores(Integer.parseInt(vista.txtCantidadTrabajadoresAlmacen.getText()));
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca un valor numérico entero en \"Cantidad trabajadores\", por favor. ");
                        break;
                    }
                    nuevoAlmacen.setCiudad(vista.txtCiudadAlmacen.getText());
                    nuevoAlmacen.setCalle(vista.txtCalleAlmacen.getText());
                    try{
                        nuevoAlmacen.setCodigoPostal(Integer.parseInt(vista.txtCodigoPostalAlmacen.getText()));
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca un valor numérico entero en \"Código postal\", por favor. ");
                        break;
                    }
                    try{
                        modelo.altaAlmacen(nuevoAlmacen);
                    }catch (PersistenceException ex){
                        JOptionPane.showMessageDialog(null, "Error, Nombre repetido. Sólo puede haber un único Nombre, gracias.");
                    }
                    listarAlmacenes(modelo.getAlmacenes());
                }
                break;
            /**
             * Cuando se ejecuta esta orden se modifica un almacén seleccionado en el JList de almacenes.
             * Primero nos avisará de si no hemos seleccionado un almacén que lo hagamos.
             * Después, comprueba si hemos rellenado todos los campos, que no hay ninguno vacío.
             * Cuando lo ha hecho, modifica los atributos que hemos puesto en la ficha del almacén seleccionado.
             * Cuando termina, vuelve a listar el JList de almacenes.
             */
            case "ModificarAlmacen":
                Almacen almacenSeleccion =null;
                try {
                    almacenSeleccion = (Almacen) vista.listAlmacen.getSelectedValue();
                    almacenSeleccion.setNombre(vista.txtNombreAlmacen.getText());
                }catch (Exception ex){
                    JOptionPane.showMessageDialog(null, "Error, seleccione un almacén" +
                            " para poder modificarlo, por favor. ");
                    break;
                }
                if(vista.txtNombreAlmacen.getText().equalsIgnoreCase("")||
                        vista.txtTelefonoAlmacen.getText().equalsIgnoreCase("")||
                        vista.txtCorreoElectronicoAlmacen.getText().equalsIgnoreCase("")||
                        vista.txtMetrosCuadradosAlmacen.getText().equalsIgnoreCase("")||
                        vista.txtCantidadTrabajadoresAlmacen.getText().equalsIgnoreCase("")||
                        vista.txtCiudadAlmacen.getText().equalsIgnoreCase("")||
                        vista.txtCalleAlmacen.getText().equalsIgnoreCase("")||
                        vista.txtCodigoPostalAlmacen.getText().equalsIgnoreCase("")) {
                    JOptionPane.showMessageDialog(null, "Error, rellene todos los campos, por favor. ");
                }else {
                    almacenSeleccion =null;
                    try {
                        almacenSeleccion = (Almacen) vista.listAlmacen.getSelectedValue();
                        almacenSeleccion.setNombre(vista.txtNombreAlmacen.getText());
                    }catch (Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, seleccione un almacén" +
                                " para poder modificarlo, por favor. ");
                        break;
                    }
                    try{
                        almacenSeleccion.setTelefono(Integer.parseInt(vista.txtTelefonoAlmacen.getText()));
                    }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Error, introduzca un valor numérico entero en \"Teléfono\", por favor. ");
                    break;
            }
                    almacenSeleccion.setCorreoElectronico(vista.txtCorreoElectronicoAlmacen.getText());
                try{
                    almacenSeleccion.setMetrosCuadrados(Integer.parseInt(vista.txtMetrosCuadradosAlmacen.getText()));
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Error, introduzca un valor numérico entero en \"Metros cuadrados\", por favor. ");
                    break;
        }
                try{
                    almacenSeleccion.setCantidadTrabajadores(Integer.parseInt(vista.txtCantidadTrabajadoresAlmacen.getText()));
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Error, introduzca un valor numérico entero en \"Cantidad trabajadores\", por favor. ");
                    break;
                }
                almacenSeleccion.setCiudad(vista.txtCiudadAlmacen.getText());
                almacenSeleccion.setCalle(vista.txtCalleAlmacen.getText());
                try{
                    almacenSeleccion.setCodigoPostal(Integer.parseInt(vista.txtCodigoPostalAlmacen.getText()));
                }catch(Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca un valor numérico entero en \"Código postal\", por favor. ");
                        break;
                }
                try{
                    modelo.modificarAlmacen(almacenSeleccion);
                }catch (PersistenceException ex){
                    JOptionPane.showMessageDialog(null, "Error, Nombre repetido. Sólo puede haber un único Nombre, gracias.");
                }
                listarAlmacenes(modelo.getAlmacenes());
                }
                break;
            /**
             * Cuando se ejecuta esta orden, se nos ordena el JList de proveedores por nombre en orden alfabético.
             *
             */
            case "ListarAlmacen":
                listarAlmacenesPorNombreAlfabetico(modelo.getAlmacenesPorOrdenAlfabetico());
                //Por cantidad de trabajadores

                break;
            /**
             * Cuando se ejecuta este orden, se nos borrará un almacén que hayamos seleccionado.
             * También nos avisará con try-catch, si hay errores de persistencia o no hemos seleccionado
             * un almacén.
             * Se nos borrarán los campos de la ficha.
             */
            case "EliminarAlmacen":

                try{
                    Almacen almacenBorrado = (Almacen) vista.listAlmacen.getSelectedValue();
                    modelo.borrarAlmacen(almacenBorrado);
                    listarAlmacenes(modelo.getAlmacenes());
                }catch (PersistenceException ex){
                    JOptionPane.showMessageDialog(null,"Error de persistencia, tiene que borrar antes " +
                            "los productos asociados a este almacén, por favor.");
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null,"Error, seleccione un almacén para " +
                            "poder borrar, por favor.");
                }
                limpiarCamposVistaAlmacen();
                break;
            /**
             *Cuando se active esta orden se comprobará si hay algún campo vacío. En caso contrario, nos emite una
             * ventana.
             * Se crea un CaracteristicasPedido. Se recojen los datos de cada caja de texto de la ficha y se modifica cada atributo.
             * Se da de alta.
             * Y se vuelve a utilizar el JList.
             *Hay distintos try-catch para cada situación. Si no hemos introducido el tipo de correcto, etc.
             */
            case "AnadirProductoPedido":

                if(vista.dcbmProductoCaracteristicasPedido.getSelectedItem()==null||
                        vista.dcbmPedidoCaracteristicasPedido.getSelectedItem()==null||
                        vista.txtCantidadProductoPedido.getText().equalsIgnoreCase("")){
                    JOptionPane.showMessageDialog(null, "Error, rellene todos los campos, por favor. ");
                }else {
                    CaracteristicasPedido nuevoCaracteristicasPedido = new CaracteristicasPedido();
                    nuevoCaracteristicasPedido.setProducto((Producto) vista.dcbmProductoCaracteristicasPedido.getSelectedItem());
                    nuevoCaracteristicasPedido.setPedido((Pedido) vista.dcbmPedidoCaracteristicasPedido.getSelectedItem());
                    try {
                        nuevoCaracteristicasPedido.setCantidad(Integer.parseInt(vista.txtCantidadProductoPedido.getText()));
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca un valor numérico entero en \"Cantidad\", por favor. ");
                        break;
                    }

                    modelo.altaCaracteristicasPedido(nuevoCaracteristicasPedido);
                        modificarTotalesPedidos(modelo.getPedidos());
                        listarPedidos(modelo.getPedidos());
                        listarCaracteristicasPedido(modelo.getCaracteristicasPedido());
                }





                break;
            /**
             * Cuando se ejecuta esta orden se modifica un CaracteristicasPedido seleccionado en el JList de CaracteristicasPedido.
             * Primero nos avisará de si no hemos seleccionado un CaracteristicasPedido que lo hagamos.
             * Después, comprueba si hemos rellenado todos los campos, que no hay ninguno vacío.
             * Cuando lo ha hecho, modifica los atributos que hemos puesto en la ficha del CaracteristicasPedido seleccionado.
             * Cuando termina, vuelve a listar el JList de CaracteristicasPedidos.
             */
            case "ModificarProductoPedido":
                CaracteristicasPedido caracteristicasPedidoSeleccion=null;
                try {
                    caracteristicasPedidoSeleccion = (CaracteristicasPedido) vista.listCaracteristicasPedido.getSelectedValue();
                    caracteristicasPedidoSeleccion.setProducto((Producto) vista.dcbmProductoCaracteristicasPedido.getSelectedItem());
                }catch (Exception ex){
                    JOptionPane.showMessageDialog(null, "Error, seleccione un detalle de pedido" +
                            " para poder modificarlo, por favor. ");
                    break;
                }

                if(vista.dcbmProductoCaracteristicasPedido.getSelectedItem()==null||
                        vista.dcbmPedidoCaracteristicasPedido.getSelectedItem()==null||
                        vista.txtCantidadProductoPedido.getText().equalsIgnoreCase("")){
                    JOptionPane.showMessageDialog(null, "Error, seleccione un detalle de pedido, por favor. ");

                }else {
                    caracteristicasPedidoSeleccion=null;
                    try {
                        caracteristicasPedidoSeleccion = (CaracteristicasPedido) vista.listCaracteristicasPedido.getSelectedValue();
                        caracteristicasPedidoSeleccion.setProducto((Producto) vista.dcbmProductoCaracteristicasPedido.getSelectedItem());
                    }catch (Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, seleccione un detalle de pedido" +
                                " para poder modificarlo, por favor. ");
                        break;
                    }
                    caracteristicasPedidoSeleccion.setPedido((Pedido) vista.dcbmPedidoCaracteristicasPedido.getSelectedItem());
                    try{
                        caracteristicasPedidoSeleccion.setCantidad(Integer.parseInt(vista.txtCantidadProductoPedido.getText()));
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca un valor numérico entero en \"Cantidad\", por favor. ");
                        break;
                    }
                    modelo.modificarCaracteristicasPedido(caracteristicasPedidoSeleccion);
                    modificarTotalesPedidos(modelo.getPedidos());
                    listarPedidos(modelo.getPedidos());
                    listarCaracteristicasPedido(modelo.getCaracteristicasPedido());
                }
                break;
            /**
             * Cuando se ejecuta esta orden, se nos ordena el JList de CaracteristicasPedido por nombre en orden alfabético.
             *
             */
            case "ListarProductoPedido":
                listarCaracteristicasPedidoPorCantidadMayorAMenor(modelo.getCaracteristicasPedidoPorCantidad());


                break;
            /**
             * Cuando se ejecuta este orden, se nos borrará un almacén que hayamos seleccionado.
             * También nos avisará con try-catch, si hay errores de persistencia o no hemos seleccionado
             * un almacén.
             * Se nos borrarán los campos de la ficha.
             */
            case "EliminarProductoPedido":
                try {
                    CaracteristicasPedido caracteristicaPedidoBorrado = (CaracteristicasPedido) vista.listCaracteristicasPedido.getSelectedValue();
                    modelo.borrarCaracteristicasPedido(caracteristicaPedidoBorrado);
                    modificarTotalesPedidos(modelo.getPedidos());
                    listarPedidos(modelo.getPedidos());
                    listarCaracteristicasPedido(modelo.getCaracteristicasPedido());
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null,"Error, seleccione un CaracteristicasPedido para " +
                            "poder borrar, por favor.");
                }
                limpiarCamposVistaCaracteristicasPedido();
                break;

            /**
             *Cuando se active esta orden se comprobará si hay algún campo vacío. En caso contrario, nos emite una
             * ventana.
             * Se crea un almacén. Se recojen los datos de cada caja de texto de la ficha y se modifica cada atributo.
             * Se da de alta.
             * Y se vuelve a utilizar el JList.
             *Hay distintos try-catch para cada situación. Si no hemos introducido el tipo de correcto, etc.
             */
            case "AnadirPedido":

                if(vista.dcbmClientePedido.getSelectedItem()==null||
                        vista.dateFechaCompraPedido.getDate()==null/*||
                        vista.txtPrecioTotalPedido.getText().equalsIgnoreCase("")*/){
                    JOptionPane.showMessageDialog(null, "Error, rellene todos los campos, por favor. ");

                }else{
                    Pedido nuevoPedido = new Pedido();
                    nuevoPedido.setCliente((Cliente)vista.dcbmClientePedido.getSelectedItem());
                    try {
                        nuevoPedido.setFechaCompra(Date.valueOf(vista.dateFechaCompraPedido.getDate()));
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca una fecha correctamente clicando en el botón de fecha " +
                                "y selecionando una, por favor. ");
                        break;
                    }
                    /*
                    try {
                        nuevoPedido.setPrecioTotal(Float.parseFloat(vista.txtPrecioTotalPedido.getText()));
                    }catch (Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca un valor númerico en \"Precio total\", por favor. ");
                        break;
                    }
                    */
                    nuevoPedido.setPrecioTotal(0);
                    nuevoPedido.setRealizado("No");
                    modelo.altaPedido(nuevoPedido);
                    modificarTotalesPedidos(modelo.getPedidos());
                    listarPedidos(modelo.getPedidos());
                }

                break;
            /**
             * Cuando se ejecuta esta orden se modifica un Pedido seleccionado en el JList de Pedidos.
             * Primero nos avisará de si no hemos seleccionado un Pedido que lo hagamos.
             * Después, comprueba si hemos rellenado todos los campos, que no hay ninguno vacío.
             * Cuando lo ha hecho, modifica los atributos que hemos puesto en la ficha del Pedido seleccionado.
             * Cuando termina, vuelve a listar el JList de Pedidos.
             */
            case "ModificarPedido":
                Pedido pedidoSeleccion =null;
                try{
                    pedidoSeleccion = (Pedido) vista.listPedido.getSelectedValue();
                    pedidoSeleccion.setCliente((Cliente) vista.dcbmClientePedido.getSelectedItem());
                }catch (Exception ex){
                    JOptionPane.showMessageDialog(null, "Error, seleccione un almacén" +
                            " para poder modificarlo, por favor. ");
                    break;
                }
                pedidoSeleccion =null;
                if(vista.dcbmClientePedido.getSelectedItem()==null||
                        vista.dateFechaCompraPedido.getDate()==null/*||
                        vista.txtPrecioTotalPedido.getText().equalsIgnoreCase("")*/){
                    JOptionPane.showMessageDialog(null, "Error, rellene todos los campos, por favor. ");

                }else {
                    try{
                        pedidoSeleccion = (Pedido) vista.listPedido.getSelectedValue();
                        pedidoSeleccion.setCliente((Cliente) vista.dcbmClientePedido.getSelectedItem());
                    }catch (Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, seleccione un almacén" +
                                " para poder modificarlo, por favor. ");
                        break;
                    }
                    pedidoSeleccion.setFechaCompra(Date.valueOf(vista.dateFechaCompraPedido.getDate()));
                    /*
                    try{
                        pedidoSeleccion.setPrecioTotal(Float.parseFloat(vista.txtPrecioTotalPedido.getText()));
                    }catch (Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca un valor númerico en \"Precio total\", por favor. ");
                        break;
                    }
                    */
                    modelo.modificarPedido(pedidoSeleccion);

                    listarPedidos(modelo.getPedidos());
                }
                break;
            /**
             * Cuando se ejecuta esta orden, se nos ordena el JList de CaracteristicasPedido por nombre en orden alfabético.
             *
             */
            case "ListarPedido":
                listarPedidoPorPrecioDeMayorAMenor(modelo.getPedidosOrdenadosPorPrecio());


                break;
            /**
             * Cuando se ejecuta este orden, se nos borrará un pedido que hayamos seleccionado.
             * También nos avisará con try-catch, si hay errores de persistencia o no hemos seleccionado
             * un almacén.
             * Se nos borrarán los campos de la ficha.
             */
            case "EliminarPedido":
                try{
                    Pedido pedidoBorrado = (Pedido) vista.listPedido.getSelectedValue();
                    modelo.borrarPedido(pedidoBorrado);
                    listarPedidos(modelo.getPedidos());
                }catch (PersistenceException ex){
                    JOptionPane.showMessageDialog(null,"Error de persistencia, tiene que borrar antes " +
                            "los \"Detalles de pedido (CaracterísticasPedido)\" asociados a este pedido, por favor.");
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null,"Error, seleccione un pedido para " +
                            "poder borrar, por favor.");
                }
                limpiarCamposVistaPedido();
                break;

                /**
                 *Cuando se active esta orden se comprobará si hay algún campo vacío. En caso contrario, nos emite una
                 * ventana.
                 * Se crea un cliente. Se recojen los datos de cada caja de texto de la ficha y se modifica cada atributo.
                 * Se da de alta.
                 * Y se vuelve a utilizar el JList.
                 *Hay distintos try-catch para cada situación. Si no hemos introducido el tipo de correcto, etc.
                 */
            case "AnadirCliente":
                if(vista.txtNombreCliente.getText().equalsIgnoreCase("")||
                        vista.txtApellidosCliente.getText().equalsIgnoreCase("")||
                        vista.txtTelefonoCliente.getText().equalsIgnoreCase("")||
                        vista.txtCorreoElectronicoCliente.getText().equalsIgnoreCase("")||
                        vista.txtNumeroTarjetaCliente.getText().equalsIgnoreCase("")||
                        vista.txtPaisCliente.getText().equalsIgnoreCase("")||
                        vista.txtCiudadCliente.getText().equalsIgnoreCase("")||
                        vista.txtCalleCliente.getText().equalsIgnoreCase("")||
                        vista.txtCodigoPostalCliente.getText().equalsIgnoreCase("")||
                        vista.txtDNICliente.getText().equalsIgnoreCase("")){
                    JOptionPane.showMessageDialog(null, "Error, rellene todos los campos, por favor. ");
                }else{
                    Cliente nuevoCliente = new Cliente();
                    nuevoCliente.setNombre(vista.txtNombreCliente.getText());
                    nuevoCliente.setApellidos(vista.txtApellidosCliente.getText());
                    try{
                        nuevoCliente.setTelefono(Integer.parseInt(vista.txtTelefonoCliente.getText()));
                    }catch (Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca un valor númerico entero en \"Teléfono\", por favor. ");
                        break;
                    }

                    nuevoCliente.setCorreoElectronico(vista.txtCorreoElectronicoCliente.getText());
                    nuevoCliente.setNumeroTarjeta(vista.txtNumeroTarjetaCliente.getText());
                    nuevoCliente.setPais(vista.txtPaisCliente.getText());
                    nuevoCliente.setCiudad(vista.txtCiudadCliente.getText());
                    nuevoCliente.setCalle(vista.txtCalleCliente.getText());
                    nuevoCliente.setCodigoPostal(vista.txtCodigoPostalCliente.getText());
                    nuevoCliente.setDni(vista.txtDNICliente.getText());
                    try{
                        modelo.altaCliente(nuevoCliente);
                    }catch (PersistenceException ex){
                        JOptionPane.showMessageDialog(null, "Error, DNI repetido. Sólo puede haber un único DNI, gracias.");
                    }
                    listarClientes(modelo.getClientes());
                }
                break;
                /**
                 * Cuando se ejecuta esta orden se modifica un cliente seleccionado en el JList de clientes.
                 * Primero nos avisará de si no hemos seleccionado un cliente que lo hagamos.
                 * Después, comprueba si hemos rellenado todos los campos, que no hay ninguno vacío.
                 * Cuando lo ha hecho, modifica los atributos que hemos puesto en la ficha del cliente seleccionado.
                 * Cuando termina, vuelve a listar el JList de clientes.
                 */
            case "ModificarCliente":
                Cliente clienteSeleccion =null;
                try{
                    clienteSeleccion = (Cliente) vista.listCliente.getSelectedValue();
                    clienteSeleccion.setNombre(vista.txtNombreCliente.getText());
                }catch (Exception ex){
                    JOptionPane.showMessageDialog(null, "Error, seleccione un cliente" +
                            " para poder modificarlo, por favor. ");
                    break;
                }
                if(vista.txtNombreCliente.getText().equalsIgnoreCase("")||
                        vista.txtApellidosCliente.getText().equalsIgnoreCase("")||
                        vista.txtTelefonoCliente.getText().equalsIgnoreCase("")||
                        vista.txtCorreoElectronicoCliente.getText().equalsIgnoreCase("")||
                        vista.txtNumeroTarjetaCliente.getText().equalsIgnoreCase("")||
                        vista.txtPaisCliente.getText().equalsIgnoreCase("")||
                        vista.txtCiudadCliente.getText().equalsIgnoreCase("")||
                        vista.txtCalleCliente.getText().equalsIgnoreCase("")||
                        vista.txtCodigoPostalCliente.getText().equalsIgnoreCase("")||
                        vista.txtDNICliente.getText().equalsIgnoreCase("")){
                    JOptionPane.showMessageDialog(null, "Error, rellene todos los campos, por favor. ");
                }else {
                    clienteSeleccion =null;
                    try{
                    clienteSeleccion = (Cliente) vista.listCliente.getSelectedValue();
                    clienteSeleccion.setNombre(vista.txtNombreCliente.getText());
                    }catch (Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, seleccione un cliente" +
                                " para poder modificarlo, por favor. ");
                        break;
                    }
                    clienteSeleccion.setApellidos(vista.txtApellidosCliente.getText());

                    try{
                        clienteSeleccion.setTelefono(Integer.parseInt(vista.txtTelefonoCliente.getText()));
                    }catch (Exception ex){
                        JOptionPane.showMessageDialog(null, "Error, introduzca un valor númerico entero en \"Teléfono\", por favor. ");
                        break;
                    }
                    clienteSeleccion.setCorreoElectronico(vista.txtCorreoElectronicoCliente.getText());
                    clienteSeleccion.setNumeroTarjeta(vista.txtNumeroTarjetaCliente.getText());
                    clienteSeleccion.setPais(vista.txtPaisCliente.getText());
                    clienteSeleccion.setCiudad(vista.txtCiudadCliente.getText());
                    clienteSeleccion.setCalle(vista.txtCalleCliente.getText());
                    clienteSeleccion.setCodigoPostal(vista.txtCodigoPostalCliente.getText());
                    clienteSeleccion.setDni(vista.txtDNICliente.getText());
                    try{
                        modelo.modificarCliente(clienteSeleccion);
                    }catch (PersistenceException ex){
                        JOptionPane.showMessageDialog(null, "Error, DNI repetido. Sólo puede haber un único DNI, gracias.");
                    }
                    listarClientes(modelo.getClientes());
                }
                break;
                /**
                 * Cuando se ejecuta esta orden, se nos ordena el JList de Clientes por nombre en orden alfabético.
                 *
                 */
            case "ListarCliente":
                listarClientesPorOrdenAlfabetico(modelo.getClientesPorOrdenAlfabetico());
                break;
            /**
             * Cuando se ejecuta este orden, se nos borrará un cliente que hayamos seleccionado.
             * También nos avisará con try-catch, si hay errores de persistencia o no hemos seleccionado
             * un cliente.
             * Se nos borrarán los campos de la ficha.
             */
            case "EliminarCliente":
                try{
                    Cliente clienteBorrado = (Cliente) vista.listCliente.getSelectedValue();
                    modelo.borrarCliente(clienteBorrado);
                    listarClientes(modelo.getClientes());
                }catch (PersistenceException ex){
                    JOptionPane.showMessageDialog(null,"Error de persistencia, tiene que borrar antes " +
                            "los pedidos asociados a este cliente, por favor.");
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null,"Error, seleccione un cliente para " +
                            "poder borrar, por favor.");
                }
                limpiarCamposVistaCliente();
                break;
            /**
             * Cuando se ejecute esta orden, si hemos seleccionado una factura se nos creará un informe a través de JasperReport
             * Si nos hemos seleccionado un pedido, nos avisará con un JOptionPane
             */
            case "generarFactura":
                JasperPrint informeLleno2=null;
                try {
                    pedidoSeleccion = (Pedido) vista.listPedido.getSelectedValue();
                    informeLleno2 = ReportGenerator.generarInformePorPedido(pedidoSeleccion.getId());
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Error, seleccione un pedido para " +
                            "poder realizar una factura, por favor. ");
                    break;
                }
                try {
                    JasperExportManager.exportReportToPdfFile(informeLleno2, "InformePorPedido.pdf");
                } catch (JRException ex) {
                    // TODO Auto-generated catch block
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Error, no pudimos crear el archivo en PDF, " +
                            "disculpe las molestias. ");
                }
                break;

            /**
             * Cuando se ejecute esta orden, si hemos seleccionado una factura se nos creará un informe con la cantidad de productos por
             * categoría a través de JasperReport
             * Si nos hemos seleccionado un pedido, nos avisará con un JOptionPane
             */
            case "cantidadProductosCategoria":

                JasperPrint informeLleno3 = ReportGenerator.generarInformeProductosPorCategorias();

                try {
                    JasperExportManager.exportReportToPdfFile(informeLleno3, "CantidadProductosCategoria.pdf");
                } catch (JRException ex) {
                    // TODO Auto-generated catch block
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Error, no pudimos crear el archivo en PDF, " +
                            "disculpe las molestias. ");
                }
                break;

            /**
             * Cuando se ejecute esta orden, si hemos seleccionado una factura se nos creará un informe con la cantidad de pedidos que ha realizaod un cliente
             * a través de JasperReport.
             * Si nos hemos seleccionado un pedido, nos avisará con un JOptionPane
             */
            case "cantPedidCliente":

                JasperPrint informeLleno4 = ReportGenerator.generarInformeCantidadPedidosCliente();
                // JasperPrint informeLleno = ReportGenerator.generarInformePeliculas();
                /*
                viewer = new JasperViewer(informeLleno4);
                viewer.setVisible(true);
                   */
                try {
                    JasperExportManager.exportReportToPdfFile(informeLleno4, "CantidadPedidosCliente.pdf");
                } catch (JRException ex) {
                    // TODO Auto-generated catch block
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Error, no pudimos crear el archivo en PDF, " +
                            "disculpe las molestias. ");
                }
                break;
            /**
             * Cuando se ejecute esta orden, si hemos seleccionado una factura se nos creará un informe con la cantidad de clientes que gastaron
             * más de 500€/mes a través de JasperReport.
             * Si nos hemos seleccionado un pedido, nos avisará con un JOptionPane
             */
            case "clientGastaronMas500Mes":

                JasperPrint informeLleno5 = ReportGenerator.    generarInforme500GastaronClientes();
                // JasperPrint informeLleno = ReportGenerator.generarInformePeliculas();
                /*
                viewer = new JasperViewer(informeLleno5);
                viewer.setVisible(true);
                */
                try {
                    JasperExportManager.exportReportToPdfFile(informeLleno5, "ClientesGastaronMas500Mes.pdf");
                } catch (JRException ex) {
                    // TODO Auto-generated catch block
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Error, no pudimos crear el archivo en PDF, " +
                            "disculpe las molestias. ");
                }
                break;

            /**
             * Cuando se ejecute esta orden, se nos enviará al login que se inicia cuando abrimos el programa.
             */
            case "CerrarSesion":
                vista.dispose();
                Login2 login = new Login2();
                break;
        }


    }

    /**
     * Método que nos redimensiona el tamaño de una imagen cuando lo seleccionemos de nuestro archivo.
     * @param imgPath Objeto imagen
     * @return
     */
    public ImageIcon ResizeImage(String imgPath){
        ImageIcon MyImage = new ImageIcon(imgPath);
        Image img = MyImage.getImage();
        //Image newImage = img.getScaledInstance(vista.labelFoto.getWidth(), vista.labelFoto.getHeight(),Image.SCALE_SMOOTH);
        Image newImage = img.getScaledInstance(150, 115,Image.SCALE_SMOOTH);
        ImageIcon image = new ImageIcon(newImage);
        return image;
    }

    /**
     * Método que nos redimensiona el tamaño de una imagen para verlo en la ficha
     * @param imagen Objeto imagen.
     * @return Objeto ImageIcon
     */
    public ImageIcon ResizeImage2(BufferedImage imagen){
        ImageIcon MyImage = new ImageIcon(imagen);
        Image img = MyImage.getImage();
        //Image newImage = img.getScaledInstance(vista.labelFoto.getWidth(), vista.labelFoto.getHeight(),Image.SCALE_SMOOTH);
        Image newImage = img.getScaledInstance(150, 115,Image.SCALE_SMOOTH);
        ImageIcon image = new ImageIcon(newImage);
        return image;
    }



    /**
     * Método que nos lista los proveedores que recibe de un arraylist. Será del arraylist con los nombre ordenados por orden
     * alfabético.
     * @param lista Objeto tipo ArrayList<Proveedor>
     */
    public void listarProveedoresPorNombreAlfabetico (ArrayList<Proveedor> lista){
        vista.dlmProveedor.clear();
        for (Proveedor unProveedor : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmProveedor.addElement(unProveedor);
        }
    }

    /**
     * Método que nos lista los proveedores que recibe de un arraylist.
     * @param lista Objeto tipo ArrayList<Proveedor>
     */
    public void listarProveedores (ArrayList<Proveedor> lista){
        vista.dlmProveedor.clear();
        for (Proveedor unProveedor : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmProveedor.addElement(unProveedor);
        }
    }

    /**
     * Método que nos lista los caracteristicaspedido que recibe de un arraylist.
     * @param lista Objeto tipo ArrayList<Producto>
     */
    public void listarProductosProveedor(ArrayList<Producto> lista){
        vista.dlmProductosProveedor.clear();
        for(Producto unProducto : lista){
            vista.dlmProductosProveedor.addElement(unProducto);
        }
    }


    //MÉTODOS PRODUCTOS
    /**
     * Método que nos lista los caracteristicaspedido que recibe de un arraylist. Los recibe por orden alfabético.
     *
     * @param lista  Objeto tipo ArrayList<Producto>
     */
    public void listarProductosPorNombreAlfabetico (ArrayList<Producto> lista){
        vista.dlmProducto.clear();
        for (Producto unProducto : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmProducto.addElement(unProducto);
        }
    }


    /**
     * Método que nos lista los productos que recibe de un arraylist.
     * @param lista  Objeto tipo ArrayList<Producto>
     */
    public void listarProductos (ArrayList<Producto> lista){
        vista.dlmProducto.clear();
        for (Producto unProducto : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmProducto.addElement(unProducto);
        }
    }




    /**
     * Método que nos lista los productos que hay en la ficha CaracteristicasPedido en el combobox.
     *
     */
    private void listarProductosCaractPedido(){
        List<Producto> listaProductos = modelo.getProductos();
        vista.dcbmProductoCaracteristicasPedido.removeAllElements();
        for(Producto producto : listaProductos){
            vista.dcbmProductoCaracteristicasPedido.addElement(producto);
        }
    }

    /**
     * Método que nos lista los Pedidos en la ficha CaracterísticasPedido en el combobox.
     */
    private void listarPedidosCaractPedido(){
        List<Pedido> listaPedidos = modelo.getPedidos();
        vista.dcbmPedidoCaracteristicasPedido.removeAllElements();
        for(Pedido pedido : listaPedidos){
            vista.dcbmPedidoCaracteristicasPedido.addElement(pedido);
        }
    }



    /**
     * Método que nos lista los almacenes en la ficha Almacenes. Recibe un arraylist que estará ordenado
     * alfabéticamente por nombre.
     * @param lista ArrayList de Almacen
     */
    public void listarAlmacenesPorNombreAlfabetico (ArrayList<Almacen> lista){
        vista.dlmAlmacen.clear();
        for (Almacen unAlmacen : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmAlmacen.addElement(unAlmacen);
        }
    }

    /**
     * Método que nos lista los almacenes en la ficha Almacenes. Recibe un arraylist que estará ordenado
     *  alfabéticamente por nombre.
     * @param lista  Objeto tipo ArrayList<Almacen>
     */
    public void listarAlmacenes (ArrayList<Almacen> lista){
        vista.dlmAlmacen.clear();
        for (Almacen unAlmacen : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmAlmacen.addElement(unAlmacen);
        }
    }

    /**
     * Método que nos lista de Productos de un determinado Almacén en la ficha Almacén. Recibe un arraylist.
     * @param lista Objeto tipo ArrayList<Producto>
     */
    public void listarProductosAlmacen (ArrayList<Producto> lista){
        vista.dlmProductoAlmacen.clear();
        for (Producto unProducto : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmProductoAlmacen.addElement(unProducto);
            System.out.println("--------------------------->");
        }
    }






    /**
     * Método que nos lista las CaracterísticasPedido de la ficha CaracterísticasPedido. Recibe un arraylist de
     * CaracterísticasPedido
     *
     * @param lista Objeto tipo ArrayList<CaracteristicasPedido>
     */
    public void listarCaracteristicasPedido (ArrayList<CaracteristicasPedido> lista){
        vista.dlmCaracteristicasPedidos.clear();
        for (CaracteristicasPedido unCaracteristicasPedido : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmCaracteristicasPedidos.addElement(unCaracteristicasPedido);
        }

    }

    /**
     * Método que nos lista las CaracteristicasPedido de CaracteristicasPedido según la cantidad de mayor a menor.
     * @param lista Objeto tipo ArrayList<CaracteristicasPedido>
     */
    public void listarCaracteristicasPedidoPorCantidadMayorAMenor (ArrayList<CaracteristicasPedido> lista){
        vista.dlmCaracteristicasPedidos.clear();
        for (CaracteristicasPedido unCaracteristicasPedido : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmCaracteristicasPedidos.addElement(unCaracteristicasPedido);
        }
    }


    public void listarProductosCaracteristicasPedido(ArrayList<Producto> lista){
        vista.dlmProductoCaracteristicasPedido.clear();
        for (Producto unProducto : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmProductoCaracteristicasPedido.addElement(unProducto);
        }
    }

    public void listarPedidosCaracteristicasPedido(ArrayList<Pedido> lista){
        vista.dlmPedidoCaracteristicasPedido.clear();
        for (Pedido unPedido : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmPedidoCaracteristicasPedido.addElement(unPedido);
        }
    }



    /**
     * Método que nos lista los pedidos en la ficha Pedidos
     * @param lista Objeto tipo ArrayList<Pedido>
     */
    public void listarPedidos (ArrayList<Pedido> lista){
        vista.dlmPedido.clear();
        for (Pedido unPedido : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmPedido.addElement(unPedido);
        }
    }
    /**
     * Método que nos lista los pedidos en la ficha Pedidos según el precio de mayor a menor
     * @param lista Objeto de ArrayList<Pedido>
     */
    public void listarPedidoPorPrecioDeMayorAMenor (ArrayList<Pedido> lista){
        vista.dlmPedido.clear();
        for (Pedido unPedido : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmPedido.addElement(unPedido);
        }
    }


    /**
     * Método que nos las CaracterísticasPedido en la ficha Pedidos. Recibe un arraylist de CaracteristicasPedido
     * @param lista Objeto de ArrayList
     */
    public void listarCaracteristicasPedidoPedido(ArrayList<CaracteristicasPedido> lista){
        vista.dlmCaracteristicasPedidoPedido.clear();
        for(CaracteristicasPedido unCaracteristicasPedido: lista){
            vista.dlmCaracteristicasPedidoPedido.addElement(unCaracteristicasPedido);
            System.out.println("----------------------->");
        }
    }

    public void listarProductosCaractPedidoPedido(ArrayList<Producto> lista){
        vista.dlmProductoCaracteristicasPedidoPedido.clear();
        for(Producto unProducto:lista){
            vista.dlmProductoCaracteristicasPedidoPedido.addElement(unProducto);
        }
    }




    /**
     * Método que nos lista los clientes de la ficha Clientes. Recibe un ArrayList de tipo Cliente
     * @param lista Objeto de ArrayList
     */
    public void listarClientes (ArrayList<Cliente> lista){
        vista.dlmCliente.clear();
        for (Cliente unCliente : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmCliente.addElement(unCliente);
        }
    }

    /**
     * Método que nos lista los clientes de la ficha Cliente por orden alfabético. Recibe un ArrayList de tipo Cliente.
     * @param lista Objeto de ArrayList
     */
    public void listarClientesPorOrdenAlfabetico (ArrayList<Cliente> lista){
        vista.dlmCliente.clear();
        for (Cliente unCliente : lista) {
            //System.out.println(unProveedor.getCorreo());
            vista.dlmCliente.addElement(unCliente);
        }
    }

    /**
     * Método que nos lista los pedidos que hizo un cliente. Recibe un arraylist de tipo Pedido.
     * @param lista Objeto tipo ArrayList<Pedido>
     */
    public void listarPedidosCliente(ArrayList<Pedido> lista){
        vista.dlmPedidosCliente.clear();
        for(Pedido unPedido : lista) {
            System.out.println("-------------------------------->");
            vista.dlmPedidosCliente.addElement(unPedido);
        }
    }




    /**
     * Método que nos rellenará el combobox de almacenes que hay en la ficha producto.
     */
    private void listarAlmacenesProducto(){
        List<Almacen> listaAlmacenes = modelo.getAlmacenes();
        vista.dcbmAlmacenProducto.removeAllElements();
        for(Almacen almacen : listaAlmacenes){
            vista.dcbmAlmacenProducto.addElement(almacen);
        }
    }
    /**
     * Método que nos rellenará el combobox de proveedores que hay en la ficha producto.
     */
    private void listarProveedoresProducto(){
        List<Proveedor> listaProveedores = modelo.getProveedores();
        vista.dcbmProveedorProducto.removeAllElements();
        for(Proveedor proveedor : listaProveedores){
            vista.dcbmProveedorProducto.addElement(proveedor);
        }
    }

    /**
     * Método que nos rellenará el combobox de proveedores que hay en la ficha producto.
     */
    private void listarClientesPedido(){
        List<Cliente> listaClientes = modelo.getClientes();
        vista.dcbmClientePedido.removeAllElements();
        for(Cliente cliente : listaClientes){
            vista.dcbmClientePedido.addElement(cliente);
        }
    }




    /*
    private void iniciarTablaProveedorProducto(){
        String[] headers={"Nombre","NIF"};
        vista.dlmProveedorProducto.setColumnIdentifiers(headers);
    }


     */





    /**
     * Método que nos calcula el PrecioTotal de un pedido. Recibe un arraylist de CaracteristicasPedido.
     * @param list Objeto de arraylist.
     * @return Objeto tipo double
     */
    private double calcularPrecioTotalPedido(ArrayList<CaracteristicasPedido> list){
        double precioTotal=0;
        double precioSubTotal=0;
        for (int i=0;i<list.size();i++){
            precioSubTotal=0;
            precioSubTotal=list.get(i).getCantidad()*list.get(i).getProducto().getPrecio();
            precioTotal+=precioSubTotal;
        }
        return precioTotal;
    }



    /**
     * Método que nos modifica los totales de cada pedido. Recibe un ArrayList de Pedido.
     * @param list Objeto tipo ArrayList<Pedido>
     */
    private void modificarTotalesPedidos(ArrayList<Pedido> list){
        for(int i=0;i<list.size();i++){
            double precioTotal=0;
            precioTotal=calcularPrecioTotalPedido(modelo.getCaracteristicasPedidoDeUnPedido(list.get(i)));
            list.get(i).setPrecioTotal(precioTotal);
            modelo.modificarPedido(list.get(i));
        }
    }




    /**
     * Método que nos permite limpiar los campos de la ficha Proveedor
     */
    public void limpiarCamposVistaProveedor(){
        vista.txtNombreProveedor.setText("");
        vista.txtTelefonoProveedor.setText("");
        vista.txtCorreoElectronicoProveedor.setText("");
        vista.txtPaisProveedor.setText("");
        vista.txtCiudadProveedor.setText("");
        vista.txtCalleProveedor.setText("");
        vista.txtNumeroProveedor.setText("");
        vista.txtCodigoPostalProveedor.setText("");
        vista.txtNIFProveedor.setText("");
        vista.txtPersonaContactoProveedor.setText("");
    }


    /**
     * Método que nos permite limpiar los campos de la ficha Producto
     */
    public void limpiarCamposVistaProducto(){
        vista.txtNombreProducto.setText("");
        vista.txtMarcaProducto.setText("");
        vista.txtPesoProducto.setText("");
        vista.txtCategoriaProducto.setText("");
        vista.txtDimensionesProducto.setText("");
        vista.txtPrecioProducto.setText("");
        vista.dateFechaProducto.setDate(null);
        vista.labelFoto.setIcon(null);

        vista.dcbmAlmacenProducto.setSelectedItem(null);
        vista.dcbmProveedorProducto.setSelectedItem(null);
    }


    /**
     * Método que nos permite limpiar los campos de la ficha Almacen
     */
    public void limpiarCamposVistaAlmacen(){
        vista.txtNombreAlmacen.setText("");
        vista.txtTelefonoAlmacen.setText("");
        vista.txtCorreoElectronicoAlmacen.setText("");
        vista.txtMetrosCuadradosAlmacen.setText("");
        vista.txtCantidadTrabajadoresAlmacen.setText("");
        vista.txtCiudadAlmacen.setText("");
        vista.txtCalleAlmacen.setText("");
        vista.txtCodigoPostalAlmacen.setText("");
    }


    /**
     * Método que nos permite limpiar los campos de la ficha CaracteristicasPedido
     */
    public void limpiarCamposVistaCaracteristicasPedido(){
        vista.dcbmProductoCaracteristicasPedido.setSelectedItem(null);
        vista.dcbmPedidoCaracteristicasPedido.setSelectedItem(null);
        vista.txtCantidadProductoPedido.setText("");
    }

    /**
     * Método que nos permite limpiar los campos de la ficha Pedido
     */
    public void limpiarCamposVistaPedido(){
        vista.dcbmClientePedido.setSelectedItem(null);
        vista.dateFechaCompraPedido.setDate(null);
        vista.txtPrecioTotalPedido.setText(null);
    }

    /**
     * Método que nos permite limpiar los campos de la ficha Cliente
     */
    public void limpiarCamposVistaCliente(){
        vista.txtNombreCliente.setText("");
        vista.txtApellidosCliente.setText("");
        vista.txtTelefonoCliente.setText("");
        vista.txtCorreoElectronicoCliente.setText("");
        vista.txtNumeroTarjetaCliente.setText("");
        vista.txtPaisCliente.setText("");
        vista.txtCiudadCliente.setText("");
        vista.txtCalleCliente.setText("");
        vista.txtCodigoPostalCliente.setText("");
        vista.txtDNICliente.setText("");
    }

    /**
     *Método que nos calcula los precios totales de Pedidos
     */
    public void calcularPrecioTotalesDeTodos(){
        ArrayList<Pedido> lista= modelo.getPedidos();
        for(Pedido pedido: lista){
            Float precioTotal=(float)calcularPrecioTotalPedido(modelo.getCaracteristicasPedidoDeUnPedido(pedido));
            //System.out.println(precioTotal+"--------------------->PRECIO TOTAL");
            pedido.setPrecioTotal(precioTotal);
            modelo.modificarPedido(pedido);
        }
    }



    /**
     * Método que nos cargar los datos en cada comobobox del programa al ciclar sobre él.
     */
    @Override
    public void focusGained(FocusEvent e) {
        if(e.getSource() == vista.comboProveedorProducto) {
            //Cuando pincho en el combo, no despliego el menu
            vista.comboProveedorProducto.hidePopup();
            listarProveedoresProducto();
            //Despues de cargar los datos, si despliego el menu
            vista.comboProveedorProducto.showPopup();
        }else if(e.getSource() == vista.comboAlmacenProducto) {
            vista.comboAlmacenProducto.hidePopup();
            listarAlmacenesProducto();
            vista.comboAlmacenProducto.showPopup();
        }else if(e.getSource() == vista.comboProductoCaractPedido) {
            vista.comboProductoCaractPedido.hidePopup();
            listarProductosCaractPedido();
            vista.comboProductoCaractPedido.showPopup();
        }else if(e.getSource() == vista.comboPedidoCaractPedido) {
            vista.comboPedidoCaractPedido.hidePopup();
            listarPedidosCaractPedido();
            vista.comboPedidoCaractPedido.showPopup();
        }else if(e.getSource() == vista.comboClientePedido) {
            vista.comboClientePedido.hidePopup();
            listarClientesPedido();
            vista.comboClientePedido.showPopup();
        }
    }

    @Override
    public void focusLost(FocusEvent e) {

    }



    @Override
    public void itemStateChanged(ItemEvent e) {
        if(e.getSource() == vista.comboAlmacenProducto && e.getStateChange() == ItemEvent.SELECTED) {
            // listarAgenciasAH();

        }else if(e.getSource() == vista.comboProveedorProducto && e.getStateChange() == ItemEvent.SELECTED){
            // listarHabitacionesAH();
        }
    }
    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void stateChanged(ChangeEvent e) {

    }

    /**
     * Método que nos detectará cuando cliquemos sobre un registro de cada JList del programa.
     * @param e Objeto tipo ListSelectionEvent
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {


        if(e.getValueIsAdjusting()){
            /**
             * Nos rellena los campos de la ficha Proveedor.
             * También hace una consulta y lista los productos que tiene un proveedor en el JList correspondiente.
             */
            if(e.getSource() == vista.listProveedor) {
                Proveedor proveedorSeleccion = (Proveedor) vista.listProveedor.getSelectedValue();
                vista.txtNombreProveedor.setText(proveedorSeleccion.getNombre());
                vista.txtTelefonoProveedor.setText(String.valueOf(proveedorSeleccion.getTelefono()));
                vista.txtCorreoElectronicoProveedor.setText(proveedorSeleccion.getCorreoElectronico());
                vista.txtPaisProveedor.setText(proveedorSeleccion.getPais());
                vista.txtCiudadProveedor.setText(proveedorSeleccion.getCiudad());
                vista.txtCalleProveedor.setText(proveedorSeleccion.getCalle());
                vista.txtNumeroProveedor.setText(String.valueOf(proveedorSeleccion.getNumero()));
                vista.txtCodigoPostalProveedor.setText(String.valueOf(proveedorSeleccion.getCodigoPostal()));
                vista.txtNIFProveedor.setText(proveedorSeleccion.getNif());
                vista.txtPersonaContactoProveedor.setText(proveedorSeleccion.getPersonaContacto());
                listarProductosProveedor(modelo.getProductosProveedor(proveedorSeleccion));
            }

            /**
             * Nos rellena los campos de la ficha Producto.
             *
             */
            if(e.getSource() == vista.listProducto) {

                Producto productoSeleccion = (Producto) vista.listProducto.getSelectedValue();
                vista.txtNombreProducto.setText(productoSeleccion.getNombre());
                vista.txtMarcaProducto.setText(String.valueOf(productoSeleccion.getMarca()));
                vista.txtPesoProducto.setText(String.valueOf(productoSeleccion.getPesoEnGramos()));
                vista.txtCategoriaProducto.setText(productoSeleccion.getCategoria());
                vista.txtDimensionesProducto.setText(productoSeleccion.getDimensiones());
                vista.txtPrecioProducto.setText(String.valueOf(productoSeleccion.getPrecio()));
                vista.dateFechaProducto.setDate(productoSeleccion.getFechaCreacion().toLocalDate());
                if(productoSeleccion.getEnvioInternacional()!=0){
                    vista.rbEnvioTrueProducto.doClick();
                }else{
                    vista.rbEnvioFalseProducto.doClick();
                }





                ByteArrayInputStream bis = new ByteArrayInputStream(productoSeleccion.getImagen());
                BufferedImage bImage2 = null;
                try {
                    bImage2 = ImageIO.read(bis);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                try {
                    ImageIO.write(bImage2, "jpg", new File("output.jpg") );
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                vista.labelFoto.setIcon(ResizeImage2(bImage2));

                vista.dcbmAlmacenProducto.setSelectedItem(productoSeleccion.getAlmacen());
                vista.dcbmProveedorProducto.setSelectedItem(productoSeleccion.getProveedor());



            }
            /**
             * Nos rellena los campos de la ficha Almacén.
             *
             */
            if(e.getSource() == vista.listAlmacen) {


                Almacen almacenSeleccion = (Almacen) vista.listAlmacen.getSelectedValue();
                vista.txtNombreAlmacen.setText(almacenSeleccion.getNombre());
                vista.txtTelefonoAlmacen.setText(String.valueOf(almacenSeleccion.getTelefono()));
                vista.txtCorreoElectronicoAlmacen.setText(almacenSeleccion.getCorreoElectronico());
                vista.txtMetrosCuadradosAlmacen.setText(String.valueOf(almacenSeleccion.getMetrosCuadrados()));
                vista.txtCantidadTrabajadoresAlmacen.setText(String.valueOf(almacenSeleccion.getCantidadTrabajadores()));
                vista.txtCiudadAlmacen.setText(almacenSeleccion.getCiudad());
                vista.txtCalleAlmacen.setText(almacenSeleccion.getCalle());
                vista.txtCodigoPostalAlmacen.setText(String.valueOf(almacenSeleccion.getCodigoPostal()));
                listarProductosAlmacen(modelo.getProductosAlmacen(almacenSeleccion));

            }
            /**
             * Nos rellena los campos de la ficha CaracteristicasPedido.
             *
             */
            if(e.getSource() == vista.listCaracteristicasPedido) {
                CaracteristicasPedido caracteristicasPedidoSeleccion = (CaracteristicasPedido) vista.listCaracteristicasPedido.getSelectedValue();
                vista.dcbmProductoCaracteristicasPedido.setSelectedItem(caracteristicasPedidoSeleccion.getProducto());
                vista.dcbmPedidoCaracteristicasPedido.setSelectedItem(caracteristicasPedidoSeleccion.getPedido());
                vista.txtCantidadProductoPedido.setText(String.valueOf(caracteristicasPedidoSeleccion.getCantidad()));
            }
            /**
             * Nos rellena los campos de la ficha Pedido. Nos lista sus caracteristicaspedido en su JList
             */
            if(e.getSource() == vista.listPedido) {
                Pedido pedidoSeleccion = (Pedido) vista.listPedido.getSelectedValue();
                Float precioTotal=(float)calcularPrecioTotalPedido(modelo.getCaracteristicasPedidoDeUnPedido(pedidoSeleccion));
                //System.out.println(precioTotal+"--------------------->PRECIO TOTAL");
                pedidoSeleccion.setPrecioTotal(precioTotal);
                vista.dcbmClientePedido.setSelectedItem(pedidoSeleccion.getCliente());
                vista.dateFechaCompraPedido.setDate(pedidoSeleccion.getFechaCompra().toLocalDate());
                vista.txtPrecioTotalPedido.setText(String.valueOf(pedidoSeleccion.getPrecioTotal()));
                modelo.modificarPedido(pedidoSeleccion);
                listarCaracteristicasPedidoPedido(modelo.getCaracteristicasPedidoPedido(pedidoSeleccion));
                //BORRAR ESTO! SI FALLA
                vista.dlmProductoCaracteristicasPedidoPedido.clear();
            }

            /**
             * Nos rellena el JList de ProducotoCaracteristicasPedido en la ficha Pedido y nos saca el producto.
             */
            if(e.getSource() == vista.listCaracteristicasPedidoPedido) {
                CaracteristicasPedido caracteristicasPedidoSeleccion = (CaracteristicasPedido) vista.listCaracteristicasPedidoPedido.getSelectedValue();
                vista.dlmProductoCaracteristicasPedidoPedido.clear();
                vista.dlmProductoCaracteristicasPedidoPedido.addElement(caracteristicasPedidoSeleccion.getProducto().toString());
            }

            /**
             * Nos rellena los campos del JList de la ficha Cliente. Y también nos muestra en otro JList los pedidos que ha realizado.
             */
            if(e.getSource() == vista.listCliente) {
                Cliente clienteSeleccion = (Cliente) vista.listCliente.getSelectedValue();
                vista.txtNombreCliente.setText(clienteSeleccion.getNombre());
                vista.txtApellidosCliente.setText(clienteSeleccion.getApellidos());
                vista.txtTelefonoCliente.setText(String.valueOf(clienteSeleccion.getTelefono()));
                vista.txtCorreoElectronicoCliente.setText(clienteSeleccion.getCorreoElectronico());
                vista.txtNumeroTarjetaCliente.setText(clienteSeleccion.getNumeroTarjeta());
                vista.txtPaisCliente.setText(clienteSeleccion.getPais());
                vista.txtCiudadCliente.setText(clienteSeleccion.getCiudad());
                vista.txtCalleCliente.setText(clienteSeleccion.getCalle());
                vista.txtCodigoPostalCliente.setText(clienteSeleccion.getCodigoPostal());
                vista.txtDNICliente.setText(clienteSeleccion.getDni());
                listarPedidosCliente(modelo.getPedidosCliente(clienteSeleccion));
            }

        }
    }


}

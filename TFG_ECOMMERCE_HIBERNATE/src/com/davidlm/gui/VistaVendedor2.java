/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.gui;

import com.davidlm.base.Cliente;
import com.davidlm.base.Pedido;
import com.davidlm.base.Producto;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

/**
 * Clase vista del programa del usuario Vendedor
 */
public class VistaVendedor2 extends JFrame{
    private JPanel panel1;
    private JScrollPane Scrollpane;
    private JTabbedPane tabbedPane1;
     JTextField txtCantidadProductoPedido;
     JButton btnAnadirProductoPedido;
     JButton btnModificarProductoPedido;
     JButton btnEliminarProductoPedido;
     JButton btnListarProductoPedido;
     JComboBox comboProductoCaractPedido;
     JComboBox comboPedidoCaractPedido;
    JList listCaracteristicasPedido;
     JTextField txtPrecioTotalPedido;
     DatePicker dateFechaCompraPedido;
     JButton btnAnadirPedidoVendedor;
     JButton btnModificarPedidoVendedor;
     JButton btnEliminarPedidoVendedor;

     JButton btnListarPedidoVendedor;
     JList listProductosCaracteristicasPedidoPedido;
     JComboBox comboClientePedido;
     JList listCaracteristicasPedidoPedidoVendedor;
     JTextField txtTelefonoCliente;
     JButton btnAnadirCliente;
     JButton btnModificarCliente;
     JButton btnEliminarCliente;
     JList listCliente;
     JButton btnListarCliente;
     JList listPedidosCliente;
     JTextField txtCorreoElectronicoCliente;
     JTextField txtNumeroTarjetaCliente;
     JTextField txtPaisCliente;
     JTextField txtCiudadCliente;
     JTextField txtCalleCliente;
     JTextField txtCodigoPostalCliente;
     JTextField txtDNICliente;
     JTextField txtApellidosCliente;
     JTextField txtNombreCliente;
     JList listPedido;
     JButton btnGenerarFactura;


    DefaultListModel dlmCaracteristicasPedidos;
    DefaultListModel dlmProductoCaracteristicasPedido;
    DefaultListModel dlmPedidoCaracteristicasPedido;

    DefaultListModel dlmPedido;
    DefaultListModel dlmCaracteristicasPedidoPedido;
    DefaultListModel dlmProductoCaracteristicasPedidoPedido;



    DefaultListModel dlmCliente;
    DefaultListModel dlmPedidosCliente;

    /*
    DefaultTableModel dtmProveedorProducto;
    DefaultTableModel dtmCaracteristicasPedido;
    */


    DefaultComboBoxModel<Producto> dcbmProductoCaracteristicasPedido;
    DefaultComboBoxModel<Pedido> dcbmPedidoCaracteristicasPedido;
    DefaultComboBoxModel<Cliente> dcbmClientePedido;

    JMenuItem cerrarSesion;


    /**
     * Constructor de la vista del vendedor. Tiene las configuraciones y ejecuta varios métodos.
     *  - Tiene las configuraciones de la Ventana
     *  - Configura los DefaultListModel con los JList
     *  - Configura los DefaultComboBoxModel con los JComboBox
     */

    public VistaVendedor2(){
        this.setContentPane(Scrollpane);
        this.setExtendedState(this.MAXIMIZED_BOTH);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //this.setSize(new Dimension(this.getWidth()+50, this.getHeight()+50));
        //this.setLocationRelativeTo(null);
        this.pack();
        this.setVisible(true);

        txtPrecioTotalPedido.setEditable(false);
        crearModelos();
        inicializarCombos();
        crearMenus();

    }

    /**
     * Método que nos inicializa los DefaultListModel y nos los configura.
     */
    private void crearModelos() {


        dlmCaracteristicasPedidos=new DefaultListModel();
        listCaracteristicasPedido.setModel(dlmCaracteristicasPedidos);

        dlmPedido=new DefaultListModel();
        listPedido.setModel(dlmPedido);

        dlmCaracteristicasPedidoPedido=new DefaultListModel();
        listCaracteristicasPedidoPedidoVendedor.setModel(dlmCaracteristicasPedidoPedido);

        dlmProductoCaracteristicasPedidoPedido=new DefaultListModel();
        listProductosCaracteristicasPedidoPedido.setModel(dlmProductoCaracteristicasPedidoPedido);

        dlmCliente= new DefaultListModel();
        listCliente.setModel(dlmCliente);

        dlmPedidosCliente= new DefaultListModel();
        listPedidosCliente.setModel(dlmPedidosCliente);


    }


    /**
     * Método que nos inicializa los DefaultComboBox y nos los configura.
     */
    private void inicializarCombos(){


        //Caracteristicas pedido
        dcbmProductoCaracteristicasPedido =  new DefaultComboBoxModel<>();
        comboProductoCaractPedido.setModel(dcbmProductoCaracteristicasPedido);

        dcbmPedidoCaracteristicasPedido =  new DefaultComboBoxModel<>();
        comboPedidoCaractPedido.setModel(dcbmPedidoCaracteristicasPedido);


        //Pedido
        dcbmClientePedido=new DefaultComboBoxModel<>();
        comboClientePedido.setModel(dcbmClientePedido);
    }

    /**
     * Método que crea los botones de la barrá de menú. Configura el JMenuItem, el JMenu y el JMenuBar.
     */
    private void crearMenus(){
        /*
        itemConectar= new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");

         */
        cerrarSesion= new JMenuItem("Cerrar Sesión");
        cerrarSesion.setActionCommand("CerrarSesion");
        JMenu menuArchivo = new JMenu ("Archivo");

        // menuArchivo.add(itemConectar);
        menuArchivo.add(cerrarSesion);
        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);
        setJMenuBar(barraMenu);
    }
}

/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

/**
 * Clase que nos permitirá elegir entre varias opciones para el Administrador.
 */
public class Login3 extends JFrame {
    private JButton btnCrearUsuario;
    private JButton btnAbrirPrograma;
    private JButton btnBorrarUsuario;
    private JPanel panel1;


    /**
     * Constructor de la ventana para administradores que nos permite elegir entre opciones. Tiene sus configuraciones.
     * Cada botón te lleva una ventana distintia.
     * Tiene 3 botones:
     * -Abrir programa: Abre el programa en modo Administrador.
     * -Crear usuario: Te lleva a la ventana que te permite crear usuarios
     * -Borrar usuario: Te lleva a la ventan que te permite borrar usuarios
     */
    public Login3(){

        this.setBounds(100, 100, 450, 300);
        this.setResizable(false);
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        //this.pack();
        //frame.dispose();
        this.setVisible(true);

        btnAbrirPrograma.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Vista vista = new Vista();
                Modelo modelo = new Modelo();
                Controlador controlador = new Controlador(vista,modelo);
                dispose();
            }
        });

        btnCrearUsuario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Login5 crear = new Login5();
                dispose();
            }
        });

        btnBorrarUsuario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Login14BorrarUsuario login = new Login14BorrarUsuario();
                    dispose();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

            }
        });

    }
}

/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * Clase ventana que nos pregunta si estamos seguros de querer salir.
 */
public class Login12EstasSeguroSalir extends JFrame{
    private JButton btnSiLogin12;
    private JButton btnNoLogin12;
    private JPanel panel1;

    /**
     * Constructor de la ventana que nos pregunta si estamos segura de querer Salir.
     * Tiene las configuraciones.
     * Si clicamos en Sí se cierra el programa.
     * Si clicamos en No desaparece la ventana.
     */
    public Login12EstasSeguroSalir(){

        this.add(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 200);
        this.setLocationRelativeTo(null);
        //this.pack();
        //frame.dispose();
        this.setResizable(false);
        this.setVisible(true);

        btnSiLogin12.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        btnNoLogin12.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
}

/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.gui;

import com.davidlm.base.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.ArrayList;

/**
 * Clase Modelo del Modelo, Vista, Controlador de usuario tipo Empaquetador.
 */
public class ModeloEmpaquetador {


    SessionFactory sessionFactory;

    /**
     * Método que nos permitirá desconectar de la base de datos.
     */
    public void desconectar() {
        //Cierro la factoria de sessiones
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    /**
     * Método que nos permitirá conectar con la base de datos.
     */
    public void conectar() {
        Configuration configuracion = new Configuration();

        configuracion.configure("hibernate.cfg.xml");

        configuracion.addAnnotatedClass(Almacen.class);
        configuracion.addAnnotatedClass(Cliente.class);
        configuracion.addAnnotatedClass(CaracteristicasPedido.class);
        configuracion.addAnnotatedClass(Pedido.class);
        configuracion.addAnnotatedClass(Producto.class);
        configuracion.addAnnotatedClass(Proveedor.class);

        String user="root";
        configuracion.setProperty("hibernate.connection.username",user);
        configuracion.setProperty("hibernate.connection.password","");
        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();


        sessionFactory = configuracion.buildSessionFactory(ssr);

    }

    /**
     * Método que nos permitirá obtener los Pedidos. Nos devuelve un ArrayList
     * @return Objeto ArrayList<Pedido>
     */
    public ArrayList<Pedido> getPedidos() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Pedido ");
        ArrayList<Pedido> listaPedidos = (ArrayList<Pedido>)query.getResultList();
        sesion.close();
        return listaPedidos;
    }

    /**
     * Método que nos devuelve un arraylist con las CaracteristicasPedido de un Pedido que recibe.
     * @param unPedido Objeto tipo Pedido
     * @return Objeto tipo ArrayList<CaracteristicasPedido>
     */
    public ArrayList<CaracteristicasPedido> getCaracteristicasPedidoPedido(Pedido unPedido) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM CaracteristicasPedido WHERE pedido.id="+unPedido.getId());
        ArrayList<CaracteristicasPedido> listaCaracteristicasPedido= (ArrayList<CaracteristicasPedido>)query.getResultList();
        sesion.close();
        return listaCaracteristicasPedido;
    }

    /**
     * Método que nos permite modificar un pedido.
     * @param pedidoSeleccion Objeto tipo Pedido
     */
    public void modificarPedido(Pedido pedidoSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(pedidoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos devuelve un arraylist de las caracteristicaspedido de un pedido
     * @param unPedido Objeto de la clase Pedido.
     * @return Objeto ArrayList<Cliente>
     */
    public ArrayList<CaracteristicasPedido> getCaracteristicasPedidoDeUnPedido(Pedido unPedido) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM CaracteristicasPedido WHERE pedido.id = "+unPedido.getId());
        ArrayList<CaracteristicasPedido> listaCaracteristicaPedido = (ArrayList<CaracteristicasPedido>)query.getResultList();
        sesion.close();
        return listaCaracteristicaPedido;
    }

}

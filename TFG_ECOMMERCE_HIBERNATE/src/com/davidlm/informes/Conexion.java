/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.informes;

import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 * Clase Conexion  que nos permitirá con la base de datos para crear informes.
 */
public class Conexion {

    public static final String USER="root";
    public static final String PASSWORD="";

    /**
     * Método para conectar con la base de datos.
     * @return Objeto de tipo Connection
     */
    public static Connection getMySQLConection(){


        try{
            //Class.forName("com.msyql.jdbc.Driver");
            Connection conn= DriverManager.getConnection("jdbc:mysql://localhost:3306/baseecommerce?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", USER, PASSWORD);
            return conn;
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error de conexión, compruebe que tiene arracado XAMPP correctamente, por favor.");
            e.printStackTrace();
        }
        return null;
    }
}

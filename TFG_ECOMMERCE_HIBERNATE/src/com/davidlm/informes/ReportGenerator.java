/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.informes;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import javax.swing.*;
import java.util.HashMap;

/**
 * Clase ReportGenerator que tiene la configuración para crear los informes.
 */
public class ReportGenerator {
    public static final String INFORME_DINERO_CATEGORIAS = "DineroGanadoPorCategoria.jasper";



    public static JasperPrint generarInformeDavidLopezMellado(){

        try {
            JasperPrint informeLleno = JasperFillManager.fillReport(INFORME_DINERO_CATEGORIAS, new HashMap(), Conexion.getMySQLConection());

            return informeLleno;
        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }


    public static final String INFORMEPORPEDIDO ="FacturaPedidos.jasper";


    /**
     * Método que nos genera un informe en función del pedido.
     * @param pedido Objeto tipo int
     * @return Objeto JasperPrint
     */
    public static JasperPrint generarInformePorPedido (int pedido) {


        HashMap<String,Object> parametros = new HashMap<>();

        parametros.put("id_pedido", pedido);
        try {
            JasperPrint informeLleno = JasperFillManager.fillReport(INFORMEPORPEDIDO, parametros,Conexion.getMySQLConection());

            return informeLleno;
        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;

    }

    public static final String INFORME_PRODUCTOS_CATEGORIAS ="ProductosPorCategoria.jasper";

    /**
     * Método que nos genera un informe con un gráfico con la cantidad de productos por categorias.
     * @return Objeto JasperPrint
     */
    public static JasperPrint generarInformeProductosPorCategorias(){

        try {
            JasperPrint informeLleno = JasperFillManager.fillReport(INFORME_PRODUCTOS_CATEGORIAS, new HashMap(), Conexion.getMySQLConection());

            return informeLleno;
        } catch (JRException e) {
            // TODO Auto-generated catch block
            JOptionPane.showMessageDialog(null, "Error, no pudimos crear el informe, disculpe las molestias. ");
            e.printStackTrace();
        }

        return null;
    }

    public static final String INFORME_CANT_PEDIDOS_CLIENTE ="CantidadPedidosHechosPorCliente.jasper";

    /**
     * Método que nos genera un informe con la cantidad de pedidos que ha realizado cada cliente.
     * @return Objeto JasperPrint
     */
    public static JasperPrint generarInformeCantidadPedidosCliente(){

        try {
            JasperPrint informeLleno = JasperFillManager.fillReport(INFORME_CANT_PEDIDOS_CLIENTE, new HashMap(), Conexion.getMySQLConection());

            return informeLleno;
        } catch (JRException e) {
            // TODO Auto-generated catch block
            JOptionPane.showMessageDialog(null, "Error, no pudimos crear el informe, disculpe las molestias. ");
            e.printStackTrace();
        }

        return null;
    }

    public static final String INFORME_CLIENTE_500_MES ="ClientesGastoMas500.jasper";

    /**
     * Método que nos genera un informe con los clientes que gastaron más de 500€ este mes.
     * @return
     */
    public static JasperPrint generarInforme500GastaronClientes(){

        try {
            JasperPrint informeLleno = JasperFillManager.fillReport(INFORME_CLIENTE_500_MES, new HashMap(), Conexion.getMySQLConection());

            return informeLleno;
        } catch (JRException e) {
            // TODO Auto-generated catch block
            JOptionPane.showMessageDialog(null, "Error, no pudimos crear el informe, disculpe las molestias. ");
            e.printStackTrace();
        }

        return null;
    }
}


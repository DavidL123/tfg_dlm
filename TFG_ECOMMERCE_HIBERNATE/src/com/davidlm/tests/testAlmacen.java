/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.tests;

import com.davidlm.base.Almacen;
import com.davidlm.base.Cliente;
import com.davidlm.gui.Modelo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

/**
 * Clase que realiza tests para los métodos que interactúan con el objeto Almacén.
 */
public class testAlmacen {

    private Almacen almacenPrueba1;
    private Almacen almacenPrueba2;
    private Modelo modelo;

    private static final String NOMBRE="almacen1";
    private static final int TELEFONO=222222;
    private static final String CORREO_ELECTRONICO="almacen1@gmail.com";
    private static final int METROS_CUADRADOS=12345;
    private static final String CIUDAD="Madrid";
    private static final String CALLE="Avenida Goya";
    private static final int CODIGO_POSTAL=45878;
    private static final int CANTIDAD_TRABAJADORES=3;

    @BeforeEach
    public void prepare(){
        modelo= new Modelo();
        modelo.conectar();
        almacenPrueba1= new Almacen();
        almacenPrueba1.setNombre(NOMBRE);
        almacenPrueba1.setTelefono(TELEFONO);
        almacenPrueba1.setCorreoElectronico(CORREO_ELECTRONICO);
        almacenPrueba1.setMetrosCuadrados(METROS_CUADRADOS);
        almacenPrueba1.setCiudad(CIUDAD);
        almacenPrueba1.setCalle(CALLE);
        almacenPrueba1.setCodigoPostal(CODIGO_POSTAL);
        almacenPrueba1.setCantidadTrabajadores(CANTIDAD_TRABAJADORES);


        almacenPrueba2= new Almacen();
        almacenPrueba2.setNombre("almacen2");
        almacenPrueba2.setTelefono(97612452);
        almacenPrueba2.setCorreoElectronico("almacen2@gmail.com");;
        almacenPrueba2.setMetrosCuadrados(54812);
        almacenPrueba2.setCiudad("Barcelona");
        almacenPrueba2.setCalle("Salvador Allende nº3");
        almacenPrueba2.setCodigoPostal(21457);
        almacenPrueba2.setCantidadTrabajadores(56);

    }

    /**
     * Test que nos servirá para comprobar si se crea correctamente un almacén.
     */
    @Test
    void testCrearAlmacen(){
        modelo.altaAlmacen(almacenPrueba1);
        ArrayList<Almacen> listaAlmacenes=modelo.getAlmacenes();

        String esperado=null;
        for(Almacen almacen: listaAlmacenes){
            if(almacen.getCiudad().equalsIgnoreCase("Madrid")){
                esperado="Madrid";
            }
        }
        Assertions.assertNotNull(esperado);
    }

    /**
     * Test que nos servirá para comprobar si se borra correctamente un almacén.
     */
    @Test
    void testBorrarAlmacen(){

        modelo.altaAlmacen(almacenPrueba2);

        ArrayList<Almacen> listaAlmacenes1=modelo.getAlmacenes();
        Almacen almacenPrueba1=listaAlmacenes1.get(listaAlmacenes1.size()-1);
        modelo.borrarAlmacen(almacenPrueba1);

        ArrayList<Almacen> listaAlmacenes2=modelo.getAlmacenes();

        String esperado=null;
        for(Almacen almacen: listaAlmacenes2){
            if(almacen.getCiudad().equalsIgnoreCase("Barcelona")){
                esperado="Barcelona";
            }
        }
        Assertions.assertNull(esperado);


    }

}

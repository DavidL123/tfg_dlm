/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.tests;

import com.davidlm.base.Producto;
import com.davidlm.base.Proveedor;
import com.davidlm.gui.Modelo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

/**
 * Clase que realiza tests para los métodos que interactúan con el objeto Proveedor.
 */
public class testProveedor {

    private Proveedor proveedorPrueba1;
    private Proveedor proveedorPrueba2;
    private Modelo modelo;

    private String nombre;
    private int telefono;
    private String correoElectronico;
    private String ciudad;
    private String calle;
    private int numero;
    private int codigoPostal;
    private String pais;
    private String nif;
    private String personaContacto;

    private static final String NOMBRE="EnterpriseB";
    private static final int TELEFONO=222222;
    private static final String CORREO_ELECTRONICO="enterpriseb@gmail.com";
    private static final String CIUDAD="Madrid";
    private static final String CALLE="Avenida Goya";
    private static final int NUMERO=123456;
    private static final int CODIGO_POSTAL=151512;
    private static final String PAIS="España";
    private static final String NIF="M0200049H";
    private static final String PERSONA_CONTACTO="Gerardo Guerra";


    @BeforeEach
    public void prepare(){
        modelo= new Modelo();
        modelo.conectar();
        proveedorPrueba1= new Proveedor();
        proveedorPrueba1.setNombre(NOMBRE);
        proveedorPrueba1.setTelefono(TELEFONO);
        proveedorPrueba1.setCorreoElectronico(CORREO_ELECTRONICO);
        proveedorPrueba1.setCiudad(CIUDAD);
        proveedorPrueba1.setCalle(CALLE);
        proveedorPrueba1.setNumero(NUMERO);
        proveedorPrueba1.setCodigoPostal(CODIGO_POSTAL);
        proveedorPrueba1.setPais(PAIS);
        proveedorPrueba1.setNif(NIF);
        proveedorPrueba1.setPersonaContacto(PERSONA_CONTACTO);




        proveedorPrueba2= new Proveedor();
        proveedorPrueba2.setNombre("EnterprisesC");
        proveedorPrueba2.setTelefono(12345678);
        proveedorPrueba2.setCorreoElectronico("enterprisec@gmail.com");
        proveedorPrueba2.setCiudad("Sevilla");
        proveedorPrueba2.setCalle("C/Antonio Machado");
        proveedorPrueba2.setNumero(3);
        proveedorPrueba2.setCodigoPostal(501125);
        proveedorPrueba2.setPais("España");
        proveedorPrueba2.setNif("M0200333H");
        proveedorPrueba2.setPersonaContacto("Sandra Vaquerizo");
    }

    /**
     * Test que nos servirá para comprobar si se modifica correctamente un proveedor.
     */
    @Test
    void testModificarProveedor(){
        modelo.altaProveedor(proveedorPrueba1);
        ArrayList<Proveedor> listaProveedores=modelo.getProveedores();

        Proveedor proveedorPrueba3 =listaProveedores.get(listaProveedores.size()-1);
        proveedorPrueba3.setNif("M0202249H");

        modelo.modificarProveedor(proveedorPrueba3);
        ArrayList<Proveedor> listaProveedores2=modelo.getProveedores();
        String esperado=null;
        for(Proveedor proveedor: listaProveedores){
            if(proveedor.getNif().equalsIgnoreCase("M0202249H"));
            esperado="M0202249H";
        }

        Assertions.assertNotNull(esperado);
    }

    /**
     * Test que nos servirá para comprobar si se borra correctamente un proveedor.
     */
    @Test
    void testBorrarProveedor(){
        modelo.altaProveedor(proveedorPrueba2);

        ArrayList<Proveedor> listaProveedores1=modelo.getProveedores();
        Proveedor proveedorPrueba5= listaProveedores1.get(listaProveedores1.size()-1);
        modelo.borrarProveedor(proveedorPrueba5);



        String esperado=null;
        String nifNoEsperado="";
        listaProveedores1=modelo.getProveedores();
        for(Proveedor proveedor: listaProveedores1){
            if(proveedor.getNif().equalsIgnoreCase("M0200333H")){
                esperado="M0200333H";
                System.out.println("Esperado");
            }
        }
        Assertions.assertNull(esperado);
        /*
        Proveedor proveedorPrueba5= listaProveedores1.get(listaProveedores1.size()-1);
        modelo.borrarProveedor(proveedorPrueba5);

        ArrayList<Proveedor> listaProveedores2=modelo.getProveedores();
        String esperado="M0200333H";
        Assertions.assertNotEquals(esperado,listaProveedores2.get(listaProveedores2.size()-1).getNif());
        */

    }
}

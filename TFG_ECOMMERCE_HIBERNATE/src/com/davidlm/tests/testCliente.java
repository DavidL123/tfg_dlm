/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.tests;

import com.davidlm.base.*;
import com.davidlm.gui.Controlador;
import com.davidlm.gui.Modelo;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase que realiza tests para los métodos que interactúan con el objeto Cliente.
 */
public class testCliente {

    private Cliente clientePrueba1;
    private Cliente clientePrueba2;
    private Modelo modelo;


    private static final String NOMBRE="Jose";
    private static final String APELLIDOS="Limón";
    private static final int TELEFONO=222222;
    private static final String CORREO_ELECTRONICO="joselimon@gmail.com";
    private static final String NUMERO_TARJETA="151512";
    private static final String CIUDAD="Jose";
    private static final String CALLE="Avenida Goya";
    private static final String PAIS="España";
    private static final String CODIGO_POSTAL="50015";
    private static final String DNI="12341234G";



    @BeforeEach
    public void prepare(){
        modelo= new Modelo();
        modelo.conectar();
        clientePrueba1= new Cliente();
        clientePrueba1.setNombre(NOMBRE);
        clientePrueba1.setApellidos(APELLIDOS);
        clientePrueba1.setTelefono(TELEFONO);
        clientePrueba1.setCorreoElectronico(CORREO_ELECTRONICO);;
        clientePrueba1.setNumeroTarjeta(NUMERO_TARJETA);
        clientePrueba1.setCiudad(CIUDAD);
        clientePrueba1.setCalle(CALLE);
        clientePrueba1.setPais(PAIS);
        clientePrueba1.setCodigoPostal(CODIGO_POSTAL);
        clientePrueba1.setDni(DNI);


        clientePrueba2= new Cliente();
        clientePrueba2.setNombre(NOMBRE);
        clientePrueba2.setApellidos(APELLIDOS);
        clientePrueba2.setTelefono(TELEFONO);
        clientePrueba2.setCorreoElectronico(CORREO_ELECTRONICO);;
        clientePrueba2.setNumeroTarjeta(NUMERO_TARJETA);
        clientePrueba2.setCiudad(CIUDAD);
        clientePrueba2.setCalle(CALLE);
        clientePrueba2.setPais(PAIS);
        clientePrueba2.setCodigoPostal(CODIGO_POSTAL);
        clientePrueba2.setDni("12349999G");

    }

    /**
     * Test que nos servirá para comprobar si se crea correctamente un cliente.
     */
    @Test
    void testCrearCliente(){

        modelo.altaCliente(clientePrueba1);
        ArrayList<Cliente> listaClientes=modelo.getClientes();

        String esperado=null;
        for(Cliente cliente: listaClientes){
            if(cliente.getDni().equalsIgnoreCase("12341234G")){
                esperado="12341234G";
            }
        }
        Assertions.assertNotNull(esperado);
        /*
        String esperado="12341234G";
        Assertions.assertEquals(esperado, listaClientes.get(listaClientes.size()-1).getDni());
        */

    }

    /**
     * Test que nos servirá para comprobar si se borra correctamente un cliente.
     */
    @Test
    void testBorrarCliente(){

        modelo.altaCliente(clientePrueba2);

        ArrayList<Cliente> listaClientes1=modelo.getClientes();
        Cliente clienterPrueba4=listaClientes1.get(listaClientes1.size()-1);
        modelo.borrarCliente(clienterPrueba4);

        ArrayList<Cliente> listaClientes2=modelo.getClientes();

        String esperado=null;
        for(Cliente cliente: listaClientes2){
            if(cliente.getDni().equalsIgnoreCase("12349999G")){
                esperado="12349999G";
            }
        }
        Assertions.assertNull(esperado);
        /*
        String esperado="12341256G";
        Assertions.assertNotEquals(esperado, listaClientes2.get(listaClientes2.size()-1).getDni());
         */

    }


}

/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

/**
 * Clase Pedido que representa a la entidad. Con sus correspondientes campos.
 */
@Entity
@Table(name = "pedidos", schema = "baseecommerce", catalog = "")
public class Pedido {
    private int id;
    private Date fechaCompra;
    private double precioTotal;
    private String realizado;
    private Cliente cliente;
    private List<CaracteristicasPedido> caracteristicaspedidos;


    /**
     * Indica que es un atributo de la tabla(id), también el nombre de la columna y el método para obtener el atributo
     * de id
     * @return Objeto tipo int
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }
    /**
     * Método para modificar el atributo id
     * @param id Dato tipo int
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Indica que es un atributo de la tabla(fecha_compra), también el nombre de la columna y el método para obtener el atributo
     * de fecha_compra
     * @return Dato tipo Date
     */
    @Basic
    @Column(name = "fecha_compra")
    public Date getFechaCompra() {
        return fechaCompra;
    }
    /**
     * Método para modificar el atributo fechaCompra
     * @param fechaCompra Dato tipo Date
     */
    public void setFechaCompra(Date fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    /**
     * Indica que es un atributo de la tabla(precio_total), también el nombre de la columna y el método para obtener el atributo
     * de precio_total
     * @return Dato tipo double
     */
    @Basic
    @Column(name = "precio_total")
    public double getPrecioTotal() {
        return precioTotal;
    }
    /**
     * Método para modificar el atributo precioTotal
     * @param precioTotal Dato tipo double
     */
    public void setPrecioTotal(double precioTotal) {
        this.precioTotal = precioTotal;
    }

    /**
     * Indica que es un atributo de la tabla(realizado), también el nombre de la columna y el método para obtener el atributo
     * de realizado
     * @return Dato tipo String
     */
    @Basic
    @Column(name = "realizado")
    public String getRealizado() {
        return realizado;
    }
    /**
     * Método para modificar el atributo realizado
     * @param realizado Dato tipo Date
     */
    public void setRealizado(String realizado) {
        this.realizado = realizado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pedido pedido = (Pedido) o;
        return id == pedido.id &&
                Double.compare(pedido.precioTotal, precioTotal) == 0 &&
                Objects.equals(fechaCompra, pedido.fechaCompra) &&
                Objects.equals(realizado, pedido.realizado);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fechaCompra, precioTotal, realizado);
    }


    /**
     * Relación de muchos a uno. Muchos pedidos pueden estar asociadas a un cliente.
     * Getter y setter.
     * @return Devuelve lista de productos
     */
    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id", nullable = false)
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * Relación de uno a muchos. Un pedido puede tener mucho caraceristicaspedidos.
     * Getter y setter.
     * @return Devuelve lista de productos
     */
    @OneToMany(mappedBy = "pedido")
    public List<CaracteristicasPedido> getCaracteristicaspedidos() {
        return caracteristicaspedidos;
    }

    public void setCaracteristicaspedidos(List<CaracteristicasPedido> caracteristicaspedidos) {
        this.caracteristicaspedidos = caracteristicaspedidos;
    }


    /**
     * Método toString para nuestra clase Pedido
     * @return Dato tipo String
     */
    @Override
    public String toString() {
        return "ID: "+id+"      Fecha de compra: "+fechaCompra+" Precio total: "+precioTotal+"      Realizado: "+realizado;
    }

    /**
     * Método que nos servirá para sacar datos de la vista de empaquetador.
     * @return Dato tipo String
     */
    public String toEmpaquetador() {
        return "ID: "+id+"      Fecha de compra: "+fechaCompra+" Precio total: "+precioTotal+"      Realizado: "+realizado ;
    }

}

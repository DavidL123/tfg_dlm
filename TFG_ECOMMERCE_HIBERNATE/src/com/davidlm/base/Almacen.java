/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.base;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * Clase Almacen que representa a la entidad. Con sus correspondientes campos.
 */
@Entity
@Table(name = "almacenes", schema = "baseecommerce", catalog = "")
public class Almacen {
    private int id;
    private String nombre;
    private int telefono;
    private String correoElectronico;
    private int metrosCuadrados;
    private String ciudad;
    private String calle;
    private int codigoPostal;
    private int cantidadTrabajadores;
    private List<Producto> productos;

    /**
     * Indica que es el campo id de la tabla, también el nombre de la columna y el método para obtener el dato de id
     *
     * @return Objeto tipo int
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    /**
     * Método para modificar el atributo id
     * @param  id Objeto tipo int
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Indica que es un atributo de la tabla(nombre), también el nombre de la columna y el método para obtener el atributo
     * de nombre.
     * @return Objeto tipo String
     */
    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    /**
     * Método para modificar el atributo nombre
     * @param nombre String nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Indica que es un atributo de la tabla(telefono), también el nombre de la columna y el método para obtener el atributo
     * de telefono
     * @return Objeto tipo int
     */
    @Basic
    @Column(name = "telefono")
    public int getTelefono() {
        return telefono;
    }
    /**
     * Método para modificar el atributo telefono
     * @param telefono
     */
    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    /**
     * Indica que es un atributo de la tabla(correo_electronico), también el nombre de la columna y el método para obtener el atributo
     * de correo_electronico
     * @return Objeto tipo int
     */
    @Basic
    @Column(name = "correo_electronico")
    public String getCorreoElectronico() {
        return correoElectronico;
    }

    /**
     * Método para modificar el atributo correoElectronico
     * @param correoElectronico
     */
    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    /**
     * Indica que es un atributo de la tabla(metros_cuadrados), también el nombre de la columna y el método para obtener el atributo
     * de metros_cuadrados
     * @return Objeto tipo int
     */
    @Basic
    @Column(name = "metros_cuadrados")
    public int getMetrosCuadrados() {
        return metrosCuadrados;
    }

    /**
     * Método para modificar el atributo metrosCuadrados
     * @param metrosCuadrados
     */
    public void setMetrosCuadrados(int metrosCuadrados) {
        this.metrosCuadrados = metrosCuadrados;
    }

    /**
     * Indica que es un atributo de la tabla(ciudad), también el nombre de la columna y el método para obtener el atributo
     * de ciudad
     * @return Objeto tipo String
     */
    @Basic
    @Column(name = "ciudad")
    public String getCiudad() {
        return ciudad;
    }
    /**
     * Método para modificar el atributo ciudad
     * @param ciudad
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * Indica que es un atributo de la tabla(calle), también el nombre de la columna y el método para obtener el atributo
     * de calle
     * @return Objeto tipo String
     */
    @Basic
    @Column(name = "calle")
    public String getCalle() {
        return calle;
    }
    /**
     * Método para modificar el atributo calle
     * @param calle Dato tipo String
     */
    public void setCalle(String calle) {
        this.calle = calle;
    }

    /**
     * Indica que es un atributo de la tabla(codigo_postal), también el nombre de la columna y el método para obtener el atributo
     * de codigo_postal
     * @return Dato tipo int
     */
    @Basic
    @Column(name = "codigo_postal")
    public int getCodigoPostal() {
        return codigoPostal;
    }
    /**
     * Método para modificar el atributo codigoPostal
     * @param codigoPostal Dato tipo int
     */
    public void setCodigoPostal(int codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    /**
     * Indica que es un atributo de la tabla(cantidad_trabajadores), también el nombre de la columna y el método para obtener el atributo
     * de cantidad_trabajadores
     * @return Dato tipo int
     */
    @Basic
    @Column(name = "cantidad_trabajadores")
    public int getCantidadTrabajadores() {
        return cantidadTrabajadores;
    }
    /**
     * Método para modificar el atributo cantidadTrabajadores
     * @param cantidadTrabajadores Dato tipo int
     */
    public void setCantidadTrabajadores(int cantidadTrabajadores) {
        this.cantidadTrabajadores = cantidadTrabajadores;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Almacen almacen = (Almacen) o;
        return id == almacen.id &&
                telefono == almacen.telefono &&
                metrosCuadrados == almacen.metrosCuadrados &&
                codigoPostal == almacen.codigoPostal &&
                cantidadTrabajadores == almacen.cantidadTrabajadores &&
                Objects.equals(nombre, almacen.nombre) &&
                Objects.equals(correoElectronico, almacen.correoElectronico) &&
                Objects.equals(ciudad, almacen.ciudad) &&
                Objects.equals(calle, almacen.calle);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, telefono, correoElectronico, metrosCuadrados, ciudad, calle, codigoPostal, cantidadTrabajadores);
    }


    /**
     * Relación de uno a muchos. Una almacén puede tener muchos productos.
     * Getter y setter.
     * @return Devuelve lista de productos ArrayList
     */
    @OneToMany(mappedBy = "almacen")
    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    /**
     * Método toString para nuestra clase Almacén
     * @return Dato tipo String
     */
    @Override
    public String toString() {
        return "Nombre: "+nombre+" Ciudad: "+ciudad;
    }

}

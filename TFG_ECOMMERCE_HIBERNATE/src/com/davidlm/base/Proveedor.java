/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.base;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;


/**
 * Clase Proveedor que representa a la entidad. Con sus correspondientes campos.
 */
@Entity
@Table(name = "proveedores", schema = "baseecommerce", catalog = "")
public class Proveedor {
    private int id;
    private String nombre;
    private int telefono;
    private String correoElectronico;
    private String ciudad;
    private String calle;
    private int numero;
    private int codigoPostal;
    private String pais;
    private String nif;
    private String personaContacto;
    private List<Producto> productos;

    /**
     * Indica que es un atributo de la tabla(id), también el nombre de la columna y el método para obtener el atributo
     * de id
     * @return Dato tipo int
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }
    /**
     * Método para modificar el atributo id
     * @param id Dato tipo int
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Indica que es un atributo de la tabla(nombre), también el nombre de la columna y el método para obtener el atributo
     * de nombre
     * @return Dato tipo String
     */
    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }
    /**
     * Método para modificar el atributo nombre
     * @param nombre Dato tipo String
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Indica que es un atributo de la tabla(telefono), también el nombre de la columna y el método para obtener el atributo
     * de telefono
     * @return Objeto tipo int
     */
    @Basic
    @Column(name = "telefono")
    public int getTelefono() {
        return telefono;
    }
    /**
     * Método para modificar el atributo telefono
     * @param telefono Dato tipo int
     */
    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    /**
     * Indica que es un atributo de la tabla(correo_electronico), también el nombre de la columna y el método para obtener el atributo
     * de correo_electronico
     * @return Dato tipo String
     */
    @Basic
    @Column(name = "correo_electronico")
    public String getCorreoElectronico() {
        return correoElectronico;
    }
    /**
     * Método para modificar el atributo correoElectronico
     * @param correoElectronico Dato tipo String
     */
    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    /**
     * Indica que es un atributo de la tabla(ciudad), también el nombre de la columna y el método para obtener el atributo
     * de ciudad
     * @return Dato tipo String
     */
    @Basic
    @Column(name = "ciudad")
    public String getCiudad() {
        return ciudad;
    }
    /**
     * Método para modificar el atributo ciudad
     * @param ciudad Dato tipo String
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * Indica que es un atributo de la tabla(calle), también el nombre de la columna y el método para obtener el atributo
     * de calle
     * @return Objeto tipo String
     */
    @Basic
    @Column(name = "calle")
    public String getCalle() {
        return calle;
    }
    /**
     * Método para modificar el atributo calle
     * @param calle Dato tipo String
     */
    public void setCalle(String calle) {
        this.calle = calle;
    }

    /**
     * Indica que es un atributo de la tabla(numero), también el nombre de la columna y el método para obtener el atributo
     * de numero
     * @return Objeto tipo int
     */
    @Basic
    @Column(name = "numero")
    public int getNumero() {
        return numero;
    }
    /**
     * Método para modificar el atributo numero
     * @param numero Dato tipo int
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     * Indica que es un atributo de la tabla(codigo_postal), también el nombre de la columna y el método para obtener el atributo
     * de codigo_postal
     * @return Dato tipo int
     */
    @Basic
    @Column(name = "codigo_postal")
    public int getCodigoPostal() {
        return codigoPostal;
    }
    /**
     * Método para modificar el atributo codigoPostal
     * @param codigoPostal Dato tipo int
     */
    public void setCodigoPostal(int codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    /**
     * Indica que es un atributo de la tabla(pais), también el nombre de la columna y el método para obtener el atributo
     * de pais
     * @return Dato tipo String
     */
    @Basic
    @Column(name = "pais")
    public String getPais() {
        return pais;
    }
    /**
     * Método para modificar el atributo pais
     * @param pais Dato tipo String
     */
    public void setPais(String pais) {
        this.pais = pais;
    }

    /**
     * Indica que es un atributo de la tabla(nif), también el nombre de la columna y el método para obtener el atributo
     * de nif
     * @return Dato tipo String
     */
    @Basic
    @Column(name = "nif")
    public String getNif() {
        return nif;
    }
    /**
     * Método para modificar el atributo nif
     * @param nif Dato tipo String
     */
    public void setNif(String nif) {
        this.nif = nif;
    }

    /**
     * Indica que es un atributo de la tabla(persona_contacto), también el nombre de la columna y el método para obtener el atributo
     * de persona_contacto
     * @return Dato tipo String
     */
    @Basic
    @Column(name = "persona_contacto")
    public String getPersonaContacto() {
        return personaContacto;
    }
    /**
     * Método para modificar el atributo personaContacto
     * @param personaContacto Dato tipo String
     */
    public void setPersonaContacto(String personaContacto) {
        this.personaContacto = personaContacto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Proveedor proveedor = (Proveedor) o;
        return id == proveedor.id &&
                telefono == proveedor.telefono &&
                numero == proveedor.numero &&
                codigoPostal == proveedor.codigoPostal &&
                Objects.equals(nombre, proveedor.nombre) &&
                Objects.equals(correoElectronico, proveedor.correoElectronico) &&
                Objects.equals(ciudad, proveedor.ciudad) &&
                Objects.equals(calle, proveedor.calle) &&
                Objects.equals(pais, proveedor.pais) &&
                Objects.equals(nif, proveedor.nif) &&
                Objects.equals(personaContacto, proveedor.personaContacto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, telefono, correoElectronico, ciudad, calle, numero, codigoPostal, pais, nif, personaContacto);
    }

    /**
     * Relación de uno a muchos. Un proveedor puede proveer muchos productos.
     * Getter y setter.
     * @return Devuelve lista de productos
     */
    @OneToMany(mappedBy = "proveedor")
    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    /**
     * Método toString para nuestra clase Proveedor
     * @return Dato tipo String.
     */
    @Override
    public String toString() {
        return "Nombre: "+nombre+" NIF: "+nif;
    }
}

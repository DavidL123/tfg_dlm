/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.base;

import javax.persistence.*;
import java.util.Objects;
/**
 * Clase CaracteristicasPedido que representa a la entidad. Con sus correspondientes campos.
 */
@Entity
@Table(name = "productos_pedidos", schema = "baseecommerce", catalog = "")
public class CaracteristicasPedido {
    private int id;
    private int cantidad;
    private Producto producto;
    private Pedido pedido;

    /**
     * Indica que es un atributo de la tabla(id), también el nombre de la columna y el método para obtener el atributo
     * de id
     * @return Dato tipo int
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }
    /**
     * Método para modificar el atributo id
     * @param id Dato tipo int
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Indica que es un atributo de la tabla(cantidad), también el nombre de la columna y el método para obtener el atributo
     * de cantidad
     * @return Objeto tipo int
     */
    @Basic
    @Column(name = "cantidad")
    public int getCantidad() {
        return cantidad;
    }
    /**
     * Método para modificar el atributo cantidad
     * @param cantidad
     */
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaracteristicasPedido that = (CaracteristicasPedido) o;
        return id == that.id &&
                cantidad == that.cantidad;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cantidad);
    }


    /**
     * Relación de muchos a uno. Muchos característicaspedido pueden estar asociadas a un producto.
     * Getter y setter.
     * @return Devuelve un producto
     */
    @ManyToOne
    @JoinColumn(name = "id_producto", referencedColumnName = "id", nullable = false)
    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    /**
     * Relación de muchos a uno. Muchos característicaspedido pueden estar asociadas a un pedido.
     * Getter y setter.
     * @return Devuelve un Pedido
     */
    @ManyToOne
    @JoinColumn(name = "id_pedido", referencedColumnName = "id", nullable = false)
    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    /**
     * Método toString para nuestra clase CaracteristicasPedido
     * @return Dato tipo String
     */
    @Override
    public String toString() {
        return "Cantidad: "+cantidad;
    }

}

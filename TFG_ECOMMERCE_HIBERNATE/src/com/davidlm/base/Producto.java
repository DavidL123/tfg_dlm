/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Clase Producto que representa a la entidad. Con sus correspondientes campos.
 */
@Entity
@Table(name = "productos", schema = "baseecommerce", catalog = "")
public class Producto {
    private int id;
    private String nombre;
    private String marca;
    private int pesoEnGramos;
    private String dimensiones;
    private double precio;
    private String categoria;
    private Date fechaCreacion;
    private byte envioInternacional;
    private byte[] imagen;
    private Almacen almacen;
    private List<CaracteristicasPedido> caracteristicaspedidos;
    private Proveedor proveedor;

    /**
     * Indica que es un atributo de la tabla(id), también el nombre de la columna y el método para obtener el atributo
     * de id
     * @return Dato tipo int
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }
    /**
     * Método para modificar el atributo id
     * @param id Dato tipo int
     */
    public void setId(int id) {
        this.id = id;
    }


    /**
     * Indica que es un atributo de la tabla(nombre), también el nombre de la columna y el método para obtener el atributo
     * de nombre
     * @return Dato tipo String
     */
    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }
    /**
     * Método para modificar el atributo nombre
     * @param nombre Dato tipo String
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Indica que es un atributo de la tabla(marca), también el nombre de la columna y el método para obtener el atributo
     * de marca
     * @return Dato tipo String
     */
    @Basic
    @Column(name = "marca")
    public String getMarca() {
        return marca;
    }
    /**
     * Método para modificar el atributo marca
     * @param marca Dato tipo String
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * Indica que es un atributo de la tabla(peso_en_gramos), también el nombre de la columna y el método para obtener el atributo
     * de peso_en_gramos
     * @return Objeto tipo int
     */
    @Basic
    @Column(name = "peso_en_gramos")
    public int getPesoEnGramos() {
        return pesoEnGramos;
    }
    /**
     * Método para modificar el atributo pesoEnGramos
     * @param pesoEnGramos Dato tipo int
     */
    public void setPesoEnGramos(int pesoEnGramos) {
        this.pesoEnGramos = pesoEnGramos;
    }

    /**
     * Indica que es un atributo de la tabla(dimensiones), también el nombre de la columna y el método para obtener el atributo
     * de dimensiones
     * @return Objeto tipo String
     */
    @Basic
    @Column(name = "dimensiones")
    public String getDimensiones() {
        return dimensiones;
    }
    /**
     * Método para modificar el atributo dimensiones
     * @param dimensiones Dato tipo String
     */
    public void setDimensiones(String dimensiones) {
        this.dimensiones = dimensiones;
    }

    /**
     * Indica que es un atributo de la tabla(precio), también el nombre de la columna y el método para obtener el atributo
     * de precio
     * @return Dato tipo double
     */
    @Basic
    @Column(name = "precio")
    public double getPrecio() {
        return precio;
    }
    /**
     * Método para modificar el atributo precio
     * @param precio Dato tipo double
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }

    /**
     * Indica que es un atributo de la tabla(categoria), también el nombre de la columna y el método para obtener el atributo
     * de categoria
     * @return Objeto tipo String
     */
    @Basic
    @Column(name = "categoria")
    public String getCategoria() {
        return categoria;
    }
    /**
     * Método para modificar el atributo categoria
     * @param categoria Dato tipo String
     */
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    /**
     * Indica que es un atributo de la tabla(fecha_creacion), también el nombre de la columna y el método para obtener el atributo
     * de fecha_creacion
     * @return Objeto tipo Date
     */
    @Basic
    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return fechaCreacion;
    }
    /**
     * Método para modificar el atributo fechaCreacion
     * @param fechaCreacion Dato tipo Date
     */
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    /**
     * Indica que es un atributo de la tabla(envio_internacional), también el nombre de la columna y el método para obtener el atributo
     * de envio_internacional
     * @return Dato tipo byte
     */
    @Basic
    @Column(name = "envio_internacional")
    public byte getEnvioInternacional() {
        return envioInternacional;
    }
    /**
     * Método para modificar el atributo envioInternacional
     * @param envioInternacional Dato tipo byte
     */
    public void setEnvioInternacional(byte envioInternacional) {
        this.envioInternacional = envioInternacional;
    }

    /**
     * Indica que es un atributo de la tabla(imagen), también el nombre de la columna y el método para obtener el atributo
     * de imagen
     * @return Dato tipo byte[]
     */
    @Basic
    @Column(name = "imagen")
    public byte[] getImagen() {
        return imagen;
    }
    /**
     * Método para modificar el atributo imagen
     * @param imagen Dato tipo byte[]
     */
    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Producto producto = (Producto) o;
        return id == producto.id &&
                pesoEnGramos == producto.pesoEnGramos &&
                Double.compare(producto.precio, precio) == 0 &&
                envioInternacional == producto.envioInternacional &&
                Objects.equals(nombre, producto.nombre) &&
                Objects.equals(marca, producto.marca) &&
                Objects.equals(dimensiones, producto.dimensiones) &&
                Objects.equals(categoria, producto.categoria) &&
                Objects.equals(fechaCreacion, producto.fechaCreacion) &&
                Arrays.equals(imagen, producto.imagen);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, nombre, marca, pesoEnGramos, dimensiones, precio, categoria, fechaCreacion, envioInternacional);
        result = 31 * result + Arrays.hashCode(imagen);
        return result;
    }


    /**
     * Relación de muchos a uno. Muchos productos pueden estar asociadas a un almacén.
     * Getter y setter.
     * @return Devuelve un almacen.
     */
    @ManyToOne
    @JoinColumn(name = "id_almacen", referencedColumnName = "id", nullable = false)
    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }


    /**
     * Relación de uno a muchos. Un producto puede estar asociado a muchos caraceristicaspedidos.
     * Getter y setter.
     * @return Devuelve lista de caracteristicaspedido
     */
    @OneToMany(mappedBy = "producto")
    public List<CaracteristicasPedido> getCaracteristicaspedidos() {
        return caracteristicaspedidos;
    }

    public void setCaracteristicaspedidos(List<CaracteristicasPedido> caracteristicaspedidos) {
        this.caracteristicaspedidos = caracteristicaspedidos;
    }

    /**
     * Relación de muchos a uno. Muchos productos pueden estar asociadas a un proveedor.
     * Getter y setter.
     * @return Devuelve un proveedor.
     */
    @ManyToOne
    @JoinColumn(name = "id_proveedor", referencedColumnName = "id", nullable = false)
    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    /**
     * Método toString para nuestra clase Producto
     * @return Dato tipo String.
     */
    @Override
    public String toString() {
        return "Nombre: "+nombre+" Marca: "+marca + " Precio: "+precio;
    }



}

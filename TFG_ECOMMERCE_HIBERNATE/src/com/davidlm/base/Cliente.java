/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.base;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * Clase Cliente que representa a la entidad. Con sus correspondientes campos.
 */
@Entity
@Table(name = "clientes", schema = "baseecommerce", catalog = "")
public class Cliente {
    private int id;
    private String nombre;
    private String apellidos;
    private int telefono;
    private String correoElectronico;
    private String numeroTarjeta;
    private String ciudad;
    private String calle;
    private String pais;
    private String codigoPostal;
    private String dni;
    private List<Pedido> pedidos;

    /**
     * Indica que es un atributo de la tabla(id), también el nombre de la columna y el método para obtener el atributo
     * de id
     * @return Objeto tipo int
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }
    /**
     * Método para modificar el atributo id
     * @param id Dato tipo int
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Indica que es un atributo de la tabla(nombre), también el nombre de la columna y el método para obtener el atributo
     * de nombre
     * @return Dato tipo String
     */
    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }
    /**
     * Método para modificar el atributo nombre
     * @param nombre Dato tipo String
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Indica que es un atributo de la tabla(apellidos), también el nombre de la columna y el método para obtener el atributo
     * de apellidos
     * @return Dato tipo String
     */
    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }
    /**
     * Método para modificar el atributo apellidos
     * @param apellidos Dato tipo String
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * Indica que es un atributo de la tabla(telefono), también el nombre de la columna y el método para obtener el atributo
     * de telefono
     * @return Objeto tipo int
     */
    @Basic
    @Column(name = "telefono")
    public int getTelefono() {
        return telefono;
    }
    /**
     * Método para modificar el atributo telefono
     * @param telefono Dato tipo int
     */
    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    /**
     * Indica que es un atributo de la tabla(correo_electronico), también el nombre de la columna y el método para obtener el atributo
     * de correo_electronico
     * @return Dato tipo String
     */
    @Basic
    @Column(name = "correo_electronico")
    public String getCorreoElectronico() {
        return correoElectronico;
    }
    /**
     * Método para modificar el atributo correoElectronico
     * @param correoElectronico Dato tipo String
     */
    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    /**
     * Indica que es un atributo de la tabla(numero_tarjeta), también el nombre de la columna y el método para obtener el atributo
     * de numero_tarjeta
     * @return Dato tipo String
     */
    @Basic
    @Column(name = "numero_tarjeta")
    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }
    /**
     * Método para modificar el atributo numeroTarjeta
     * @param numeroTarjeta Dato tipo String
     */
    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    /**
     * Indica que es un atributo de la tabla(ciudad), también el nombre de la columna y el método para obtener el atributo
     * de ciudad
     * @return Objeto tipo String
     */
    @Basic
    @Column(name = "ciudad")
    public String getCiudad() {
        return ciudad;
    }
    /**
     * Método para modificar el atributo ciudad
     * @param ciudad Dato tipo String
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * Indica que es un atributo de la tabla(calle), también el nombre de la columna y el método para obtener el atributo
     * de calle
     * @return Dato tipo String
     */
    @Basic
    @Column(name = "calle")
    public String getCalle() {
        return calle;
    }
    /**
     * Método para modificar el atributo calle
     * @param calle Dato tipo String
     */
    public void setCalle(String calle) {
        this.calle = calle;
    }

    /**
     * Indica que es un atributo de la tabla(pais), también el nombre de la columna y el método para obtener el atributo
     * de pais
     * @return Objeto tipo String
     */
    @Basic
    @Column(name = "pais")
    public String getPais() {
        return pais;
    }
    /**
     * Método para modificar el atributo pais
     * @param pais Dato tipo String
     */
    public void setPais(String pais) {
        this.pais = pais;
    }

    /**
     * Indica que es un atributo de la tabla(codigo_postal), también el nombre de la columna y el método para obtener el atributo
     * de codigo_postal
     * @return Dato tipo String
     */
    @Basic
    @Column(name = "codigo_postal")
    public String getCodigoPostal() {
        return codigoPostal;
    }
    /**
     * Método para modificar el atributo codigoPostal
     * @param codigoPostal Dato tipo String
     */
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    /**
     * Indica que es un atributo de la tabla(dni), también el nombre de la columna y el método para obtener el atributo
     * de dni
     * @return Dato tipo String
     */
    @Basic
    @Column(name = "dni")
    public String getDni() {
        return dni;
    }
    /**
     * Método para modificar el atributo dni
     * @param dni Dato tipo String
     */
    public void setDni(String dni) {
        this.dni = dni;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return id == cliente.id &&
                telefono == cliente.telefono &&
                Objects.equals(nombre, cliente.nombre) &&
                Objects.equals(apellidos, cliente.apellidos) &&
                Objects.equals(correoElectronico, cliente.correoElectronico) &&
                Objects.equals(numeroTarjeta, cliente.numeroTarjeta) &&
                Objects.equals(ciudad, cliente.ciudad) &&
                Objects.equals(calle, cliente.calle) &&
                Objects.equals(pais, cliente.pais) &&
                Objects.equals(codigoPostal, cliente.codigoPostal) &&
                Objects.equals(dni, cliente.dni);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellidos, telefono, correoElectronico, numeroTarjeta, ciudad, calle, pais, codigoPostal, dni);
    }

    /**
     * Relación de uno a muchos. Una cliente puede tener muchos pedidos.
     * Getter y setter.
     * @return Devuelve lista de pedidos
     */
    @OneToMany(mappedBy = "cliente")
    public List<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    /**
     * Método toString para nuestra clase Cliente
     * @return Dato tipo String
     */
    @Override
    public String toString() {
        return "Nombre: "+nombre+ " DNI: "+dni;
    }

}
